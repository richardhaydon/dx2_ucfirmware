// Title: WideNorth Framework
//
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw, Gtest, Rapidcheck
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "mocks/mock_serial_interface.hpp"
#include "mocks/mock_temperature_sensor.hpp"

using testing::_;
using testing::Return;

// Environment test check that the mock objects are working as expected and can be built
TEST(Environment, mockserial) {
  // test creation and expectations on a mock serial object
  mock_serial_interface mockif;

  EXPECT_CALL(mockif,tx(0x20));
  mockif.tx(0x20);

  mockif.feed(0x21);
  mockif.feed(0x22);
  uint8_t achar;
  auto res = mockif.rx(&achar);
  EXPECT_EQ(0u,res);
  EXPECT_EQ(0x21, achar);
  res = mockif.rx(&achar);
  EXPECT_EQ(0u,res);
  EXPECT_EQ(0x22, achar);

}

// Test and demo use of the mock temperature sensor object
TEST(Environment, mocktemperature) {
    // test creation and expectations on a mock temperature object
    mock_temperature_sensor mocktemp;
    EXPECT_CALL(mocktemp,read_temperature()).WillRepeatedly(Return(20));

    auto temp = mocktemp.read_temperature();
    EXPECT_EQ(temp,20);

}


