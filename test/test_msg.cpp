// Title: WideNorth Framework
//
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw, Gtest, Rapidcheck
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "wn_msg.hpp"
#include "scheduler_msg.hpp"

using testing::_;
using testing::Return;

using namespace wn;

TEST(Msg, scheduler_msg_default) {
    scheduler_msg::msg cmd;
    EXPECT_EQ(cmd.m_msg_type, scheduler_msg::msg_types::nop);
    EXPECT_TRUE(cmd.getCommandStr() == "NOP");
}


TEST(Msg, scheduler_msg_tick) {
  scheduler_msg::msg cmd(scheduler_msg::msg_types::tick);
  EXPECT_EQ(cmd.m_msg_type, scheduler_msg::msg_types::tick);
  EXPECT_TRUE(cmd.getCommandStr() == "TICK");
}

TEST(Msg, scheduler_msg_poweron) {
    scheduler_msg::msg cmd(scheduler_msg::msg_types::power_on);
    EXPECT_EQ(cmd.m_msg_type, scheduler_msg::msg_types::power_on);
    EXPECT_TRUE(cmd.getCommandStr() == "POWER_ON");
}

TEST(Msg, scheduler_msg_mainstoggle) {
    scheduler_msg::msg cmd(scheduler_msg::msg_types::mains_status);
    EXPECT_EQ(cmd.m_msg_type, scheduler_msg::msg_types::mains_status);
    EXPECT_TRUE(cmd.getCommandStr() == "MAINS_STATUS");
}


TEST(Msg, scheduler_msg_serial_rx) {
    scheduler_msg::msg cmd(scheduler_msg::msg_types::serial_rx,0x55, 0x01, 0x02,0xBEEF);
    EXPECT_EQ(cmd.m_msg_type, scheduler_msg::msg_types::serial_rx);
    EXPECT_EQ(cmd.m_address,0x55);
    EXPECT_EQ(cmd.m_control,0x01);
    EXPECT_EQ(cmd.m_command,0x02);
    EXPECT_EQ(cmd.m_value,0xBEEF);
}

