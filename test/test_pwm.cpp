// Title: WideNorth Framework test of pwm class
//
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw, Gtest, Rapidcheck
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "pwm_if.hpp"
#include "mocks/mock_pwm_if.hpp"

using testing::_;
using testing::Return;

using namespace wn;



// Environment test check that the mock objects are working as expected and can be built
TEST(Msg, ds_register) {
    mock_pwm_if mock;
    EXPECT_EQ(mock.duty_cycle_reg(1000,25000,50),500);
    EXPECT_EQ(mock.duty_cycle_reg(1000,25000,0),1000);
    EXPECT_EQ(mock.duty_cycle_reg(1000,25000,101),0);
}

