// Title: WideNorth Framework test of scheduler
//
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw, Gtest, Rapidcheck
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <iomanip>
#include "mocks/mock_serial_interface.hpp"
#include "mocks/mock_gpio_interface.hpp"
#include "mocks/mock_persistent_storage_if.hpp"
#include "mocks/mock_pwm_if.hpp"
#include "mocks/mock_power_monitor.hpp"
#include "dx2_factory_data.hpp"
#include "dx2_serial_if.hpp"
#include "dib_const.hpp"
#include "dib_version.hpp"
#define protected public
#include "scheduler.hpp"
#undef protected

#include <thread>
#include <exception>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>
#include <vector>
#include "dx2_pins.hpp"
#include "scheduler_msg.hpp"
#include "c_serial.h"

using testing::_;
using testing::Return;


class Scheduler_fixture : public ::testing::Test {

protected:
    std::string m_test_port {"COM10"}  ;

  Scheduler_fixture() {

  }
  ~Scheduler_fixture() {
  }

  protected:


      c_serial_port_t *protocol_port;
      uint8_t frame_buf[128];
      size_t frame_len{0};

      void create_frame(uint8_t command, uint16_t value) {
          frame_len=0;
          frame_buf[frame_len++] = wn::dx2_serial_if::serport_start_stop_flag;
          frame_buf[frame_len++] = 0x55; // address
          frame_buf[frame_len++] = 0x00; // control
          frame_buf[frame_len++] = command; // command
          frame_buf[frame_len++] = (value >> 8) & 0xFF; // value msb
          frame_buf[frame_len++] = (value)      & 0xFF; // value lsb
          uint8_t crc{0};
          for(auto i=1; i<frame_len; i++) {
              crc+=frame_buf[i];
          }
          frame_buf[frame_len++] = crc;
          frame_buf[frame_len++] = wn::dx2_serial_if::serport_start_stop_flag;
      }

      c_serial_port_t *open_port(std::string const &port_name) {
          c_serial_port_t *port = nullptr;

          // c_serial_set_global_log_function(c_serial_stderr_log_function);
          if (c_serial_new(&port, nullptr) < 0) {
          }
          if (c_serial_set_port_name(port, port_name.c_str()) < 0) {
          }
          c_serial_set_baud_rate(port, CSERIAL_BAUD_115200);
          c_serial_set_data_bits(port, CSERIAL_BITS_8);
          c_serial_set_stop_bits(port, CSERIAL_STOP_BITS_1);
          c_serial_set_parity(port, CSERIAL_PARITY_NONE);
          c_serial_set_flow_control(port, CSERIAL_FLOW_NONE);
          c_serial_set_serial_line_change_flags(port, CSERIAL_LINE_FLAG_ALL);
          auto status = c_serial_open(port);
          if (status != 0) {
              std::cout << " failed to open " << port_name << "\r\n";
              return nullptr;
          }
          return port;
      }

      void close_port(c_serial_port_t *port) {
          c_serial_close(port);
      }


      bool available(c_serial_port_t *port) {
          int cnt{0};
          auto res = c_serial_get_available(port, &cnt);
          if (res == CSERIAL_OK) {
              return (cnt != 0);
          }
          return false;
      }

      bool is_open(c_serial_port_t *port) {
          return c_serial_is_open(port) == 1;
      }




  // test creation and expectations on a mock serial object
  std::shared_ptr<mock_serial_interface> mock_trace_if = std::make_shared<mock_serial_interface>() ;
  std::shared_ptr<mock_serial_interface> mock_pc_if = std::make_shared<mock_serial_interface>() ;
  std::shared_ptr<mock_gpio_interface> mockgpio = std::make_shared<mock_gpio_interface>();
  std::shared_ptr<stub_gpio_interface> stubgpio = std::make_shared<stub_gpio_interface>();
  std::shared_ptr<wn::mock_persistent_storage_if> flash_exact =
      std::make_shared<wn::mock_persistent_storage_if>(10);
  std::shared_ptr<mock_pwm_if> mockpwm = std::make_shared<mock_pwm_if>();
  std::shared_ptr<mock_power_monitor> mockpowermonitor = std::make_shared<mock_power_monitor>();
  std::shared_ptr<stub_power_monitor> stubpowermonitor = std::make_shared<stub_power_monitor>();

  std::shared_ptr<wn::factory_data> mockfactorydata = std::make_shared<wn::factory_data>(flash_exact)  ;
  wn::scheduler_msg::msg tick_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::tick);
  wn::scheduler_msg::msg poweron_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::power_on);
  wn::scheduler_msg::msg mainstoggle_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::mains_status);

  std::shared_ptr<wn::Scheduler>  scheduler = std::make_shared<wn::Scheduler>( mock_trace_if,stubgpio,mockfactorydata,mock_pc_if,mockpwm,stubpowermonitor );

  // test port for target tests

};










// Test driving of the pcb led with the tick rate we expect change of state every second
TEST_F(Scheduler_fixture, pcb_led) {
    //auto  scheduler = wn::Scheduler(mock_trace_if,stubgpio,mockfactorydata,mock_pc_if,mockpwm,mockpowermonitor  );
    auto ms_elapsed{0u};
    while(!stubgpio->m_bits[ wn::dib_pin::pcb_led ]) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed,20);
    // expect the led to be turned off
    while(ms_elapsed < 1000 &&  stubgpio->m_bits[ wn::dib_pin::pcb_led ]) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed,1000);

}

TEST_F(Scheduler_fixture, top_led) {
    auto ms_elapsed{0u};
    while(!stubgpio->m_bits[ wn::dib_pin::pcb_led ]) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed,20);
    // expect the led to be turned off
    while(ms_elapsed < 1000 &&  stubgpio->m_bits[ wn::dib_pin::pcb_led ]) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed,1000);

}



// Test that the door lock line is cleared after our configured delay in seconds
// note from the original code it takes configured seconds + 1 to close..
TEST_F(Scheduler_fixture, door_lock) {
    //auto  scheduler = wn::Scheduler(mockif,stubgpio,mockfactorydata  );
    // simulates the door being open
    stubgpio->set(    wn::dib_pin::door_lock );
    scheduler->m_DoorOpenSecCnt = 3;

    // tick until it is closed check how long is needed
    auto ms_elapsed{0u};
    while(ms_elapsed < 5000 && stubgpio->m_bits[ wn::dib_pin::door_lock ]) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed,4010);
    // repeat
    stubgpio->set(    wn::dib_pin::door_lock );
    scheduler->m_DoorOpenSecCnt = 3;
    ms_elapsed = 0;
    while(ms_elapsed < 5000 && stubgpio->m_bits[ wn::dib_pin::door_lock ]) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed,4000);



}

// Test tick action on first tick with power off ( active low)
TEST_F(Scheduler_fixture, boot_power_off) {

   // power on is active low set to HI for this test
   stubgpio->set(wn::dib_pin::power_on);

   // Set up lines we expect to be flipped in the start position
   stubgpio->set(wn::dib_pin::valve_1);
   stubgpio->clr(wn::dib_pin::valve_2);
   stubgpio->set(wn::dib_pin::compressor);
   stubgpio->set(wn::dib_pin::fan_on);
   stubgpio->set(wn::dib_pin::relay_on_230vac);

   ASSERT_EQ(scheduler->m_ucState, wn::scheduler_state::STATE_BOOT);
   scheduler->msg(&tick_msg);

   // Write of control register
   EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_1 ),0  );
   EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_2 ), 1);  // air bleed valve
   EXPECT_EQ(stubgpio->get( wn::dib_pin::compressor ), 0);
   EXPECT_EQ(stubgpio->get( wn::dib_pin::fan_on ),0);
   EXPECT_EQ(stubgpio->get( wn::dib_pin::relay_on_230vac ),0);

   ASSERT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_SLEEP);
   EXPECT_EQ(scheduler->m_BattCntSec,0);
   scheduler->msg(&tick_msg);
   EXPECT_EQ(scheduler->m_ucState, wn::scheduler_state::STATE_SLEEP);

}

TEST_F(Scheduler_fixture, boot_power_on_mains_connected) {

    // power on is active low set to LO for this test
    stubgpio->clr(wn::dib_pin::power_on);
    stubgpio->set(wn::dib_pin::mains_connected);

    // Set up lines we expect to be flipped in the start position
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->clr(wn::dib_pin::valve_2);
    stubgpio->set(wn::dib_pin::compressor);
    stubgpio->set(wn::dib_pin::fan_on);
    stubgpio->set(wn::dib_pin::relay_on_230vac);

    ASSERT_EQ(scheduler->m_ucState, wn::scheduler_state::STATE_BOOT);
    scheduler->msg(&tick_msg);

    // Write of control register
    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_1 ),0  );
    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_2 ), 1);  // air bleed valve
    EXPECT_EQ(stubgpio->get( wn::dib_pin::compressor ), 0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::fan_on ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::relay_on_230vac ),0);

    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_IDLE_MAINS);
    scheduler->msg(&tick_msg);
    EXPECT_EQ(scheduler->m_ucState, wn::scheduler_state::STATE_IDLE_MAINS);

}

TEST_F(Scheduler_fixture, boot_power_on_mains_unconnected) {

    // power on is active low set to LO for this test
    stubgpio->clr(wn::dib_pin::power_on);
    stubgpio->clr(wn::dib_pin::mains_connected);

    // Set up lines we expect to be flipped in the start position
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->clr(wn::dib_pin::valve_2);
    stubgpio->set(wn::dib_pin::compressor);
    stubgpio->set(wn::dib_pin::fan_on);
    stubgpio->set(wn::dib_pin::relay_on_230vac);

    ASSERT_EQ(scheduler->m_ucState, wn::scheduler_state::STATE_BOOT);
    scheduler->msg(&tick_msg);

    // Write of control register
    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_1 ),0  );
    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_2 ), 1);  // air bleed valve
    EXPECT_EQ(stubgpio->get( wn::dib_pin::compressor ), 0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::fan_on ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::relay_on_230vac ),0);

    ASSERT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_IDLE_BATTERY);

}

// Test going to sleep state which turns off charge, force run and relay
// state should persist long enought to allow PC to shut down
TEST_F(Scheduler_fixture, go_to_sleep) {

    // preenter the test state, test that in this states run the battery force n
    // set the converse of the states we expect
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_GO_TO_SLEEP;
    stubgpio->set(wn::dib_pin::force_run_on_battery_n);
    stubgpio->set(wn::dib_pin::battery_charge);
    stubgpio->set(wn::dib_pin::relay_on_230vac);
    stubgpio->set(wn::dib_pin::power_to_pc);

    scheduler->msg(&tick_msg);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::force_run_on_battery_n ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::battery_charge ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::relay_on_230vac ),0);
    // initially the pc power is still on
    EXPECT_EQ(stubgpio->get( wn::dib_pin::power_to_pc ),1);
    // tick until the pc power is off
    auto ms_elapsed = 0;
    while( stubgpio->get(wn::dib_pin::power_to_pc)) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    // all timings use plus one second and we add the actual tick that triggers the change
    EXPECT_EQ(ms_elapsed,(wn::STATE_POWER_OFF_SEC+1)*1000+10);

}

// Test exit conditions and line settings in the going to sleep state in the transition to sleep
TEST_F(Scheduler_fixture, going_to_sleep_completes) {

    // preenter the test state, test that in this states run the battery force n
    // set the converse of the states we expect
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_GOING_TO_SLEEP;
    stubgpio->set(wn::dib_pin::force_run_on_battery_n);
    stubgpio->set(wn::dib_pin::battery_charge);
    stubgpio->set(wn::dib_pin::relay_on_230vac);
    stubgpio->set(wn::dib_pin::power_to_pc);
    // the power button remains indicating off so that the operation completes
    stubgpio->set(wn::dib_pin::power_on);

    scheduler->msg(&tick_msg);


    // Test single tick resets the controlled line states
    EXPECT_EQ(stubgpio->get( wn::dib_pin::force_run_on_battery_n ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::battery_charge ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::relay_on_230vac ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::power_to_pc ),0);

    // Test that the state does not change until the pc power off sec has elapsed
    auto ms_elapsed = 0;
    while( scheduler->m_ucStateNxt == wn::scheduler_state::STATE_GOING_TO_SLEEP) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed , (wn::STATE_PC_POWER_OFF_SEC+1)*1000+10);
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::STATE_SLEEP  );

}

// Test exit conditions and line settings in the going to idle mains state
TEST_F(Scheduler_fixture, going_to_sleep_to_idle_mains) {

    // preenter the test state, test that in this states run the battery force n
    // set the converse of the states we expect
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_GOING_TO_SLEEP;
    stubgpio->set(wn::dib_pin::force_run_on_battery_n);
    stubgpio->set(wn::dib_pin::battery_charge);
    stubgpio->set(wn::dib_pin::relay_on_230vac);
    stubgpio->set(wn::dib_pin::power_to_pc);
    // the power button is on, indicating that we repower up
    stubgpio->clr(wn::dib_pin::power_on);
    // mains is present to determine our end state
    stubgpio->set(wn::dib_pin::mains_connected);

    // Test that the state does not change until the pc power off sec has elapsed
    auto ms_elapsed = 0;
    while( scheduler->m_ucStateNxt == wn::scheduler_state::STATE_GOING_TO_SLEEP) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed , (wn::STATE_PC_POWER_OFF_SEC+1)*1000+20);
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::STATE_IDLE_MAINS  );

}

// Test exit conditions and line settings in the going to idle battery state
TEST_F(Scheduler_fixture, going_to_sleep_to_idle_battery) {

    // preenter the test state, test that in this states run the battery force n
    // set the converse of the states we expect
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_GOING_TO_SLEEP;
    stubgpio->set(wn::dib_pin::force_run_on_battery_n);
    stubgpio->set(wn::dib_pin::battery_charge);
    stubgpio->set(wn::dib_pin::relay_on_230vac);
    stubgpio->set(wn::dib_pin::power_to_pc);
    // the power button is on, indicating that we repower up
    stubgpio->clr(wn::dib_pin::power_on);
    // mains is absent to determine our end state
    stubgpio->clr(wn::dib_pin::mains_connected);

    // Test that the state does not change until the pc power off sec has elapsed
    auto ms_elapsed = 0;
    while( scheduler->m_ucStateNxt == wn::scheduler_state::STATE_GOING_TO_SLEEP) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed , (wn::STATE_PC_POWER_OFF_SEC+1)*1000+20);
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::STATE_IDLE_BATTERY  );

}

// Test the sleep state transition to idle mains
TEST_F(Scheduler_fixture, sleep_to_idle_mains) {
    // test that the control register is reset and all lines set low
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_SLEEP;
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->set(wn::dib_pin::compressor);
    stubgpio->set(wn::dib_pin::fan_on);

    scheduler->m_regCtrl = 0xffff;
    stubgpio->clr(wn::dib_pin::power_on);  // turn to power on so we come out of sleep
    stubgpio->set(wn::dib_pin::mains_connected); // into mains idle on wake up

    scheduler->msg(&tick_msg);

    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_1 ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_2 ),1); // air bleed valve open
    EXPECT_EQ(stubgpio->get( wn::dib_pin::compressor ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::fan_on ),0);
    EXPECT_EQ(scheduler->m_regCtrl, 4);
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::STATE_IDLE_MAINS);

}


TEST_F(Scheduler_fixture, sleep_to_idle_battery) {
    // test that the control register is reset and all lines set low
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_SLEEP;
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->set(wn::dib_pin::compressor);
    stubgpio->set(wn::dib_pin::fan_on);

    scheduler->m_regCtrl = 0xffff;
    stubgpio->clr(wn::dib_pin::power_on);  // turn to power on so we come out of sleep
    stubgpio->clr(wn::dib_pin::mains_connected); // into mains idle on wake up

    scheduler->msg(&tick_msg);

    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_1 ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_2 ),1); // air bleed valve open
    EXPECT_EQ(stubgpio->get( wn::dib_pin::compressor ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::fan_on ),0);
    EXPECT_EQ(scheduler->m_regCtrl, 4);
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::STATE_IDLE_BATTERY);

}

TEST_F(Scheduler_fixture, sleep_to_off_battery) {
    // test that the control register is reset and all lines set low
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_SLEEP;
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->set(wn::dib_pin::valve_1);
    stubgpio->set(wn::dib_pin::compressor);
    stubgpio->set(wn::dib_pin::fan_on);

    scheduler->m_regCtrl = 0xffff;
    stubgpio->set(wn::dib_pin::power_on);  // turn to power on so we come out of sleep
    stubgpio->clr(wn::dib_pin::mains_connected); // into mains idle on wake up

    scheduler->msg(&tick_msg);

    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_1 ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::valve_2 ),1); // air bleed valve open
    EXPECT_EQ(stubgpio->get( wn::dib_pin::compressor ),0);
    EXPECT_EQ(stubgpio->get( wn::dib_pin::fan_on ),0);
    EXPECT_EQ(scheduler->m_regCtrl, 4);
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::STATE_SLEEP);

}


// test that the pc is power off over a minute before it can be tured on again
TEST_F(Scheduler_fixture, turn_off_mains) {
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_TURN_OFF_MAINS;
    stubgpio->clr(wn::dib_pin::force_run_on_battery_n);
    stubgpio->set(wn::dib_pin::relay_on_230vac);
    stubgpio->set(wn::dib_pin::power_to_pc);

    scheduler->msg(&tick_msg);

    EXPECT_TRUE(stubgpio->get(wn::dib_pin::force_run_on_battery_n));
    EXPECT_FALSE(stubgpio->get(wn::dib_pin::relay_on_230vac));
    EXPECT_FALSE(stubgpio->get(wn::dib_pin::power_to_pc));

    EXPECT_EQ(scheduler->m_ucState, wn::scheduler_state::STATE_TURN_OFF_MAINS);

    // press power on should be ignored for a minute
    stubgpio->clr(wn::dib_pin::power_on);
    auto ms_elapsed = 0;
    while( scheduler->m_ucStateNxt == wn::scheduler_state::STATE_TURN_OFF_MAINS) {
        ms_elapsed+=10;
        scheduler->msg(&tick_msg);
    }
    EXPECT_EQ(ms_elapsed , (wn::STATE_PC_POWER_OFF_SEC+1)*1000+10);
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::STATE_OFF_MAINS  );



}

// Test off mains where we remain in that state
TEST_F(Scheduler_fixture, off_mains_continue) {
  //std::vector<int> expect_set = {wn::dib_pin::force_run_on_battery_n, wn::dib_pin::valve_2, wn::dib_pin::power_on, wn::dib_pin::pump_brake, wn::dib_pin::mains_connected};
  //std::vector<int> expect_clr = {wn::dib_pin::relay_on_230vac, wn::dib_pin::fan_on, wn::dib_pin::compressor, wn::dib_pin::valve_1, wn::dib_pin::power_to_pc, wn::dib_pin::pump_direction};
  //std::sort(expect_set.begin(),expect_set.end());
  //std::sort(expect_clr.begin(),expect_clr.end());

  //std::shared_ptr<wn::Scheduler>  scheduler = std::make_shared<wn::Scheduler>( mock_trace_if,stubgpio,mockfactorydata,mock_pc_if );
  //// set the mains ON so we stay in the same state
  //stubgpio->set(wn::dib_pin::mains_connected);
  //// set the power button off
  //stubgpio->set(wn::dib_pin::power_on); // active low we want to stay in power off

  //scheduler->m_ucStateNxt = wn::scheduler_state::STATE_OFF_MAINS;

  //scheduler->msg(&tick_msg);
  //auto was_set = stubgpio->getPins_set();
  //auto was_clr = stubgpio->getPins_cleared();
  //EXPECT_EQ(  expect_set, was_set );
  //EXPECT_EQ(  expect_clr, was_clr );
  //// we should stay in the mains off state
  //EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_OFF_MAINS);


}

// Test off mains where we go to idle mains state due to the power on button
TEST_F(Scheduler_fixture, off_mains_to_idle_mains) {
    std::vector<int> expect_set = {wn::dib_pin::force_run_on_battery_n, wn::dib_pin::valve_2,  wn::dib_pin::pump_brake, wn::dib_pin::mains_connected,wn::dib_pin::power_to_pc};
    std::vector<int> expect_clr = {wn::dib_pin::relay_on_230vac, wn::dib_pin::fan_on, wn::dib_pin::compressor, wn::dib_pin::valve_1, wn::dib_pin::pump_direction, wn::dib_pin::power_on,};
    std::sort(expect_set.begin(),expect_set.end());
    std::sort(expect_clr.begin(),expect_clr.end());

    // set the mains ON so we enter idle mains
    stubgpio->set(wn::dib_pin::mains_connected);
    // set the power button on
    stubgpio->clr(wn::dib_pin::power_on); // active low we want to stay in power off

    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_OFF_MAINS;

    scheduler->msg(&tick_msg);
    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(  expect_clr, was_clr );
    // we should stay in the mains off state
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_IDLE_MAINS);

}
// Test off mains where we go to idle mains state due to the power on button
TEST_F(Scheduler_fixture, off_mains_to_idle_battery) {
    std::vector<int> expect_set = {wn::dib_pin::force_run_on_battery_n, wn::dib_pin::valve_2,  wn::dib_pin::pump_brake, wn::dib_pin::power_to_pc};
    std::vector<int> expect_clr = {wn::dib_pin::relay_on_230vac, wn::dib_pin::fan_on, wn::dib_pin::compressor, wn::dib_pin::valve_1, wn::dib_pin::pump_direction, wn::dib_pin::power_on,wn::dib_pin::mains_connected,};
    std::sort(expect_set.begin(),expect_set.end());
    std::sort(expect_clr.begin(),expect_clr.end());

    // set the mains Off so we enter idle battery
    stubgpio->clr(wn::dib_pin::mains_connected);
    // set the power button on
    stubgpio->clr(wn::dib_pin::power_on); // active low we want to stay in power off

    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_OFF_MAINS;

    scheduler->msg(&tick_msg);
    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(  expect_clr, was_clr );
    // we should stay in the mains off state
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_IDLE_BATTERY);

}

// Test off mains where we go to sleep state as the mains has been disconnected
TEST_F(Scheduler_fixture, off_mains_to_sleep) {
    std::vector<int> expect_set = {wn::dib_pin::force_run_on_battery_n, wn::dib_pin::valve_2,  wn::dib_pin::pump_brake,   wn::dib_pin::power_on,};
    std::vector<int> expect_clr = {wn::dib_pin::relay_on_230vac, wn::dib_pin::fan_on, wn::dib_pin::compressor, wn::dib_pin::valve_1, wn::dib_pin::pump_direction,  wn::dib_pin::power_to_pc, wn::dib_pin::mains_connected,};
    std::sort(expect_set.begin(),expect_set.end());
    std::sort(expect_clr.begin(),expect_clr.end());

    // set the mains Off so we enter idle battery
    stubgpio->clr(wn::dib_pin::mains_connected);
    // set the power button off
    stubgpio->set(wn::dib_pin::power_on);

    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_OFF_MAINS;

    scheduler->msg(&tick_msg);
    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(  expect_clr, was_clr );

    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_SLEEP);

}

TEST_F(Scheduler_fixture, power_on_mains_present) {
    std::vector<int> expect_set = {wn::dib_pin::mains_connected, wn::dib_pin::valve_2,wn::dib_pin::pump_brake};
    std::vector<int> expect_clr = {};
    std::sort(expect_set.begin(),expect_set.end());
    std::sort(expect_clr.begin(),expect_clr.end());

    // set the mains On so we enter idle mains
    stubgpio->set(wn::dib_pin::mains_connected);
    // set the power button on
    stubgpio->clr(wn::dib_pin::power_on);
    // start in sleep mode
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_SLEEP;

    scheduler->msg(&poweron_msg);
    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_IDLE_MAINS);
}
TEST_F(Scheduler_fixture, power_on_mains_absent) {
    std::vector<int> expect_set = {wn::dib_pin::valve_2,wn::dib_pin::pump_brake};
    std::vector<int> expect_clr = {};
    std::sort(expect_set.begin(),expect_set.end());
    std::sort(expect_clr.begin(),expect_clr.end());

    // set the power button on
    stubgpio->clr(wn::dib_pin::power_on);
    // start in sleep mode
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_SLEEP;

    scheduler->msg(&mainstoggle_msg);
    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_IDLE_BATTERY);
}


// test action on loss of mains power while in active state
TEST_F(Scheduler_fixture, main_disconnected) {
    std::vector<int> expect_set = {};
    std::vector<int> expect_clr = {wn::dib_pin::power_on, wn::dib_pin::force_run_on_battery_n};
    std::sort(expect_set.begin(),expect_set.end());
    std::sort(expect_clr.begin(),expect_clr.end());

    // set the power button on
    stubgpio->clr(wn::dib_pin::power_on);
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_RUNNING;

    scheduler->msg(&mainstoggle_msg);
    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_IDLE_BATTERY);
    EXPECT_EQ(scheduler->m_stateCntStopProcessLostPower,1);

}

TEST_F(Scheduler_fixture, read_versions) {
    wn::scheduler_msg::msg query_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::serial_rx,0x55,0x00,wn::dib_register::ADR_SW_HW_READ,0x0000);
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_RUNNING;
    mockfactorydata->set_hw_sn(0x01020304);
    mockfactorydata->set_hw_manu(0xA1);
    mockfactorydata->set_hw_manu_year(19);
    mockfactorydata->set_hw_manu_month(12);
    mockfactorydata->set_hw_ver_major(0x02);
    mockfactorydata->set_hw_ver_minor(0x03);
    mockfactorydata->set_board_variant(0x1);

    scheduler->msg(&query_msg);
    // test that we receive a framed multi variable block with the set data from the factory stored values
    auto rx = mock_pc_if->fifo();
    uint8_t checksum{0};
    EXPECT_EQ(rx.front(), wn::dx2_serial_if::serport_start_stop_flag);
    EXPECT_EQ(rx.back() , wn::dx2_serial_if::serport_start_stop_flag);
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0x55);  // fixed address
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0x00);  // not used
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), wn::ADR_SW_HW_READ);  // the read instruction
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), wn::hw_pn_byte2);  // pn byte2
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), wn::hw_pn_byte1);  // pn byte1
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), wn::hw_pn_byte0);  // pn byte0
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0x00);  // fixed
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0x02);  // hw ver major
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0x03);  // hw ver minor
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0x00);  // fixed
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0xa1);  // factData.hw_manufacturer;
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 19);  //factData.hw_manu_year;
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 12);  //factData.hw_manu_month;
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 3 );  //factData.hw_sn_byte1;
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 4);  //factData.hw_sn_byte0;
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), wn::sw_ver_major);  // >  sw version major;
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), wn::sw_ver_minor);  //    sw version minor
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 1);  // >  board variant
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0);  // unused
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), 0);  //  unuseed
    checksum+=rx.front();
    rx.pop_front();
    EXPECT_EQ(rx.front(), checksum);
}

TEST_F(Scheduler_fixture, read_sensor_ctrl_read) {
    // ADR_SENSOR_CTRL_READ
    wn::scheduler_msg::msg query_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::serial_rx,0x55,0x00,wn::dib_register::ADR_SW_HW_READ,0x0000);
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_RUNNING;

}



// test entering test mode
TEST_F(Scheduler_fixture, test_mode) {

    wn::scheduler_msg::msg query_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::serial_rx,0x55,0x00,wn::dib_register::ADR_COMMAND + 0x80, 1 << wn::BIT_COMMAND_TEST_MODE_START  );
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_RUNNING;

    EXPECT_EQ(query_msg.m_value, 1 << wn::BIT_COMMAND_TEST_MODE_START);
    scheduler->msg(&query_msg);

    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_TEST);

}

// Test setting pump brake line from control test interface
TEST_F(Scheduler_fixture, pump_brake_ctrl) {
    std::vector<int> expect_set = {wn::dib_pin::pump_brake};
    std::vector<int> expect_clr = {wn::dib_pin::pump_direction};

    wn::scheduler_msg::msg query_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::serial_rx,0x55,0x00,wn::dib_register::ADR_COMMAND + 0x80, 1 << wn::BIT_COMMAND_TEST_MODE_START  );
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_RUNNING;
    scheduler->msg(&query_msg);

    EXPECT_EQ(scheduler->m_ucStateNxt, wn::scheduler_state::STATE_TEST);
    scheduler->m_ucState = wn::scheduler_state::STATE_TEST;

     //EXPECT_CALL(*mockpwm,start(0));
    // set brake on
    wn::scheduler_msg::msg ctrl_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::serial_rx,0x55,0x00,wn::dib_register::ADR_PUMP_CTRL + 0x80, 1 << wn::dib_pump_ctrl_regiser::BIT_PUMP_CTRL_BRAKE  );
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_RUNNING;
    scheduler->msg(&ctrl_msg);

    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(  expect_clr, was_clr );
}


// Test setting of the pump speed to 25% and setting fwd direction
TEST_F(Scheduler_fixture, pump_speed_ctrl) {
    std::vector<int> expect_set = {wn::dib_pin::pump_direction};
    std::vector<int> expect_clr = {wn::dib_pin::pump_brake};

    scheduler->m_ucState = wn::scheduler_state::STATE_TEST;

    EXPECT_CALL(*mockpwm,start(25));
    // set direction fwd , speed 25% based on full range of legacy system
    wn::scheduler_msg::msg ctrl_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::serial_rx,0x55,0x00,wn::dib_register::ADR_PUMP_CTRL + 0x80, 1 << wn::dib_pump_ctrl_regiser::BIT_PUMP_CTRL_DIR | (wn::BIT_MASK_PUMP_CTRL_PWM >> 2) );
    scheduler->m_ucStateNxt = wn::scheduler_state::STATE_RUNNING;
    scheduler->msg(&ctrl_msg);

    auto was_set = stubgpio->getPins_set();
    auto was_clr = stubgpio->getPins_cleared();
    EXPECT_EQ(  expect_set, was_set );
    EXPECT_EQ(  expect_clr, was_clr );

    was_set.clear();
    was_clr.clear();

}

TEST_F(Scheduler_fixture, battery_ctrl) {

}




//TEST_F(Scheduler_fixture, target_test_pump_ctrl) {
//    protocol_port = open_port(m_test_port);
//    auto res = c_serial_flush(protocol_port);

//    uint8_t checksum{0};
//    uint8_t rx_buffer[128];
//    c_serial_control_lines_t lines;


//    create_frame(wn::dib_register::ADR_COMMAND + 0x80, 1 << wn::BIT_COMMAND_TEST_MODE_START );
//    int len = frame_len;
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";


//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

//    // set the direction forward
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80, 1 << wn::dib_pump_ctrl_regiser::BIT_PUMP_CTRL_DIR );
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    EXPECT_EQ(res,0);

//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80, 0 );
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";

//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80, 1 << wn::dib_pump_ctrl_regiser::BIT_PUMP_CTRL_BRAKE );
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";

//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80, 0 );
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " b"
//                                     "tes to the target res = " << res << "\r\n";


//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80,  0x100);
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";


//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80,  0x200);
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";


//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80,  0x900);
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";


//    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//    create_frame(wn::dib_register::ADR_PUMP_CTRL +0x80, 0 );
//    res = c_serial_write_data(protocol_port,frame_buf,&len);
//    std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";


//    close_port(protocol_port);


//}


// BIT_COMMAND_TEST_MODE_START1

//TEST_F(Scheduler_fixture, target_reader) {


//    protocol_port = open_port(m_test_port);

//    for(auto j=0;j<1;j++) {

//        auto res = c_serial_flush(protocol_port);
//        std::cout << "flushed with the result " << res << "\r\n";

//        uint8_t checksum{0};
//        uint8_t rx_buffer[128];
//        c_serial_control_lines_t lines;

//        // ADR_SENSOR_CTRL_READ
//        wn::scheduler_msg::msg query_msg = wn::scheduler_msg::msg(wn::scheduler_msg::msg_types::serial_rx,0x55,0x00,wn::dib_register::ADR_SW_HW_READ,0x0000);
//        std::cout << "***target testing\r\n";
//        create_frame(wn::dib_register::ADR_SW_HW_READ,0x0000);
//        int len = frame_len;
//         res = c_serial_write_data(protocol_port,frame_buf,&len);
//        std::cout << " Wrote " << len << " bytes to the target res = " << res << "\r\n";

//        int available_to_read{0};
//        for(;;) {
//            std::this_thread::sleep_for(std::chrono::milliseconds(100));
//            res = c_serial_get_available(protocol_port, &available_to_read);
//            if (available_to_read)
//                break;

//        }

//        res = c_serial_read_data(protocol_port,
//                                 &rx_buffer,
//                                 &available_to_read,
//                                 &lines );



//        size_t rp{0};
//        EXPECT_EQ(rx_buffer[rp++], wn::dx2_serial_if::serport_start_stop_flag);
//        EXPECT_EQ(rx_buffer[rp++], 0x55);
//        EXPECT_EQ(rx_buffer[rp++], 0x00);
//        EXPECT_EQ(rx_buffer[rp++], 0x0f);
//        EXPECT_EQ(rx_buffer[rp++], wn::hw_pn_byte2); // pn byte2
//        EXPECT_EQ(rx_buffer[rp++], wn::hw_pn_byte1); // pn byte1
//        EXPECT_EQ(rx_buffer[rp++], wn::hw_pn_byte0); // pn byte0
//        std::cout << "OK\r\n";


//    }
//    close_port(protocol_port);


//}




