#ifndef MOCKWATCHDOG_H
#define MOCKWATCHDOG_H

#include "interface/watchdog.hpp"

class MockWatchdog : public Watchdog
{
public:
    MockWatchdog();
};

#endif // MOCKWATCHDOG_H
