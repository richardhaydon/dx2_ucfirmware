#ifndef MOCK_TEMPERATURE_SENSOR_HPP
#define MOCK_TEMPERATURE_SENSOR_HPP

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>
#include "temperature_sensor.hpp"


class mock_temperature_sensor {
public:
    virtual ~mock_temperature_sensor() = default;

    MOCK_METHOD0(read_temperature, int32_t());

};

#endif // MOCK_TEMPERATURE_SENSOR_HPP
