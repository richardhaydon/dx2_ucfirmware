#ifndef MOCK_PWM_IF_HPP
#define MOCK_PWM_IF_HPP

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>

#include "pwm_if.hpp"

class mock_pwm_if : public wn::pwm_if {
public:
    mock_pwm_if() = default;
    virtual ~mock_pwm_if() = default;

    MOCK_METHOD2(run,uint32_t(uint16_t,uint32_t));
    MOCK_METHOD1(start,uint32_t(uint16_t));
    MOCK_METHOD0(stop,uint32_t());

};

#endif // MOCK_PWM_IF_HPP
