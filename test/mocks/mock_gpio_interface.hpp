#ifndef MOCK_GPIO_INTERFACE_HPP
#define MOCK_GPIO_INTERFACE_HPP


#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>
#include <string>
#include <bitset>
#include <algorithm>

#include <list>
#include <mutex>

#include "gpio_if.hpp"

/**
 * @brief The mock_gpio_interface class
 */
class mock_gpio_interface : public wn::gpio_if {
 public:
    virtual ~mock_gpio_interface() = default;
    MOCK_METHOD1(set,void(unsigned int));
    MOCK_METHOD1(clr,void(unsigned int));
    MOCK_METHOD1(get,int(int));
    MOCK_METHOD0(init_pins_run_mode,void());
    MOCK_METHOD0(init_pins_sleep_mode,void());

};


class stub_gpio_interface : public wn::gpio_if {
public:
    virtual ~stub_gpio_interface() = default;
    void set(unsigned int i) override {
        m_pins_set.push_back(i);
        std::sort( m_pins_set.begin(), m_pins_set.end() );
        m_pins_set.erase( unique( m_pins_set.begin(), m_pins_set.end() ), m_pins_set.end() );
        m_bits[i] = true;
    };
    void clr(unsigned int i) override {
       m_bits[i] = false;
	   m_pins_cleared.push_back(i);
	   std::sort( m_pins_cleared.begin(), m_pins_cleared.end() );
       m_pins_cleared.erase( unique( m_pins_cleared.begin(), m_pins_cleared.end() ), m_pins_cleared.end() );
    };
    int get(int i) override {
        return m_bits[i];
    };

    void init_pins_run_mode() override {

    }
    void init_pins_sleep_mode() override {}

    std::bitset<64> m_bits = {0};

    std::vector<int> m_pins_set;
    std::vector<int> m_pins_cleared;


    std::vector<int> getPins_set() const;
    std::vector<int> getPins_cleared() const;
};

#endif  // MOCK_GPIO_INTERFACE_HPP

std::vector<int> stub_gpio_interface::getPins_cleared() const
{
    return m_pins_cleared;
}

std::vector<int> stub_gpio_interface::getPins_set() const
{
return m_pins_set;
}
