#include "mock_power_monitor.hpp"


uint32_t stub_power_monitor::set_power_mode(bool normal) {
    return 0;
}

uint32_t stub_power_monitor::config() {
    return 0;
}

uint32_t stub_power_monitor::get_current_A(float &amps) {
    return m_current;
}

uint32_t stub_power_monitor::get_sense_V(float &volts) {
    return m_vshunt;
}

uint32_t stub_power_monitor::get_bus_V(float &volts) {
    return m_vbus;
}

void stub_power_monitor::setModel(const std::string &model)
{
    m_model = model;
}

float stub_power_monitor::getVbus() const
{
    return m_vbus;
}

void stub_power_monitor::setVshunt(float vshunt)
{
    m_vshunt = vshunt;
}

void stub_power_monitor::setCurrent(float current)
{
    m_current = current;
}
