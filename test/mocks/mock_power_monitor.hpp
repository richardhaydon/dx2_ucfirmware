#ifndef MOCK_POWER_MONITOR_HPP
#define MOCK_POWER_MONITOR_HPP

#include <stdint.h>
#include "power_monitor.hpp"
#include <gmock/gmock.h>
#include <gtest/gtest.h>


class mock_power_monitor : public wn::PowerMonitor
{
public:
    virtual ~mock_power_monitor() {}

    MOCK_METHOD1(set_power_mode,uint32_t(bool));
    MOCK_METHOD0(get_model,std::string(void));
    MOCK_METHOD0(config, uint32_t());
    MOCK_METHOD1(get_current_A,uint32_t(float &amps));
    MOCK_METHOD1(get_sense_V,uint32_t(float &amps));
    MOCK_METHOD1(get_bus_V,uint32_t(float &amps));

};

class stub_power_monitor : public wn::PowerMonitor
{
public:
    virtual ~stub_power_monitor() {}
    uint32_t set_power_mode(bool normal) override;

    std::string get_model() override {
        return m_model;
    }

    uint32_t config() override;

    uint32_t get_current_A(float &amps) override;

    uint32_t get_sense_V(float &volts) override;

    uint32_t get_bus_V(float &volts) override;

    void setModel(const std::string &model);

    float getVbus() const;

    void setVshunt(float vshunt);

    void setCurrent(float current);

private:
    std::string m_model;
    float m_vbus;
    float m_vshunt;
    float m_current;


};



#endif // MOCK_POWER_MONITOR_HPP
