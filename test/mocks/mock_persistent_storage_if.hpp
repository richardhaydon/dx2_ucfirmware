#ifndef MOCK_PERSISTENT_STORAGE_IF_HPP
#define MOCK_PERSISTENT_STORAGE_IF_HPP

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>
#include "persistent_storage_if.hpp"
#include <vector>


namespace wn {

class mock_persistent_storage_if : public persistent_storage_if {
public:
    mock_persistent_storage_if(size_t extent) : persistent_storage_if{extent} {}
    virtual ~mock_persistent_storage_if() override = default;
    virtual uint32_t save(const std::vector<uint32_t> &data) override {
        m_mock_content = data;
        return m_next_result;
    }

    virtual std::vector<uint32_t> read() const override {
        //    std::cout << "returned mock content with size " << m_mock_content.size()
        //              << std::endl;
        return m_mock_content;
    }

    uint32_t next_result() const { return m_next_result; }

    void setNext_result(const uint32_t &next_result) {
        m_next_result = next_result;
    }

    void setMockContent(const std::vector<uint32_t> content) {
        m_mock_content = content;
        //    std::cout << "setting mock content with size " << m_mock_content.size()
        //              << std::endl;
    }

protected:
    std::vector<uint32_t> m_mock_content;
    uint32_t m_next_result{};
};
} // namespace wn

#endif // MOCK_PERSISTENT_STORAGE_IF_HPP
