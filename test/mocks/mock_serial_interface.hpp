#ifndef MOCK_SERIAL_INTERFACE_HPP
#define MOCK_SERIAL_INTERFACE_HPP


#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>
#include <string>

#include <list>
#include <mutex>

#include "serial_interface.hpp"

/**
 * @brief The mock_serial_interface class
 */
class mock_serial_interface : public wn::serial_interface {
 public:
    virtual ~mock_serial_interface() = default;
    MOCK_METHOD1(tx,uint32_t(uint8_t));

    MOCK_METHOD1(tx,uint32_t(std::string));

    /**
     * @brief tx
     * @param p pointer to data to send
     * @param len of data to send
     * @return error code from nrf sdk
     */

    uint32_t tx(uint8_t *data,size_t sz) {
        for(auto i=0;i<sz;i++) {
            m_fifo.push_back(data[i]);
        }
        return  m_last_result;
    }

    MOCK_METHOD4(rx, uint32_t(uint8_t * p_data,size_t size,size_t * p_read, uint32_t timeout_ms ));

    uint32_t rx(uint8_t *c) override {
       while(m_fifo.empty());
       m_fifo_lock.lock();
       *c = m_fifo.front();
       m_fifo.pop_front();
       m_fifo_lock.unlock();

       return 0;
    }

    // Provides a method of populating the response of the mock
    void feed(uint8_t c) {
      m_fifo_lock.lock();
      try {
        m_fifo.push_back(c);
      } catch (...) {
        std::cout << "failed in the mock\n";
      }
      m_fifo_lock.unlock();
    }

    std::list<uint8_t> fifo() const {return  m_fifo;}

private:
    uint32_t m_last_result{0};
    std::list<uint8_t> m_fifo;
    std::mutex m_fifo_lock;

};



#endif  // MOCK_SENSOR_BOARD_INTERFACE_HPP

