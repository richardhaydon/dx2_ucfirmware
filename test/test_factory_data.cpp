// Title: WideNorth Framework test of factory data class
//
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw, Gtest, Rapidcheck
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <iomanip>

#define protected public
#include "dx2_factory_data.hpp"
#undef protected

#include "mocks/mock_persistent_storage_if.hpp"

#include <exception>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>
#include <vector>

using testing::_;
using testing::Return;
using testing::Throw;

using namespace wn;

class Factory_data_fixture : public ::testing::Test {

protected:
  Factory_data_fixture() {
    good[factory_data::dx_data_offset::check_word] = calc_cw(good);
  }

  std::shared_ptr<mock_persistent_storage_if> flash_small =
      std::make_shared<mock_persistent_storage_if>(9);
  std::shared_ptr<mock_persistent_storage_if> flash_exact =
      std::make_shared<mock_persistent_storage_if>(10);
  std::shared_ptr<mock_persistent_storage_if> flash_excess =
      std::make_shared<mock_persistent_storage_if>(11);

  std::vector<uint32_t> good = {
      0x10000001, // layout
      0x00000001, // hw manu
      0x00000002, // major version
      0x00000003, // minor version
      0x00000004, // manu year
      0x00000005, // manu month
      0x12345678, // hw ser
      0xdeadbeef, // bat cal
      0x00000011, // adc zero offset
      0x00000000  // checkword
  };

  uint32_t calc_cw(std::vector<uint32_t> d) {
    uint32_t res = 0xbeadbabe;
    for (auto i = static_cast<int>(factory_data::dx_data_offset::hw_manu);
         i < static_cast<int>(factory_data::dx_data_offset::check_word); i++) {
      // std::cout << "for [" << i << "] add element 0x" << std::setbase(16) <<
      // good.at(i) << std::endl;
      res ^= good.at(i);
    }
    // std::cout << "checkword is 0x" << std::setbase(16) << res << std::endl;
    return res;
  }
};

// Test that factory data object cannot be created with persistent storage that
// has too small an extent
TEST_F(Factory_data_fixture, extent) {
  // create an instance of storage with capacity for 8 x uint32_t

  // storage is too small for the data type
  ASSERT_ANY_THROW({ wn::factory_data fs(flash_small); });

  // storage is nine words, exact match
  ASSERT_NO_THROW({ wn::factory_data fs(flash_exact); });

  // storage is nine words, excess no problem test
  ASSERT_NO_THROW({ wn::factory_data fs(flash_excess); });
}

TEST_F(Factory_data_fixture, read_invalid) {

  wn::factory_data fs(flash_exact);

  // control the good data case with exact size store
  flash_exact->setMockContent(good);
  EXPECT_EQ(0u, fs.read());


  // control the good data with storage size larger than the structure
  std::shared_ptr<mock_persistent_storage_if> flash_over =
      std::make_shared<mock_persistent_storage_if>(1024);
  wn::factory_data fs_page_dimension(flash_over);

  auto page_data = good;
  for(auto i=0;i<100;i++) {
      page_data.push_back(i);
  }
  std::cout << "Set large mock data\r\n";
  flash_over->setMockContent(page_data);

  EXPECT_EQ(0u, fs_page_dimension.read());


  // test short (blank)data
  flash_exact->setMockContent({});
  EXPECT_EQ(1u, fs.read());

  // test bad version
  auto bad = good;
  bad.at(0) = 0xffffffff;
  flash_exact->setMockContent(bad);
  EXPECT_EQ(2u, fs.read());

  // test bad checkword
  bad = good;
  bad.at(factory_data::dx_data_offset::check_word) = 0xffffffff;
  flash_exact->setMockContent(bad);
  EXPECT_EQ(3u, fs.read());
}

// Test initiation of new writeable content
TEST_F(Factory_data_fixture, init) {
  wn::factory_data fs(flash_exact);
  fs.init();
  // test that created structure is correct size
  EXPECT_EQ(fs.m_dx_data.size(), factory_data::dx_data_offset::check_word + 1);
  // Expect that the layout version has been set
  EXPECT_EQ(fs.m_dx_data.front(), fs.m_layout_version);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::hw_manu), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::hw_ver_major), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::hw_ver_minor), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::hw_manu_year), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::hw_manu_month), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::hw_sn), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::variant), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::spare_01), 0);
  EXPECT_EQ(fs.m_dx_data.at(factory_data::dx_data_offset::check_word), 0);
}

TEST_F(Factory_data_fixture, save) {
  wn::factory_data fs(flash_exact);
  fs.set_hw_manu(1);
  fs.set_hw_ver_major(0);
  fs.set_hw_ver_minor(1);
  fs.set_hw_manu_year(2019);
  fs.set_hw_manu_month(12);
  fs.set_hw_sn(0x000001);
  fs.set_board_variant(1);
  // write to the mock permanent store
  EXPECT_EQ(0,fs.write());
  // read back to a new instance
  wn::factory_data fs2(flash_exact);
  EXPECT_EQ(0u, fs2.read());
  // test values match
  EXPECT_EQ(fs2.get_hw_manu(), fs.get_hw_manu());
  EXPECT_EQ(fs2.get_hw_ver_major(), fs.get_hw_ver_major());
  EXPECT_EQ(fs2.get_hw_ver_minor(), fs.get_hw_ver_minor());
  EXPECT_EQ(fs2.get_hw_manu_year(), fs.get_hw_manu_year());
  EXPECT_EQ(fs2.get_hw_manu_month(), fs.get_hw_manu_month());
  EXPECT_EQ(fs2.get_hw_sn(), fs.get_hw_sn());
  EXPECT_EQ(fs2.get_board_variant(), fs.get_board_variant());
}
