// Title: WideNorth Framework test of factory data class
//
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw, Gtest, Rapidcheck
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <iomanip>

#define protected public
#include "dx2_serial_if.hpp"
#undef protected

#include <exception>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>
#include <vector>

using testing::_;
using testing::Return;

using namespace wn;

class Serial_protocol_fixture : public ::testing::Test {

protected:
  Serial_protocol_fixture() {}

  dx2_serial_if m_port;
};

// Test defautl constructor and destuctor of instance check initial stats
TEST_F(Serial_protocol_fixture, default_state) {
  EXPECT_FALSE(m_port.ready());

  EXPECT_EQ(m_port.get_rx_ok_cnt(), 0);
  EXPECT_EQ(m_port.get_rx_msg_cnt(), 0);
  EXPECT_EQ(m_port.get_rx_checksum_err(), 0);
  EXPECT_EQ(m_port.get_rx_oflowcnt(), 0);
  EXPECT_EQ(m_port.get_rx_wrong_stopflag(), 0);
  EXPECT_EQ(m_port.get_rx_write_reg_err(), 0);
  EXPECT_EQ(m_port.get_rx_read_reg_err(), 0);
}

// Test non reaction to noise
RC_GTEST_PROP(messages, ctrl_messages, (std::vector<uint8_t> noise)) {
  dx2_serial_if port;
  for (auto const &i : noise) {
    RC_ASSERT(port.rx(i) == false);
  }
}

TEST_F(Serial_protocol_fixture, complete_msg) {
// test initial complete message
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_FALSE(m_port.rx(0x55));  // addr
    EXPECT_FALSE(m_port.rx(0x01));  // ctrl
    EXPECT_FALSE(m_port.rx(0x02));  // cmd
    EXPECT_FALSE(m_port.rx(0x01));  // val hi
    EXPECT_FALSE(m_port.rx(0x03));  // val lo
    EXPECT_FALSE(m_port.rx( (0x55 + 0x01 + 0x02 + 0x01 + 0x03) & 0xFF   ));  // check
    EXPECT_TRUE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_TRUE(m_port.ready());

    scheduler_msg::msg rx_msg = m_port.get_rx_msg();
    EXPECT_EQ(rx_msg.m_address, 0x55);
    EXPECT_EQ(rx_msg.m_value, 0x0103);
    EXPECT_EQ(rx_msg.m_command,0x02);
    EXPECT_EQ(rx_msg.m_control,0x01);


// follow on message
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_FALSE(m_port.rx(0x55));  // addr
    EXPECT_FALSE(m_port.rx(0x01));  // ctrl
    EXPECT_FALSE(m_port.rx(0x02));  // cmd
    EXPECT_FALSE(m_port.rx(0xaa));  // val hi
    EXPECT_FALSE(m_port.rx(0xbb));  // val lo
    EXPECT_FALSE(m_port.rx( (0x55 + 0x01 + 0x02 + 0xaa + 0xbb) & 0xFF   ));  // check
    EXPECT_TRUE(m_port.rx(dx2_serial_if::serport_start_stop_flag));

    rx_msg = m_port.get_rx_msg();
    EXPECT_EQ(rx_msg.m_address, 0x55);
    EXPECT_EQ(rx_msg.m_value, 0xaabb);
    EXPECT_EQ(rx_msg.m_command,0x02);
    EXPECT_EQ(rx_msg.m_control,0x01);

// test statistics for this case
    EXPECT_EQ(m_port.get_rx_ok_cnt(),2)    ;
    EXPECT_EQ(m_port.get_rx_msg_cnt(),2)    ;
    EXPECT_EQ(m_port.get_rx_oflowcnt(),0)    ;
    EXPECT_EQ(m_port.get_rx_wrong_stopflag(),0)    ;
    EXPECT_EQ(m_port.get_rx_checksum_err(),0)    ;

}

TEST_F(Serial_protocol_fixture, bad_checksum) {
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_FALSE(m_port.rx(0x55));  // addr
    EXPECT_FALSE(m_port.rx(0x01));  // ctrl
    EXPECT_FALSE(m_port.rx(0x02));  // cmd
    EXPECT_FALSE(m_port.rx(0x01));  // val hi
    EXPECT_FALSE(m_port.rx(0x03));  // val lo
    EXPECT_FALSE(m_port.rx( (0x55 + 0x01 + 0x02 + 0x01 + 0x03) & 0x0F   ));  // check sum hi nibble zeroed
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_FALSE(m_port.ready());

    // test statistics for this bad crc
    EXPECT_EQ(m_port.get_rx_ok_cnt(),0)    ;
    EXPECT_EQ(m_port.get_rx_msg_cnt(),1)    ;
    EXPECT_EQ(m_port.get_rx_oflowcnt(),0)    ;
    EXPECT_EQ(m_port.get_rx_wrong_stopflag(),0)    ;
    EXPECT_EQ(m_port.get_rx_checksum_err(),1)    ;

    // test a follow on message
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_FALSE(m_port.rx(0x55));  // addr
    EXPECT_FALSE(m_port.rx(0x01));  // ctrl
    EXPECT_FALSE(m_port.rx(0x02));  // cmd
    EXPECT_FALSE(m_port.rx(0xaa));  // val hi
    EXPECT_FALSE(m_port.rx(0xbb));  // val lo
    EXPECT_FALSE(m_port.rx( (0x55 + 0x01 + 0x02 + 0xaa + 0xbb) & 0xFF   ));  // check
    EXPECT_TRUE(m_port.rx(dx2_serial_if::serport_start_stop_flag));

    auto rx_msg = m_port.get_rx_msg();
    EXPECT_EQ(rx_msg.m_address, 0x55);
    EXPECT_EQ(rx_msg.m_value, 0xaabb);
    EXPECT_EQ(rx_msg.m_command,0x02);
    EXPECT_EQ(rx_msg.m_control,0x01);

    // test statistics for accumulated results
    EXPECT_EQ(m_port.get_rx_ok_cnt(),1)    ;
    EXPECT_EQ(m_port.get_rx_msg_cnt(),2)    ;
    EXPECT_EQ(m_port.get_rx_oflowcnt(),0)    ;
    EXPECT_EQ(m_port.get_rx_wrong_stopflag(),0)    ;
    EXPECT_EQ(m_port.get_rx_checksum_err(),1)    ;


}

TEST_F(Serial_protocol_fixture, bad_endflag) {
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_FALSE(m_port.rx(0x55));  // addr
    EXPECT_FALSE(m_port.rx(0x01));  // ctrl
    EXPECT_FALSE(m_port.rx(0x02));  // cmd
    EXPECT_FALSE(m_port.rx(0x01));  // val hi
    EXPECT_FALSE(m_port.rx(0x03));  // val lo
    EXPECT_FALSE(m_port.rx( (0x55 + 0x01 + 0x02 + 0x01 + 0x03) & 0xFF   ));
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag & 0x0F)); // flag hi nibble zeroed
    EXPECT_FALSE(m_port.ready());

    // test statistics for this bad crc
    EXPECT_EQ(m_port.get_rx_ok_cnt(),0)    ;
    EXPECT_EQ(m_port.get_rx_msg_cnt(),1)    ;
    EXPECT_EQ(m_port.get_rx_oflowcnt(),0)    ;
    EXPECT_EQ(m_port.get_rx_wrong_stopflag(),1)    ;
    EXPECT_EQ(m_port.get_rx_checksum_err(),0)    ;

    // test a follow on message
    EXPECT_FALSE(m_port.rx(dx2_serial_if::serport_start_stop_flag));
    EXPECT_FALSE(m_port.rx(0x55));  // addr
    EXPECT_FALSE(m_port.rx(0x01));  // ctrl
    EXPECT_FALSE(m_port.rx(0x02));  // cmd
    EXPECT_FALSE(m_port.rx(0xaa));  // val hi
    EXPECT_FALSE(m_port.rx(0xbb));  // val lo
    EXPECT_FALSE(m_port.rx( (0x55 + 0x01 + 0x02 + 0xaa + 0xbb) & 0xFF   ));  // check
    EXPECT_TRUE(m_port.rx(dx2_serial_if::serport_start_stop_flag));

    auto rx_msg = m_port.get_rx_msg();
    EXPECT_EQ(rx_msg.m_address, 0x55);
    EXPECT_EQ(rx_msg.m_value, 0xaabb);
    EXPECT_EQ(rx_msg.m_command,0x02);
    EXPECT_EQ(rx_msg.m_control,0x01);

    // test statistics for accumulated results
    EXPECT_EQ(m_port.get_rx_ok_cnt(),1)    ;
    EXPECT_EQ(m_port.get_rx_msg_cnt(),2)    ;
    EXPECT_EQ(m_port.get_rx_oflowcnt(),0)    ;
    EXPECT_EQ(m_port.get_rx_wrong_stopflag(),1)    ;
    EXPECT_EQ(m_port.get_rx_checksum_err(),0)    ;


}







