# DX2sensorboard with nRF52833

Firmware and tools for the DeconX sensor board development

# building for the target
<details to be included>
Install the SDK from https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK to a folder immediately off your filesystem root. 
Install the gnu arm tool chain, and the nrf command line tools.
Tested with arm gcc version 9 at https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
The debugger uses python, tested with 3.8.0
Modification of the sdk uses CMIS_configuration_wizard
Install cmake for your target, minimum version 3.6



checkout this repository, include submodules with git submodules update --init --recursive
Cmake generates an intel hex file, without soft device.

Make an out of source build in /bin
mkdir bin 
cd bin
cmake -DCMAKE_BUILD_TYPE="Release"  -G"Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="../cmake/arm-gcc-toolchain.cmake"   ..
make 
make flash

# building for host
Without specifying the arm toolchain the project will build for your host. This allows running of unit tests. 
Testing is enable for the host build, in QT the tests will automatically be registered.  

# included tools
We use GTest, RapidCheck and GMock as the unit testing framework


# IDE configuration

The CmakeLists.txt file in this project generates a selected build sytems. In the example line above that is Unix Make files. 
Specify the generator to use with -G" build system " , by default on windows a visual studio project will be the result. 
Segger studio projects can also be generated with -G"tbd"

Qt can be configured to build the target system as well as the unit tests on the hosts.

a) Enable bare metal device: About plugins -> devices supported -> bare metal (check)
b) create a kit for the target build, name rf52, type bare metal
c) specify arm compilers, however we should be over writing this with the tool chain file
d) specify Unix Makefiles as the build system
e) In cmake configuration -> change and set 

CMAKE_PREFIX_PATH:STRING=%{Qt:QT_INSTALL_PREFIX}
CMAKE_SYSTEM_NAME:INTERNAL=Generic
CMAKE_SYSTEM_PROCESSOR:INTERNAL=ARM
CMAKE_TOOLCHAIN_FILE:INTERNAL=./cmake/arm-gcc-toolchain.cmake
QT_QMAKE_EXECUTABLE:STRING=%{Qt:qmakeExecutable}

# Target debug with QT


Install Python

    Because QtCreator uses Python inside gdb.
    Install version 2.7.11
    Add to environnement these variables:
    PYTHONHOME = C:\Python27
    PYTHONPATH = C:\Python27\Lib

Install GUN ARM toolchain

Here: https://launchpad.net/gcc-arm-embedded/+download

Version 5.3-2016-q1
Install GDB Server

Install SEGGER J-Link GDB Server V5.10u
Install Terminal

To receive/send message via UART. Install RealTerm: Serial Capture Program 2.0.0.70
Install Haroopad

Because it's cool to write documents with!
Configure QtCreator
New GDB Server

    Open Help > About Plugins...

    In Device Support section, enable BareMetal (v 4.0.0)

    Retart QtCreator

    Open Tools > Options

    In left pane, click BareMetal

    Click Add > OpenOCD (even if we don't use OpenOCD)

    My config:

    Name = JLink GDB Server

    Startup mode = No Startup

    Host = localhost ; 2331

    Init cmds =

    set remote hardware-breakpoint-limit 6

    set remote hardware-watchpoint-limit 6 #(Not sure about limit)

    monitor semihosting enable

    load

    monitor reset 0

    Reset cmds = monitor reset 0

New Device

    In Options left pane, go to Devices
    Click Add, select Bare Metal Device and Start Wizard
    Name = nrf52 device
    GDB Server provider = JLink GDB Server (It's our previous config !)

Config GDB

    In Options left pane, goto Debugger
    Options I enabled (it works with them, maybe some of them are useless):
    General tab: Use tooltips ; Warn when debugging Release
    GDB tab: Adjust bp locations ; Use dynamic object type ; Load .gdbinit ; Load pretty printers
    GDB Extend: Use asynchronous mode to control the inferior (or QtCreator will crash when you pause execution or set breakpoints while it's running.) ; Use common location for debug information.

New Debugger

    Options left pane, go to Build & Run

    Go to Debugger tab

    Add a new one

    Name = arm-gdb-py

    Path = C:\nrf\toolchain\gnu-arm\5.3-2016q1\bin\arm-none-eabi-gdb-py.exe (QtCreator needs python-enabled GDB)

    Click Apply

New Kit

    Go to Kits tab

    Click Add

    Name = NRF52

    Device type = Bare Metal Device

    Device = nrf52 device

    Compiler = whatever, I use Makefile

    Debugger = arm-gdb-py

    Qt Version = None

    Close and validate Options

Configure new project

    Create a new project, and import an existing project. It will allow you to use Makefile.
    In QtCreator left pane, click on Projects
    Add a Kit NRF52
    To Build (in Build tab), I use the command line

 make -rRj VERBOSE=0 nrf52

    To Run (in Run tab), add a Run configuration Custom Executable (on GDB server or HW db). Actually, "run" means that QtCreator will load the firmware into the target via GDB.
    In Executable, enter path to the ".out" file. If you have never compiled, it is not existent yet.


# gdb server

run using the swd link
PS C:\Program Files (x86)\SEGGER\JLink> .\JLinkGDBServerCL.exe -if swd -device nRF5

troubleshooting
When adding the gdb-py debugger ensure that it is recognised as gdb.
If not try running the program from a console and act on any errors noticed.







