// Title: DX2 Sensor board
//
// Description: Scheduler task
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#include "wn_common.hpp"
#include "scheduler.hpp"
#include "scheduler_msg.hpp"
#include "dx2_pins.hpp"
#include "dx2_serial_if.hpp"
#include "dib_version.hpp"

namespace wn {


Scheduler::Scheduler(std::shared_ptr<serial_interface> com, std::shared_ptr<gpio_if> gpio, std::shared_ptr<factory_data> fact_data, std::shared_ptr<serial_interface> pc_com, std::shared_ptr<wn::pwm_if> pump_pwm, std::shared_ptr<wn::PowerMonitor> power_monitor, std::shared_ptr<wn::Watchdog> wdog) : m_com{com}, m_gpio{gpio}, m_factory_data{fact_data},m_pc_com{pc_com}, m_pump_pwm {pump_pwm}, m_power_monitor{power_monitor}, m_watchdog{wdog}
{

}


void Scheduler::init()
{
    m_com->tx("Scheduler task starts\r\n");
    m_watchdog->enable();
    //@TODO WDTCONbits.SWDTEN = 1; // Enable Watchdog (4seconds loop)
    //@TODOInit(); //Initialize processor registers
    m_gpio->init_pins_run_mode();


}

void Scheduler::tick()
{
    //@TODO ClrWdt(); //Note: The WDT and all its prescalers are reset with ANY device Reset.
    m_watchdog->reset();

        // setting next state from IRQ and from scheduler
    if (m_ucStateSetIrq && (m_ucState != scheduler_state::STATE_BOOT))
    {
        m_ucState = m_ucStateNxtIrq;
        m_ucStateSetIrq = 0;
    }
    else
    {
        // Count number of seconds in this state and update counter for last poll from host
        if (m_pps_pulse)
        {
            m_ucStateSecCnt++;
            m_HostPollSecCnt++;
        }

        // Reset counter when change uC state
        if (m_ucState != m_ucStateNxt) {
            m_ucStateSecCnt = 0;
        }
        m_ucState = m_ucStateNxt;
    }

    switch (m_ucState) {
    case scheduler_state::STATE_BOOT               :
        m_com->tx("STATE_BOOT\r\n");

        // Put devices controlled in known state
        if (m_gpio->get(wn::dib_pin::power_on) == HIGH) { // Active low
            Init_io_low_power();
        }
        else {
            Init_io_power_on(ON);
        }
        m_regCtrl = 0; // All valves closed and all relays off
        //Init_TempHum_Sensor(); not present on dib
        CtrlWrite(m_regCtrl);  // this is not needed we write also in Process Stop
        ProcessStop();

        if (m_factory_data->read()) {
            m_com->tx("Failed reading factory flash settings save defaults\r\n");
        }

        m_BattCntSec = 0;

        if (  m_gpio->get(wn::dib_pin::power_on) == HIGH) // Active low
        {
            m_ucStateNxt = STATE_SLEEP;
        }
        else
        {
            if ( m_gpio->get(wn::dib_pin::mains_connected))
            {
                m_ucStateNxt = STATE_IDLE_MAINS;
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
            }
        }

        break;
    case scheduler_state::STATE_GO_TO_SLEEP        :
        m_com->tx("STATE_GO_TO_SLEEP\r\n");
        m_ucStateNxt = STATE_GO_TO_SLEEP;
        static_assert (wn::LOW == 0,"defintion error LO" ); // BATTERY_FORCE_N = LOW;
        m_gpio->clr(wn::dib_pin::force_run_on_battery_n);

        static_assert (wn::OFF == 0,"definition error OFF" );
        m_gpio->clr(wn::dib_pin::battery_charge); // BATTERY_CHARGE = OFF;


        m_gpio->clr(wn::dib_pin::relay_on_230vac); //   RELAY_ON = OFF;
        ProcessStop();
        if (m_ucStateSecCnt > STATE_POWER_OFF_SEC) // Go to sleep state after waiting long enough for PC to shutdown Windows
        {
            m_gpio->clr(wn::dib_pin::power_to_pc); //   PC_POWER = OFF;
            m_ucStateNxt = STATE_GOING_TO_SLEEP;
        }

        break;

    case scheduler_state::STATE_GOING_TO_SLEEP     :
        m_com->tx("STATE_GOING_TO_SLEEP\r\n");
        m_ucStateNxt = STATE_GOING_TO_SLEEP;
        m_gpio->clr(wn::dib_pin::force_run_on_battery_n); //   BATTERY_FORCE_N = LOW;
        m_gpio->clr(wn::dib_pin::battery_charge); //  BATTERY_CHARGE = OFF;
        m_gpio->clr(wn::dib_pin::relay_on_230vac); // RELAY_ON = OFF;
        ProcessStop();
        m_gpio->clr(wn::dib_pin::power_to_pc); //   PC_POWER = OFF;
        if (m_pps_pulse)
        {
            if (m_ucStateSecCnt > STATE_PC_POWER_OFF_SEC)
            {
                if (m_gpio->get(wn::dib_pin::power_on) == HIGH) //   POWER_ON_BUTTON == HIGH) // Active low
                {
                    m_ucStateNxt = STATE_SLEEP;
                }
                else //Power button has been turned on again
                {
                    if (m_gpio->get(wn::dib_pin::mains_connected)) //    MAINS_CONNECTED)
                        {
                            m_ucStateNxt = STATE_IDLE_MAINS;
                        }
                        else
                        {
                            m_ucStateNxt = STATE_IDLE_BATTERY;
                        }
                }
                }
        }
        break;

    case scheduler_state::STATE_SLEEP              :
        m_com->tx("STATE_SLEEP\r\n");
        //@todo sleep mode...
        m_gpio->clr(dib_pin::pcb_led);//   PCBLED = LOW;


        Init_io_low_power();
        /* Below 3 lines is "double up" and "mostly" included also in "Init_io_low_power" function */
        // Sets all valves closed and all relays off
        m_regCtrl = 0;
        CtrlWrite(m_regCtrl);
        ProcessStop();
        m_stateCntSleep++;

        Config_irq_sleep();
        //@todo WDTCONbits.SWDTEN = 0; /* Disable Watchdog */
        //SLEEP();
        //Nop();
        //Nop();

        /* Execute when woke up from sleep mode by either power on button IRQ or mains connected IRQ */
        Config_irq_poweron();
        m_BattCntSec = 0;
        //@todo WDTCONbits.SWDTEN = 1; /* Enable Watchdog */

        if (m_gpio->get(wn::dib_pin::power_on) == LOW) // Active low
        {
            Init_io_power_on(ON);

            if (m_gpio->get(wn::dib_pin::mains_connected))
            {
                m_ucStateNxt = STATE_IDLE_MAINS;
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
            }

        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected))
            {
                Init_io_power_on(OFF);
                m_ucStateNxt = STATE_OFF_MAINS;
            }
            else
            {
                // Should enver end up here - indicated unexpected way of waking from sleep mode
                m_ucStateNxt = STATE_SLEEP;
            }
        }
        //Nop();


        break;
    case scheduler_state::STATE_TURN_OFF_MAINS     :
        m_com->tx("STATE_TURN_OFF_MAINS\r\n");
        // Ensure PC is powered off for min 1 second in order to be able to power it on again
        m_ucStateNxt = STATE_TURN_OFF_MAINS;
        m_gpio->set(wn::dib_pin::force_run_on_battery_n); //   BATTERY_FORCE_N = HIGH;
        m_gpio->clr(wn::dib_pin::relay_on_230vac); //  RELAY_ON = OFF;
        m_gpio->clr(wn::dib_pin::power_to_pc); //  PC_POWER = OFF;
        ProcessStop();
        if (m_pps_pulse)
        {
            if (m_ucStateSecCnt > STATE_PC_POWER_OFF_SEC)
            {
                m_ucStateNxt = STATE_OFF_MAINS;
            }
        }
        break;


    case scheduler_state::STATE_OFF_MAINS          :
        m_com->tx("STATE_OFF_MAINS\r\n");
        if (m_gpio->get(wn::dib_pin::power_on) == LOW) //   if (POWER_ON_BUTTON == LOW) // Active low
        {
            m_gpio->set(wn::dib_pin::power_to_pc); //  PC_POWER = ON;

            if (m_gpio->get(wn::dib_pin::mains_connected)) //  if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_IDLE_MAINS;
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
            }
        }
        else
        {
            m_gpio->clr(wn::dib_pin::power_to_pc); //  PC_POWER = OFF;
            if (m_gpio->get(wn::dib_pin::mains_connected)) //  if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_OFF_MAINS;
            }
            else
            {
                m_ucStateNxt = STATE_SLEEP;
            }
        }
        m_gpio->set(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = HIGH;
        m_gpio->clr(wn::dib_pin::relay_on_230vac); //  RELAY_ON = OFF;
        ProcessStop();
        m_BattCntSec=0;
        break;

    case scheduler_state::STATE_IDLE_BATTERY       :
        m_com->tx("STATE_IDLE_BATTERY\r\n");

        if (m_gpio->get(wn::dib_pin::power_on) == HIGH) // if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_GO_TO_SLEEP;
        }
        else
        {
            m_gpio->set(wn::dib_pin::power_to_pc); //  PC_POWER = ON;
            if (m_gpio->get(wn::dib_pin::mains_connected)) //  if (MAINS_CONNECTED)
            {
                // When mains is back wait 1 sec before switching
                // from battery to mains to avoid multiple switches
                // back and forth between battery and mains
                if (m_BattSwitchCnt++ < 10)
                {
                    m_ucStateNxt = STATE_IDLE_BATTERY;
                }
                else
                {
                    m_ucStateNxt = STATE_IDLE_MAINS;
                }
            }
            else
            {
                m_BattSwitchCnt = 0;
                /* Check if having run too long on battery - if yes then shutdown (go to sleep mode) */
                if (m_SensorBattVoltADC > BATT_VOLT_22_5V)
                {
                    if (m_BattCntSec > BATT_TIME_MAX)
                    {
                        m_ucStateNxt = STATE_GO_TO_SLEEP;
                        m_stateCntBattTimeout++;
                    }
                    else
                    {
                        m_ucStateNxt = STATE_IDLE_BATTERY;
                    }
                    m_BattShutdownSec = BATT_TIME_MAX;
                }
                else  // Voltage lower than 22.5V
                {
                    if (m_BattCntSec > BATT_TIME_MAX_LOW_VOLTAGE)
                    {
                        m_ucStateNxt = STATE_GO_TO_SLEEP;
                        m_stateCntBattTimeoutLowVolt++;
                    }
                    else
                    {
                        m_ucStateNxt = STATE_IDLE_BATTERY;
                    }
                    m_BattShutdownSec = BATT_TIME_MAX_LOW_VOLTAGE;
                }
            }
        }
        m_gpio->clr(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = LOW;
        m_gpio->clr(wn::dib_pin::battery_charge);  //   BATTERY_CHARGE = OFF;
        m_gpio->clr(wn::dib_pin::relay_on_230vac); //RELAY_ON = OFF;
        ProcessStop();
        if (m_pps_pulse)
        {
            m_BattCntSec++;
        }
        break;
    case scheduler_state::STATE_IDLE_MAINS         :
        m_com->tx("STATE_IDLE_MAINS\r\n");
        if (m_gpio->get(wn::dib_pin::power_on) == HIGH )   //   if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected)) //   if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_IDLE_MAINS;
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
            }
        }
        m_gpio->set(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = HIGH;
        m_gpio->clr(wn::dib_pin::relay_on_230vac); //  RELAY_ON = OFF;
        ProcessStop();
        m_BattCntSec=0;

        break;
    case scheduler_state::STATE_START              :
        m_com->tx("STATE_START\r\n");
        if (m_gpio->get(wn::dib_pin::power_on) == HIGH)   //  if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
            m_stateCntStopProcessLostPower++;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected)) //  if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_STARTING;
                m_gpio->set(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = HIGH;
                m_gpio->set(wn::dib_pin::relay_on_230vac); //  RELAY_ON = ON;
                ProcessPrepare();
                m_stateCntRunProcess++;
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
                m_stateCntStopProcessLostPower++;
            }
        }
        m_BattCntSec=0;
        break;

    case scheduler_state::STATE_STARTING           :
        m_com->tx("STATE_STARTING\r\n");
        if (m_gpio->get(wn::dib_pin::power_on) == HIGH) //  if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
            m_stateCntStopProcessLostPower++;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected)) //   if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_STARTING;
                m_gpio->set(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = HIGH;
                m_gpio->set(wn::dib_pin::relay_on_230vac); // RELAY_ON = ON;
                if (m_ucStateSecCnt > STATE_STARTING_SEC) // Goto Running state if waiting in Starting state long enough
                {
                    m_ucStateNxt = STATE_RUN;
                }
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
                m_stateCntStopProcessLostPower++;
            }
        }
        m_BattCntSec=0;
        break;
    case scheduler_state::STATE_RUN                :
        m_com->tx("STATE_RUN\r\n");
        if (m_gpio->get(wn::dib_pin::power_on)) //   if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
            m_stateCntStopProcessLostPower++;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected))  // if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_RUNNING;
                m_gpio->set(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = HIGH;
                m_gpio->set(wn::dib_pin::relay_on_230vac); //  RELAY_ON = ON;
                ProcessStart();
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
                m_stateCntStopProcessLostPower++;
            }
        }
        m_BattCntSec=0;

        break;
    case scheduler_state::STATE_RUNNING            :
        m_com->tx("STATE_RUNNING\r\n");
        if (m_gpio->get(wn::dib_pin::power_on)) //   if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
            m_stateCntStopProcessLostPower++;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected)) //  if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_RUNNING;
                m_gpio->set(wn::dib_pin::force_run_on_battery_n);  // BATTERY_FORCE_N = HIGH;
                m_gpio->set(wn::dib_pin::relay_on_230vac); //  RELAY_ON = ON;
                if (m_pps_pulse)
                {
                    if(m_HostPollSecCnt > MAX_TIME_HOST_POLL)
                    {
                        m_ucStateNxt = STATE_STOP;
                        m_stateCntStopProcessHostTimeout++;
                    }
                }
                m_BattCntSec=0;
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
                m_stateCntStopProcessLostPower++;
            }
        }

        break;
    case scheduler_state::STATE_TEST               :
        m_com->tx("STATE_TEST\r\n");
        m_gpio->set(wn::dib_pin::relay_on_230vac); //  RELAY_ON = ON;
        if (m_gpio->get(wn::dib_pin::mains_connected)) //  if (MAINS_CONNECTED)
        {
            m_BattCntSec=0;
        }
        else
        {
            if (m_pps_pulse)
            {
                m_BattCntSec++;
            }
        }
        m_ucStateNxt = m_ucState;

        break;
    case scheduler_state::STATE_EMERG_BUTTON_PUSHED:
        break;
    case scheduler_state::STATE_STOP               :
        m_com->tx("STATE_STOP\r\n");
        if (m_gpio->get(wn::dib_pin::power_on))  //   if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
            m_stateCntStopProcessLostPower++;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected)) //   if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_STOPPING;
                m_gpio->set(wn::dib_pin::force_run_on_battery_n);   //BATTERY_FORCE_N = HIGH;
                m_gpio->set(wn::dib_pin::relay_on_230vac);  //RELAY_ON = ON;
                ProcessPrepareToStop();
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
                m_stateCntStopProcessLostPower++;
            }
        }
        m_BattCntSec=0;

        break;
    case scheduler_state::STATE_STOPPING           :
        m_com->tx("STATE_STOPPING\r\n");
        if (m_gpio->get(wn::dib_pin::power_on))  //   if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
            m_stateCntStopProcessLostPower++;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected)) //   if (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_STOPPING;
                m_gpio->set(wn::dib_pin::force_run_on_battery_n); // BATTERY_FORCE_N = HIGH;
                m_gpio->set(wn::dib_pin::relay_on_230vac); //  RELAY_ON = ON;
                if (m_ucStateSecCnt > m_regPumpReverseTime) // Goto IDLE state if waiting in STOPPING state long enough
                {
                    m_ucStateNxt = STATE_IDLE_MAINS;
                }
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
                m_stateCntStopProcessLostPower++;
            }
        }
        m_BattCntSec=0;
        break;


        break;
    case scheduler_state::STATE_TURNING_OFF_MAINS  :
        m_com->tx("STATE_TURNING_OFF_MAINS\r\n");
        m_ucStateNxt = STATE_TURNING_OFF_MAINS;
        m_gpio->set(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = HIGH;
        m_gpio->set(wn::dib_pin::relay_on_230vac); //   RELAY_ON = OFF;
        ProcessStop();
        if (m_ucStateSecCnt > STATE_POWER_OFF_SEC) // Go to sleep state after waiting long enough for PC to shutdown Windows
        {
            m_ucStateNxt = STATE_TURN_OFF_MAINS;
            m_gpio->clr(wn::dib_pin::power_to_pc); //  PC_POWER = OFF;
        }

        break;
    case scheduler_state::STATE_230VAC_EXT_ON      :
        m_com->tx("STATE_230VAC_EXT_ON\r\n");
        if (m_gpio->get(wn::dib_pin::power_on) == HIGH ) //  if (POWER_ON_BUTTON == HIGH) // Active low
        {
            m_ucStateNxt = STATE_TURNING_OFF_MAINS;
            m_stateCntStopProcessLostPower++;
        }
        else
        {
            if (m_gpio->get(wn::dib_pin::mains_connected)) //  if  (MAINS_CONNECTED)
            {
                m_ucStateNxt = STATE_230VAC_EXT_ON;
                m_gpio->set(wn::dib_pin::force_run_on_battery_n); //  BATTERY_FORCE_N = HIGH;
                m_gpio->set(wn::dib_pin::relay_on_230vac); //  RELAY_ON = ON;
                if (m_pps_pulse)
                {
                    if(m_HostPollSecCnt > MAX_TIME_HOST_POLL)
                    {
                        m_ucStateNxt = STATE_IDLE_MAINS;
                        m_stateCntStopProcessHostTimeout++;
                    }
                }
                m_BattCntSec=0;
                Ext230VACOn();
            }
            else
            {
                m_ucStateNxt = STATE_IDLE_BATTERY;
                m_stateCntStopProcessLostPower++;
            }
        }
        break;

    default:
        m_com->tx("Unhandled state\r\n");

    }


    m_pps_pulse = LOW;
    switch (m_ucLoopTics)
    {
    case 0: //Tic no.0 is special case, ie run only once after reset.
    {

    }
    break;

    case 1: //Tic no.1
    {
        BlinkPcbLed();
        ReadStatus();
    }
    break;

    case 2: //Tic no.2
    {
        BlinkTopLed();
    }
    break;

    case 3: //Tic no.3
    {
        ReadAnalog();

    }
    break;

    case 4: //Tic no.4
    {
        ReadTempHumSensor();
    }
    break;

    case 5: //Tic no.5
    {
        /* Check state of battery charger and enable charging for 10 msec (to tick 6) */
//@TODO   SensorBattVoltADC       = Int12AdcRead(11); //Measure Battery Voltage
//        SensorBattFloatADC      = Int12AdcRead(0);  //Measure Battery Float Voltage
//        SensorBattVolt = 27.4 + ((float)SensorBattVoltADC - 2175.0) * 0.012552301;
//        SensorBattFloat = 27.4 + ((float)SensorBattFloatADC - 1452.0) * 0.01910828;
//        if (SensorBattFloat > SensorBattVolt)
//            BattChargeVolt1 = SensorBattFloat - SensorBattVolt;
//        else
//            BattChargeVolt1 = 0.0;
//        if (ucState != STATE_IDLE_BATTERY)
//        {
//            BattCharge(BattChargeVolt1);
//        }
    }
    break;

    case 6: //Tic no.6
    {
//@TODO/* Check if charger can be left on or leave off for 90 msec (10% duty cycle when Vflot - Vbat > 3V) */
//        SensorBattVoltADC       = Int12AdcRead(11); //Measure Battery Voltage
//        SensorBattFloatADC      = Int12AdcRead(0);  //Measure Battery Float Voltage
//        SensorBattVolt = 27.4 + ((float)SensorBattVoltADC - 2175.0) * 0.012552301;
//        SensorBattFloat = 27.4 + ((float)SensorBattFloatADC - 1452.0) * 0.01910828;
//        if (SensorBattFloat > SensorBattVolt)
//            BattChargeVolt2 = SensorBattFloat - SensorBattVolt;
//        else
//            BattChargeVolt2 = 0.0;
//        if (BattChargeVolt2 > 3.0)
//            BATTERY_CHARGE = OFF;
    }
    break;

    case 7: //Tic no.7
    {

    }
    break;

    case 8: //Tic no.8
    {

    }
    break;

    case 9: //Tic no.9
    {
        //ComHandler(); //Handle datadump requests.
    }
    break;

    case 10: //Tic no.10
    {
        // Generate 1 pulse per second signal
        if (++m_cnt100msec >= 10)
        {
            m_cnt100msec = 0;
            m_pps_pulse = HIGH;
        }
    }
    break;
    }

    // Door lock
    if (m_pps_pulse == HIGH)
    {
        if (m_DoorOpenSecCnt > 0)
        {
            m_DoorOpenSecCnt--;
        }
        else
        {
            m_gpio->clr(dib_pin::door_lock);
            //@TODO DOOR_LOCK = LOW; // Door lock closed
        }
    }

    //Wait time until TIMER1 rollover:
    m_ucLoopTics++; //next tic
    if (m_ucLoopTics > NUMBEROFTICS)
    {
        m_ucLoopTics = 1; //Reset tic counter
        m_ucLoopCnt++;    // Increment loop status cnt
    }


}

void Scheduler::BlinkPcbLed()
{
    if (m_pcbledcnt-- <= 0)
    {
        m_pcbledcnt = 2*LED_FREQ;
    }
    if (m_pcbledcnt > LED_FREQ)
    {
        m_gpio->set(dib_pin::pcb_led); //  PCBLED = HIGH;
    }
    else
    {
        m_gpio->clr(dib_pin::pcb_led);//   PCBLED = LOW;
    }

}

void Scheduler::ReadStatus()
{
    m_regStatus = 0;
    if ((m_gpio->get(wn::dib_pin::power_on) == 0) && (m_ucState != STATE_GO_TO_SLEEP))// active low

    {                                                            // Also set power off button to "off" when turning off due to low battery voltage
        setbit(m_regStatus, BIT_STAT_POWER_ON);                  // or having run too long on battery
    }
    if (m_gpio->get(wn::dib_pin::mains_connected)) {
        setbit(m_regStatus, BIT_STAT_MAINS_CONNECTED);
    }
    if (m_gpio->get(wn::dib_pin::door_lock) == HIGH) {
        setbit(m_regStatus, BIT_STAT_DOOR_LOCK);
    }
    if (m_gpio->get(wn::dib_pin::pump_temperature_warning) == OFF)
    {
        setbit(m_regStatus,BIT_STAT_PUMP_CTRL_TEMP_WARN);
    }
    m_regStatus += (m_BattPresent   << BIT_STAT_BATT_PRESENT);
    m_regStatus += (m_battLowWarning << BIT_STAT_BATT_ALARM);
    // @TODO m_regStatus += m_SensorTempHumType << BIT_STAT_SENSOR_TEMP_HUM_TYPE;

}

void Scheduler::BlinkTopLed()
{
    if (m_topledcnt-- <= 0)
    {
        m_topledcnt = 2*LED_FREQ;
    }
    if (m_topledcnt > LED_FREQ)
    {
        getbit(m_regLedCtrl, BIT_CTRL_LED_RED_ON_OFF) ? m_gpio->set(wn::dib_pin::valve_4) : m_gpio->set(wn::dib_pin::valve_4);
        getbit(m_regLedCtrl, BIT_CTRL_LED_GREEN_ON_OFF) ? m_gpio->set(wn::dib_pin::valve_3) : m_gpio->clr(wn::dib_pin::valve_3);
    }
    else
    {
        (getbit(m_regLedCtrl, BIT_CTRL_LED_RED_ON_OFF) ^ getbit(m_regLedCtrl, BIT_CTRL_LED_RED_BLINK)) ? m_gpio->set(wn::dib_pin::valve_4) : m_gpio->clr(wn::dib_pin::valve_4);
        (getbit(m_regLedCtrl, BIT_CTRL_LED_GREEN_ON_OFF) ^ getbit(m_regLedCtrl, BIT_CTRL_LED_GREEN_BLINK)) ? m_gpio->set(wn::dib_pin::valve_3) : m_gpio->clr(wn::dib_pin::valve_3);
    }


}

void Scheduler::ReadAnalog()
{

}

void Scheduler::ReadTempHumSensor()
{

}

void Scheduler::Init_io_low_power()
{
    //@TODO set all io's ready for sleep mode, how we do this will be different on the nRF platform compared to the PIC
    m_gpio->init_pins_sleep_mode();
}

void Scheduler::CtrlWrite(uint16_t value)
{
    getbit(value,BIT_CTRL_VALVE1) ? m_gpio->set(wn::dib_pin::valve_1) : m_gpio->clr(wn::dib_pin::valve_1);
    getbit(value,BIT_CTRL_VALVE2) ? m_gpio->set(wn::dib_pin::valve_2) : m_gpio->clr(wn::dib_pin::valve_2);
    getbit(value,BIT_CTRL_COMPR) ? m_gpio->set(wn::dib_pin::compressor) : m_gpio->clr(wn::dib_pin::compressor);
    getbit(value,BIT_CTRL_FAN_COMPR) ? m_gpio->set(wn::dib_pin::fan_on) : m_gpio->clr(wn::dib_pin::fan_on);
    getbit(value,BIT_CTRL_230VAC_EXT) ? m_gpio->set(wn::dib_pin::relay_on_230vac) : m_gpio->clr(wn::dib_pin::relay_on_230vac);
}

void Scheduler::ProcessStop()
{
    clrbit(m_regCtrl, BIT_CTRL_230VAC_EXT);
    clrbit(m_regCtrl, BIT_CTRL_FAN_COMPR);
    clrbit(m_regCtrl, BIT_CTRL_COMPR);
    //clrbit(regCtrl, BIT_CTRL_VALVE4);
    //clrbit(regCtrl, BIT_CTRL_VALVE3);
    setbit(m_regCtrl, BIT_CTRL_VALVE2);  // Keep air bleed valve open
    clrbit(m_regCtrl, BIT_CTRL_VALVE1);
    CtrlWrite(m_regCtrl);
    // Stop pump
    PumpStop();
}

void Scheduler::ProcessPrepare()
{
    /* Close all valves, door locked, start compressor and fan, ext 230VAC off while preparing process */
    m_regCtrl = (1 << BIT_CTRL_COMPR) +
              (1 << BIT_CTRL_FAN_COMPR);
    CtrlWrite(m_regCtrl);
    PumpStop();
}

void Scheduler::ProcessStart()
{
    /* Open air valve, other valves closed. Door locked, keep compressor and fan running  external 230VAC off */
    m_regCtrl = (1 << BIT_CTRL_COMPR) +
              (1 << BIT_CTRL_FAN_COMPR) +
              (1 << BIT_CTRL_VALVE1);
    CtrlWrite(m_regCtrl);
    // Start pump
    PumpStart(PUMP_FORWARD);

}

void Scheduler::PumpStart(uint8_t direction)
{
    m_regPumpCtrl = m_regPumpSpeed;                // Set PWM to defined pump speed
    if (direction) {                            // Default is low - only need to set if high
        setbit(m_regPumpCtrl,BIT_PUMP_CTRL_DIR);
    }
    clrbit(m_regPumpCtrl,BIT_PUMP_CTRL_BRAKE);   // Clear pump brake (start pump)
    PumpCtrlWrite(m_regPumpCtrl);
}

void Scheduler::Ext230VACOn()
{
    setbit(m_regCtrl, BIT_CTRL_230VAC_EXT);
    clrbit(m_regCtrl, BIT_CTRL_FAN_COMPR);
    clrbit(m_regCtrl, BIT_CTRL_COMPR);
    CtrlWrite(m_regCtrl);

}

void Scheduler::irqPowerOn()
{
    tick();
}

void Scheduler::irqMainsStateChange()
{
    if (!m_gpio->get(wn::dib_pin::mains_connected)) {
        if (m_ucState == STATE_START ||
            m_ucState == STATE_STARTING ||
            m_ucState == STATE_RUN ||
            m_ucState == STATE_RUNNING ||
            m_ucState == STATE_STOP ||
            m_ucState == STATE_STOPPING ||
            m_ucState == STATE_EMERG_BUTTON_PUSHED)
        {
            m_gpio->clr(wn::dib_pin::force_run_on_battery_n);//  BATTERY_FORCE_N = LOW;
            m_ucStateNxtIrq = STATE_IDLE_BATTERY;
            m_ucStateSetIrq = 1;
            m_stateCntStopProcessLostPower++;
        }
    }
    tick();

}

void Scheduler::ProcessPrepareToStop()
{
    clrbit(m_regCtrl, BIT_CTRL_COMPR);
    clrbit(m_regCtrl, BIT_CTRL_VALVE1);  // Close air valve
    setbit(m_regCtrl, BIT_CTRL_VALVE2);  // Open air bleed valve
    CtrlWrite(m_regCtrl);
    PumpStart(PUMP_REVERSE);

}

void Scheduler::PumpStop()
{
    setbit(m_regPumpCtrl,BIT_PUMP_CTRL_BRAKE); // Set pump brake (stop pump)
    PumpCtrlWrite(m_regPumpCtrl);
}

void Scheduler::PumpCtrlWrite(uint16_t value)
{
    int duty_percent;

    getbit(value,BIT_PUMP_CTRL_BRAKE) ? m_gpio->set(wn::dib_pin::pump_brake) : m_gpio->clr(wn::dib_pin::pump_brake);
    getbit(value, BIT_PUMP_CTRL_DIR) ? m_gpio->set(wn::dib_pin::pump_direction) : m_gpio->clr(wn::dib_pin::pump_direction);

    duty_percent = to_percent_from_mask(  value & BIT_MASK_PUMP_CTRL_PWM );
    if (duty_percent > 0) {
      m_pump_pwm->start( duty_percent  );
    } else {
        m_pump_pwm->stop();
    }

}

void Scheduler::BattChargePwmUpdate(uint16_t duty_cycle)
{
//@TODO    duty_cycle = duty_cycle & 0x3FF; // Ensure not more than 10 bit
//    // Write new duty cycle to ECCP3
//    CCP3CONbits.DC3B0 = duty_cycle & 1;        //set bit 0
//    CCP3CONbits.DC3B1 = (duty_cycle >> 1) & 1; //set bit 1
//    CCPR3L            = (duty_cycle >> 2);     //set bit 9-2

}

void Scheduler::FactoryDataWriteStart()
{
  /* Read factory data from program memory */
    m_factory_data->read();
}

void Scheduler::FactoryDataWriteEnd()
{
    m_factory_data->write();
}

void Scheduler::StartBootloader()
{
    //@TODO
}



void Scheduler::Init_io_power_on(uint8_t pc_on)
{
    //@TODO set all io's ready for run mode
    m_gpio->init_pins_run_mode();
}

void Scheduler::Config_irq_sleep()
{

}

void Scheduler::Config_irq_poweron()
{

}

void Scheduler::serport_rx_cnt_reset()
{
  m_rx_msg_cnt        = 0;
  m_rx_ok_cnt         = 0;
  m_rx_checksum_err   = 0;
  m_rx_oflowcnt       = 0;
  m_rx_wrong_stopflag = 0;
  m_rx_write_reg_err  = 0;
  m_rx_read_reg_err   = 0;
}

void Scheduler::serport_receive( wn::scheduler_msg::msg const *m  )
{
    uint8_t rx_command;
    uint16_t  rx_value;
    uint8_t unknown_register = FALSE;

    if (++m_rx_msg_cnt == 0) {    // Increment rx msg cnt and reset all other stat counters when wrap around
        serport_rx_cnt_reset();
    }

    rx_command  = m->m_command;
    rx_value    = m->m_value;

    {
        /* Checksum is correct - start to decode message */
        m_tx_wr_reg = rx_command & 0x7F; /* Register read from or written to */
        if (getbit(rx_command,7)) /* Test if write command - if yes decode here */
        /* Write from PC - store value in internal register */
        {
            switch (rx_command & 0x7F)
            {
            case ADR_HW_VER:
                if (m_stateWriteFactoryData)
                {
                    m_factory_data->set_hw_ver_major(rx_value >> 8);  //     FactoryDataWrite(OFFSET_DATA_HW_VER_MAJOR,rx_value >> 8);
                    m_factory_data->set_hw_ver_minor(rx_value & 0xFF); //   FactoryDataWrite(OFFSET_DATA_HW_VER_MINOR,rx_value & 0xFF);
                }
                break;
            case ADR_HW_SN_BYTE_4:
                if (m_stateWriteFactoryData)
                {
                    m_factory_data->set_hw_manu(rx_value & 0xFF);//    FactoryDataWrite(OFFSET_DATA_HW_MANU,rx_value & 0xFF);
                }
                break;
            case ADR_HW_SN_BYTE_2_3:
                if (m_stateWriteFactoryData)
                {
                    m_factory_data->set_hw_manu_year(rx_value >> 8); //  FactoryDataWrite(OFFSET_DATA_HW_MANU_YEAR,rx_value >> 8);
                    m_factory_data->set_hw_manu_month(rx_value & 0xFF); //  FactoryDataWrite(OFFSET_DATA_HW_MANU_MONTH,rx_value & 0xFF);
                }
                break;
               case ADR_HW_SN_BYTE_0_1:
                if (m_stateWriteFactoryData)
                {
                    m_factory_data->set_hw_sn(rx_value);
                    //m_factory_data->s  FactoryDataWrite(OFFSET_DATA_HW_SN_BYTE1,rx_value >> 8);
                    //FactoryDataWrite(OFFSET_DATA_HW_SN_BYTE0,rx_value & 0xFF);
                }
                break;
            case ADR_VARIANT:
                if (m_stateWriteFactoryData)
                {
                    m_factory_data->set_board_variant(rx_value);
                }
                break;
            case ADR_UNUSED_1:
                break;
            case ADR_CTRL:
                if (m_ucState == STATE_TEST) // Only allow manual control of valves, relays, etc
                {                         // in test mode
                    m_regCtrl = rx_value;
                    CtrlWrite(m_regCtrl);
                }
                break;
            case ADR_PUMP_CTRL:
                if (m_ucState == STATE_TEST) // Only allow manual control of Pump
                {                         // in test mode
                    m_regPumpCtrl = rx_value;
                    PumpCtrlWrite(m_regPumpCtrl);
                }
                break;
            case ADR_BATT_CHARGE_PWM:
                if (m_ucState == STATE_TEST) // Only allow manual control of Battery
                {                         // charge PWM in test mode
                    m_regBattChargePwm = rx_value;
                    BattChargePwmUpdate(m_regBattChargePwm);
                }
                break;
            case ADR_COMMAND:
                if (testbit (rx_value, BIT_COMMAND_TEST_MODE_START))
                    m_ucStateNxt = STATE_TEST;
                if (testbit (rx_value, BIT_COMMAND_TEST_MODE_END))
                    m_ucStateNxt = STATE_BOOT;
                if (testbit(rx_value,BIT_COMMAND_FACT_WRITE_START))
                {
                    FactoryDataWriteStart();
                    m_stateWriteFactoryData = TRUE;
                }
                if (testbit(rx_value,BIT_COMMAND_FACT_WRITE_END))
                {
                    if (m_stateWriteFactoryData)
                    {
                        FactoryDataWriteEnd();
                        //factData = FactoryDataRead();
                        //calData  = CalDataRead();
                        m_stateWriteFactoryData = FALSE;
                    }
                }
                if (testbit(rx_value,BIT_COMMAND_START_BOOTLOADER))
                {
                    StartBootloader();
                }
                if (testbit (rx_value,BIT_COMMAND_230VAC_EXT_ON))
                {
                    if (m_ucState == STATE_IDLE_MAINS)     // Only allow enabling external 230VAC outlet when connected to mains and idle
                    {
                        m_ucStateNxt = STATE_230VAC_EXT_ON;
                    }
                }
                if (testbit (rx_value,BIT_COMMAND_230VAC_EXT_OFF))
                {
                    if (m_ucState == STATE_230VAC_EXT_ON)     // Only allow going to mains IDLE with this command when in expected state
                    {
                        m_ucStateNxt = STATE_IDLE_MAINS;
                    }
                }
                if (testbit (rx_value,BIT_COMMAND_DOOR_OPEN_V1))
                {
                    m_DoorOpenSecCnt = DOOR_OPEN_TIME_SEC_V1;
                    m_gpio->set(wn::dib_pin::door_lock); //   DOOR_LOCK = HIGH; // Door lock open
                }
                if (testbit (rx_value,BIT_COMMAND_DOOR_OPEN_V2))
                {
                    m_DoorOpenSecCnt = DOOR_OPEN_TIME_SEC_V2;
                    m_gpio->set(wn::dib_pin::door_lock); //   DOOR_LOCK = HIGH; // Door lock open
                }
                if (testbit (rx_value,BIT_COMMAND_PROCESS_START))
                {
                    if ((m_ucState == STATE_IDLE_MAINS) ||
                        (m_ucState == STATE_230VAC_EXT_ON) ||
                        (m_ucState == STATE_START) ||
                        (m_ucState == STATE_STARTING) ||
                        (m_ucState == STATE_RUN) ||
                        (m_ucState == STATE_RUNNING) ||
                        (m_ucState == STATE_STOP) ||
                        (m_ucState == STATE_STOPPING) ||
                        (m_ucState == STATE_TEST))     // Only allow starting process when connected to mains (or running in test mode)
                    {
                        m_ucStateNxt = STATE_START;
                    }
                }
                if (testbit (rx_value,BIT_COMMAND_PROCESS_STOP))
                {
                    if ((m_ucState == STATE_START) ||
                        (m_ucState == STATE_STARTING) ||
                        (m_ucState == STATE_RUN) ||
                        (m_ucState == STATE_RUNNING))
                    {
                        m_ucStateNxt = STATE_STOP; // Skip testing for lost mains here - only worst case delay is one scheduler tick.
                        m_stateCntStopProcess++;
                    }
                }
                break;
            case ADR_LED:
                m_regLedCtrl = rx_value;
                break;
            case ADR_PUMP_SPEED:
                m_regPumpSpeed = rx_value;
                break;
            case ADR_PUMP_REVERSE_TIME:
                m_regPumpReverseTime = rx_value;
                break;
            default:
                unknown_register = TRUE;
                break;
            }
        }
        if (unknown_register == TRUE)
        {
            m_rx_read_reg_err++;
        }
        else
        {
            m_tx_write = TRUE; /* Flag to signal start to write to serial port */
            /* Will trigger both response to write command  */
            /* and output from read command                 */
            m_rx_ok_cnt++;
        }

    }
    // Complete reception of message and ready to receive next message
    m_rx_cnt = 0;
    // Check if anything to send
    serport_transmit();
    //PIE3bits.RC2IE = 1; // Turn on RX interrupt for SERPORT2 again
        // and be ready to receive next message

}

/*------------------------------------------------------------------*/
void Scheduler::serport_transmit( void )
/*------------------------------------------------------------------*/
/**
** DESCRIPTION : Transmit message on serial port
**
** PARAMETERS  : None.
**
** GLOBAL VAR  :
**
** RETURNS     : None.
**
**/
{
    uint8_t checksum;
    int i;


    /* Build Start of TX message  */
    m_tx_buffer[0]                =  dx2_serial_if::serport_start_stop_flag; //  SERPORT_START_STOP_FLAG;
    m_tx_buffer[dx2_serial_if::serport_adr_pos]  = 0x55;
    m_tx_buffer[dx2_serial_if::serport_ctrl_pos] = 0;
    m_tx_buffer[dx2_serial_if::serport_cmd_pos]  = m_tx_wr_reg;
    if (m_tx_wr_reg ==  ADR_SW_HW_READ)
    {
        /* Build multi register read message */
        m_tx_msg_len = 4;
        m_tx_buffer[m_tx_msg_len++] = wn::hw_pn_byte2; //    wn::hw_hw_pn_byte2; //pn  HW_PN_BYTE2;
        m_tx_buffer[m_tx_msg_len++] = wn::hw_pn_byte1;
        m_tx_buffer[m_tx_msg_len++] = wn::hw_pn_byte0;
        m_tx_buffer[m_tx_msg_len++] = 0;
        m_tx_buffer[m_tx_msg_len++] = m_factory_data->get_hw_ver_major(); //  factData.hw_ver_major;
        m_tx_buffer[m_tx_msg_len++] = m_factory_data->get_hw_ver_minor(); //  factData.hw_ver_minor;
        m_tx_buffer[m_tx_msg_len++] = 0;
        m_tx_buffer[m_tx_msg_len++] = m_factory_data->get_hw_manu();       //   factData.hw_manufacturer;
        m_tx_buffer[m_tx_msg_len++] = m_factory_data->get_hw_manu_year(); //    factData.hw_manu_year;
        m_tx_buffer[m_tx_msg_len++] = m_factory_data->get_hw_manu_month(); //   factData.hw_manu_month;
        m_tx_buffer[m_tx_msg_len++] = (m_factory_data->get_hw_sn() >> 8) & 0xFF; // >  factData.hw_sn_byte1;
        m_tx_buffer[m_tx_msg_len++] = m_factory_data->get_hw_sn() & 0xff;        //      factData.hw_sn_byte0;
        m_tx_buffer[m_tx_msg_len++] = wn::sw_ver_major; //  SW_VER_MAJOR;
        m_tx_buffer[m_tx_msg_len++] = wn::sw_ver_minor; //  SW_VER_MINOR;
        m_tx_buffer[m_tx_msg_len++] = m_factory_data->get_board_variant() ;
        m_tx_buffer[m_tx_msg_len++] = 0; // unused
        m_tx_buffer[m_tx_msg_len++] = 0; // unused
        checksum = 0;
        for (i=1; i<m_tx_msg_len;i++)
        {
            checksum += m_tx_buffer[i];
        }
        m_tx_buffer[m_tx_msg_len++] = checksum;
        m_tx_buffer[m_tx_msg_len++] = dx2_serial_if::serport_start_stop_flag; //   SERPORT_START_STOP_FLAG;
    }
    else
    {
        if (m_tx_wr_reg ==  wn::dib_register::ADR_SENSOR_CTRL_READ)
        {
            m_HostPollSecCnt = 0; // Reset host poll counter used for keep alive (host present) during RUNNING state

            /* Build multi register read message */
            m_tx_msg_len = 4;
            m_tx_buffer[m_tx_msg_len++] = m_regStatus >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_regStatus & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_SensorCurrentLoop1 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_SensorCurrentLoop1 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_SensorCurrentLoop2 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_SensorCurrentLoop2 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V1 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V1 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V2 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V2 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V3 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V3 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V4 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V4 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V5 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V5 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V6 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V6 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V7 >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor0To10V7 & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_SensorBattVoltADC >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_SensorBattVoltADC & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_SensorBattFloatADC >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_SensorBattFloatADC & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_SensorHumidity >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_SensorHumidity & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_SensorTemperature >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_SensorTemperature & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor1_024VbandGap >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_Sensor1_024VbandGap & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_regCtrl >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_regCtrl & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_regPumpCtrl >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_regPumpCtrl & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_SensorPumpCtrlCurr >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_SensorPumpCtrlCurr & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_regBattChargePwm >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_regBattChargePwm & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_BattCntSec >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_BattCntSec & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_BattShutdownSec >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_BattShutdownSec & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = (int)(m_BattChargeVolt2 * 100.0) >> 8;
            m_tx_buffer[m_tx_msg_len++] = (int)(m_BattChargeVolt2 * 100.0) & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_ucLoopCnt >> 8;
            m_tx_buffer[m_tx_msg_len++] = m_ucLoopCnt & 0xFF;
            m_tx_buffer[m_tx_msg_len++] = m_ucStateSecCnt;
            m_tx_buffer[m_tx_msg_len++] = m_ucState;
            checksum = 0;
            for (i=1; i<m_tx_msg_len;i++)
            {
                checksum += m_tx_buffer[i];
            }
            m_tx_buffer[m_tx_msg_len++] = checksum;
            m_tx_buffer[m_tx_msg_len++] = dx2_serial_if::serport_start_stop_flag;
        }
        else
        {
            if (m_tx_wr_reg == ADR_STATE_CNT_READ)
            {
                /* Build multi register read message */
                m_tx_msg_len = 4;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntSleep >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntSleep & 0xFF;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntRunProcess >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntRunProcess & 0xFF;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntStopProcess >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntStopProcess & 0xFF;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntStopProcessHostTimeout >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntStopProcessHostTimeout & 0xFF;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntStopProcessLostPower >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntStopProcessLostPower & 0xFF;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntBattTimeout >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntBattTimeout & 0xFF;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntBattTimeoutLowVolt >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntBattTimeoutLowVolt & 0xFF;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntEmergBtn >> 8;
                m_tx_buffer[m_tx_msg_len++] = m_stateCntEmergBtn & 0xFF;

                checksum = 0;
                for (i=1; i<m_tx_msg_len;i++)
                {
                    checksum += m_tx_buffer[i];
                }
                m_tx_buffer[m_tx_msg_len++] = checksum;
                m_tx_buffer[m_tx_msg_len++] = dx2_serial_if::serport_start_stop_flag;
            }
            else
            {
                if (m_tx_wr_reg ==   wn::dib_register::ADR_SERPORT_RX_READ)
                {
                    /* Build multi register read message */
                    m_tx_msg_len = 4;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_msg_cnt >> 8;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_msg_cnt & 0xFF;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_ok_cnt >> 8;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_ok_cnt & 0xFF;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_oflowcnt >> 8;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_oflowcnt & 0xFF;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_write_reg_err >> 8;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_write_reg_err & 0xFF;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_read_reg_err >> 8;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_read_reg_err & 0xFF;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_checksum_err >> 8;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_checksum_err & 0xFF;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_wrong_stopflag >> 8;
                    m_tx_buffer[m_tx_msg_len++] = m_rx_wrong_stopflag & 0xFF;

                    checksum = 0;
                    for (i=1; i<m_tx_msg_len;i++)
                    {
                        checksum += m_tx_buffer[i];
                    }
                    m_tx_buffer[m_tx_msg_len++] = checksum;
                    m_tx_buffer[m_tx_msg_len++] = dx2_serial_if::serport_start_stop_flag;
                }
                else
                {
                    /* Build single register read message */
                    switch(m_tx_wr_reg)
                    {
                    case ADR_CTRL:
                        m_tx_buffer[wn::dx2_serial_if::serport_value_hi_pos] = m_regCtrl >> 8;
                        m_tx_buffer[wn::dx2_serial_if::serport_value_lo_pos] = m_regCtrl & 0xFF;
                        break;
                    case ADR_PUMP_CTRL:
                        m_tx_buffer[wn::dx2_serial_if::serport_value_hi_pos] = m_regPumpCtrl >> 8;
                        m_tx_buffer[wn::dx2_serial_if::serport_value_lo_pos] = m_regPumpCtrl & 0xFF;
                        break;
                    case ADR_BATT_CHARGE_PWM:
                        m_tx_buffer[wn::dx2_serial_if::serport_value_hi_pos] = m_regBattChargePwm >> 8;
                        m_tx_buffer[wn::dx2_serial_if::serport_value_lo_pos] = m_regBattChargePwm & 0xFF;
                        break;
                    case ADR_PUMP_SPEED:
                        m_tx_buffer[wn::dx2_serial_if::serport_value_hi_pos] = m_regPumpSpeed >> 8;
                        m_tx_buffer[wn::dx2_serial_if::serport_value_lo_pos] = m_regPumpSpeed & 0xFF;
                        break;
                    case ADR_PUMP_REVERSE_TIME:
                        m_tx_buffer[wn::dx2_serial_if::serport_value_hi_pos] = m_regPumpReverseTime >> 8;
                        m_tx_buffer[wn::dx2_serial_if::serport_value_lo_pos] = m_regPumpReverseTime & 0xFF;
                        break;
                    }
                    m_tx_buffer[wn::dx2_serial_if::serport_checksum_pos] = m_tx_buffer[wn::dx2_serial_if::serport_adr_pos]
                                                      +m_tx_buffer[wn::dx2_serial_if::serport_ctrl_pos]
                                                      +m_tx_buffer[wn::dx2_serial_if::serport_cmd_pos]
                                                      +m_tx_buffer[wn::dx2_serial_if::serport_value_hi_pos]
                                                      +m_tx_buffer[wn::dx2_serial_if::serport_value_lo_pos];
                    m_tx_buffer[ wn::dx2_serial_if::serport_msg_len-1] = dx2_serial_if::serport_start_stop_flag;
                    m_tx_msg_len = wn::dx2_serial_if::serport_msg_len;
                }
            }
        }
    }
    m_pc_com->tx((uint8_t *)&m_tx_buffer[0],m_tx_msg_len);
    /* Transmit first byte and then turn on serport tx IRQ to transmit rest of message */
    m_tx_cnt=0;
    m_tx_write = FALSE;
}


void Scheduler::msg(const WnMessage *m)
{
    auto scheduler_msg = (wn::scheduler_msg::msg const *)(m);
    switch (scheduler_msg->m_msg_type) {
    case scheduler_msg::msg_types::tick:
        tick();
        break;
    case scheduler_msg::msg_types::power_on:
        irqPowerOn();
        break;
    case scheduler_msg::msg_types::mains_status:
        irqMainsStateChange();
        break;
    case scheduler_msg::msg_types::serial_rx:
        serport_receive(scheduler_msg);
        break;
    default:
        m_com->tx("Unhandled msg\r\n");

    }


}

bool Scheduler::runnable()
{
    return m_ucState != STATE_SLEEP;
}


} // namespace wn
