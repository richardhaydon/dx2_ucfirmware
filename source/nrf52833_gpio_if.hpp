// Title: DX2 - implementation of the gpio interface for the nrf52833 board
//
// Description: Base class for gpio interface operations
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef NRF52833_GPIO_IF_HPP
#define NRF52833_GPIO_IF_HPP

#include "gpio_if.hpp"
#include "nrfx_gpiote.h"
#include "nrf_drv_gpiote.h"

namespace wn {

class nrf52833_gpio_if : public gpio_if
{
public:
    nrf52833_gpio_if();
    virtual ~nrf52833_gpio_if() = default;

    void set(unsigned int pin) override;
    void clr(unsigned int pin) override;

    int get(int pin) override;

    void init_pins_run_mode() override;
    void init_pins_sleep_mode() override;

    void set_falling_edge_cb(unsigned int pin, nrfx_gpiote_evt_handler_t cb);
    void set_rising_edge_cb(unsigned int pin, nrfx_gpiote_evt_handler_t cb);
    void set_toggle_edge_cb(unsigned int pin, nrfx_gpiote_evt_handler_t cb);

protected:

    nrf_drv_gpiote_in_config_t m_falling_edge_config {NRF_GPIOTE_POLARITY_HITOLO,NRF_GPIO_PIN_PULLUP,false,true,true};
    nrf_drv_gpiote_in_config_t m_rising_edge_config {NRF_GPIOTE_POLARITY_LOTOHI,NRF_GPIO_PIN_PULLUP,false,true,true};
    nrf_drv_gpiote_in_config_t m_toggle_edge_config {NRF_GPIOTE_POLARITY_TOGGLE,NRF_GPIO_PIN_PULLUP,false,true,true};

};

} // namespace wn
#endif // NRF52833_GPIO_IF_HPP
