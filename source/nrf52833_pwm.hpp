// Title: DX2 - implementation of the pwm interface for the nrf52833 board
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef NRF52833_PWM_HPP
#define NRF52833_PWM_HPP

#include <vector>
#include "nrf_drv_pwm.h"
#include "pwm_if.hpp"

namespace  wn {

/**
 * @brief The nrf52833_pwm class implementation for the nrf52833 board
 * Is created with an associated board pwm device instance
 */
class nrf52833_pwm : public pwm_if
{
public:
    nrf52833_pwm(const nrf_drv_pwm_t &pwm_ins, const nrf_drv_pwm_config_t &config, uint32_t cf);

    virtual ~nrf52833_pwm() override;

    /**
     * @brief run drive the pin associated with the instance at the given duty cycle for the given period
     * @param pct
     * @param ms
     * @return error code
     */
    uint32_t run(uint16_t pct, uint32_t ms) override;


    /**
     * @brief start drive output with % duty cycle until stopped
     * @param pct
     * @return error code

     */
    uint32_t start(uint16_t pct) override;

    /**
     * @brief stop drive output
     * @param pct
     * @return error code
     */
    uint32_t stop() override;



private:
    const nrf_drv_pwm_t &m_pwm_ins;
    uint16_t m_seq_value;
    nrf_pwm_sequence_t m_sequence;
    const nrf_drv_pwm_config_t m_config;
    uint32_t m_cf;


};


}
#endif // NRF52833_PWM_HPP
