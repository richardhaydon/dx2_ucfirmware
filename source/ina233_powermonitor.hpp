// Title: DX2 DIB
//
// Description: TI INA233 implementation of the power monitor interface class
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef INA233_POWERMONITOR_HPP
#define INA233_POWERMONITOR_HPP

#include <memory>
#include "power_monitor.hpp"
#include "Arduino.h"  // nRF52822 Wire Interface port


namespace wn {


class Ina233_PowerMonitor : public PowerMonitor
{
public:
    Ina233_PowerMonitor(std::shared_ptr<arduino::TwoWire> twi, float shunt_resistor=1.0, float max_current=1.000); // for dx the sense resistor is 0.04ohms

    uint32_t set_power_mode(bool normal) override;

    std::string get_model() override;

    uint32_t config() override;

    uint32_t get_current_A(float &amps) override;

    uint32_t get_sense_V(float &volts) override;

    uint32_t get_bus_V(float &volts) override;


private:
    std::shared_ptr<arduino::TwoWire> m_twi;
    float m_shunt_resistor;
    float m_max_current;
    float m_current_lsb;

    uint32_t read_reg8(const uint8_t reg, uint8_t &val);
    uint32_t read_reg16(const uint8_t reg, uint16_t &val);
    uint32_t write_reg16(const uint8_t reg, uint16_t val);

};

} // namespace wn
#endif // INA233_POWERMONITOR_HPP
