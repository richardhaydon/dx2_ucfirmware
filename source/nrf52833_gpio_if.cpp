// Title: DX2 - implementation of the gpio interface for the nrf52833 board
//
// Description: Base class for gpio interface operations
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include "nrf52833_gpio_if.hpp"
#include "nrf_gpio.h"
#include "dx2_pins.hpp"
#include <boards.h>
#include <assert.h>
namespace wn {


nrf52833_gpio_if::nrf52833_gpio_if()
{
    auto err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);
}

void nrf52833_gpio_if::set(unsigned int pin)
{
   assert(nrf_gpio_pin_dir_get(pin) == NRF_GPIO_PIN_DIR_OUTPUT);
   nrf_gpio_pin_set(pin);
   assert(get(pin) == 1  );
}

void nrf52833_gpio_if::clr(unsigned int pin)
{
    assert(nrf_gpio_pin_dir_get(pin) == NRF_GPIO_PIN_DIR_OUTPUT);
    nrf_gpio_pin_clear(pin);
    assert(get(pin) == 0  );
}

int nrf52833_gpio_if::get(int pin)
{
    if (nrf_gpio_pin_dir_get(pin) == NRF_GPIO_PIN_DIR_INPUT) {
      return static_cast<int>(nrf_gpio_pin_read(pin) );
    } else {
        return static_cast<int>(nrf_gpio_pin_out_read(pin) );
    }
}

// GPIO line setup descriptions for power save and run modes



void nrf52833_gpio_if::init_pins_run_mode()
{
    nrf_gpio_cfg_output(wn::dib_pin::door_lock);
    nrf_gpio_cfg_input(wn::dib_pin::door_sense, BUTTON_PULL);
    nrf_gpio_cfg_input(wn::dib_pin::pump_temperature_warning, BUTTON_PULL);
    nrf_gpio_cfg_input(wn::dib_pin::battery_low_warning, BUTTON_PULL);
    nrf_gpio_cfg_output(wn::dib_pin::pcb_led);
    nrf_gpio_cfg_output(wn::force_run_on_battery_n);
    nrf_gpio_cfg_output(wn::relay_on_230vac);
    nrf_gpio_cfg_output(wn::battery_charge);
    nrf_gpio_cfg_output(wn::valve_1    );
    nrf_gpio_cfg_output(wn::valve_2    );
    nrf_gpio_cfg_output(wn::valve_3    );
    nrf_gpio_cfg_output(wn::valve_4    );
    nrf_gpio_cfg_output(wn::power_to_pc  );
    nrf_gpio_cfg_output(wn::fan_on     );
    nrf_gpio_cfg_input(wn::dib_pin::power_on, BUTTON_PULL);
    nrf_gpio_cfg_input(wn::dib_pin::mains_connected, BUTTON_PULL);
    nrf_gpio_cfg_output(wn::peripheral_low_power);
    nrf_gpio_cfg_output(wn::compressor);
    nrf_gpio_cfg_output(wn::pump_brake);
    nrf_gpio_cfg_output(wn::battery_pwm);
    nrf_gpio_cfg_output(wn::pump_direction);
    nrf_gpio_cfg_output(wn::pump_current_bit0);
    nrf_gpio_cfg_output(wn::pump_current_bit1);
    nrf_gpio_cfg_output(wn::pump_current_bit2);
    nrf_gpio_cfg_output(wn::pump_reset);


    clr(wn::valve_1    );
    clr(wn::valve_2    );
    clr(wn::valve_3    );
    clr(wn::valve_4    );
    clr(wn::power_to_pc  );
    clr(wn::fan_on     );
    clr(wn::dib_pin::pcb_led);
    clr(wn::dib_pin::force_run_on_battery_n);
    clr(wn::relay_on_230vac);
    clr(wn::battery_charge);
    set(wn::dib_pin::peripheral_low_power);
    clr(wn::compressor);
    clr(wn::pump_brake);
    clr(wn::pump_direction);
    clr(wn::pump_current_bit0);
    clr(wn::pump_current_bit1);
    set(wn::pump_reset);
    clr(wn::dib_pin::door_lock);

}

void nrf52833_gpio_if::init_pins_sleep_mode()
{
    init_pins_run_mode();
    clr(wn::dib_pin::peripheral_low_power);
    clr(wn::dib_pin::pump_reset);

}

void nrf52833_gpio_if::set_falling_edge_cb(unsigned int pin, nrfx_gpiote_evt_handler_t cb)
{
    auto err_code = nrf_drv_gpiote_in_init(pin, &m_falling_edge_config, cb);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(pin, true);
}

void nrf52833_gpio_if::set_rising_edge_cb(unsigned int pin, nrfx_gpiote_evt_handler_t cb)
{
    auto err_code = nrf_drv_gpiote_in_init(pin, &m_rising_edge_config, cb);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(pin, true);

}

void nrf52833_gpio_if::set_toggle_edge_cb(unsigned int pin, nrfx_gpiote_evt_handler_t cb)
{
    auto err_code = nrf_drv_gpiote_in_init(pin, &m_toggle_edge_config, cb);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(pin, true);
}

} // namespace wn
