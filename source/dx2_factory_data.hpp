// Title: DX2  - persistent storage interface
//
// Description: Factory data class
// The
//
// Standard: C++ 17
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef FACTORY_DATA_HPP
#define FACTORY_DATA_HPP

#include <memory>
#include <stdint.h>
#include "persistent_storage_if.hpp"

namespace wn {

/**
 * @brief The factory_data includes all factory set unit information and calibration
 */
class factory_data
{
public:

    enum board_variant {
        unset = 0,
        dib = 1,
        dsb = 2
    };

    /**
     * @brief factory_data construct with the persistent storage class to use
     * @param storage_if
     */
    factory_data(std::shared_ptr<persistent_storage_if> storage_if);

    /**
     * @brief read data from the storage interface
     * @return error code
     * 0 success
     * 1 failed
     */
    uint32_t read();

    /**
     * @brief write form initiated storage to underlying storage mechanism
     * @return error code
     * 0 success
     * 1 failed
     */
    uint32_t write();


    /**
     * @brief init create initial contents for write operatin
     */
    void init();


    /**
     * @brief get_hw_manu
     * throw if the data area is invalid before read
     * @return manufacture code
     */
    uint32_t get_hw_manu() const;

    /**
     * @brief get_hw_ver_major
     * throw if the data area is invalid before read
     * @return major version
     */
    uint32_t get_hw_ver_major() const;

    /**
     * @brief get_hw_ver_minor
     * throw if the data area is invalid before read
     * @return minor versin
     */
    uint32_t get_hw_ver_minor() const;

    /**
     * @brief get_hw_manu_year
     * throw if the data area is invalid before read
     * @return manufacture year
     */
    uint32_t get_hw_manu_year() const;

    /**
     * @brief get_hw_manu_month
     * throw if the data area is invalid before read
     * @return manufacture mont
     */
    uint32_t get_hw_manu_month() const;

    /**
     * @brief get_hw_sn
     * throw if the data area is invalid before read
     * @return serial number
     */
    uint32_t get_hw_sn() const;

    /**
     * @brief get_board_variant
     * @return type of board
     */
    uint32_t get_board_variant() const;



    /**
     * @brief set_hw_manu
     * throw if the data area is invalid before write
     */
    void set_hw_manu(uint32_t value);

    /**
     * @brief set_hw_ver_major
     * throw if the data area is invalid before write
     */
    void set_hw_ver_major(uint32_t value);

    /**
     * @brief set_hw_ver_minor
     * throw if the data area is invalid before write
     */
    void set_hw_ver_minor(uint32_t value);

    /**
     * @brief set_hw_manu_year
     * throw if the data area is invalid before write
     */
    void set_hw_manu_year(uint32_t value);

    /**
     * @brief set_hw_manu_month
     * throw if the data area is invalid before write
     */
     void set_hw_manu_month(uint32_t value);

    /**
     * @brief set_hw_sn
     * throw if the data area is invalid before write
     */
    void set_hw_sn(uint32_t value);

    /**
     * @brief set_board_variant
     */
    void set_board_variant(uint32_t value);

    static constexpr uint32_t m_layout_version = 0x10000001;
    static constexpr uint32_t success = 0;
    static constexpr uint32_t bad_size = 1;
    static constexpr uint32_t bad_version = 2;
    static constexpr uint32_t bad_checkword = 3;

protected:
    // supported layout version
    std::shared_ptr<persistent_storage_if> m_storage_if;



    enum dx_data_offset {
        layout_version = 0,
        hw_manu,
        hw_ver_major,
        hw_ver_minor,
        hw_manu_year,
        hw_manu_month,
        hw_sn,
        variant,
        spare_01,
        check_word
    };

    std::vector<uint32_t> m_dx_data;

    // checkword runs over data from layout to checkword exclusive
    uint32_t checkword() const;

};

} // namespace wn
#endif // FACTORY_DATA_HPP
