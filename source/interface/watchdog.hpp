// Title: Interface definition for system watchdog
//
// Description: Scheduler task
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.



#ifndef WATCHDOG_HPP
#define WATCHDOG_HPP


namespace wn {

class Watchdog
{
public:
    Watchdog() {}
    virtual ~Watchdog() = default;

    /**
     * @brief enable the watchdog instance
     */
    virtual void enable() = 0;

    /**
     * @brief reset the watchdog, called periodically when in operation
     */
    virtual void reset() = 0;

};

} // namespace wn
#endif // WATCHDOG_HPP
