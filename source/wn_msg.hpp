// Title: WideNorth message base defintion
//
// Description: Base type for messages in the WN interprocess comms framework
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef WN_MSGS_HPP
#define WN_MSGS_HPP

#include <iostream>
#include <stdint.h>
#include <string>

#define MAX_MSG_LEN 512

using WnPayload = std::string;
using WnFlags = uint32_t;
using MboxAdddress = std::string;
// Base for all the messages structures in the system
struct WnMessage {


    virtual ~WnMessage() {}

  /**
   * @brief getCommandStr
   * @param the name of the parameter
   * @return string representation of the command, all derived messages must
   * implement this
   */
  const virtual std::string getCommandStr() const = 0;
};


#endif // WN_MSGS_HPP
