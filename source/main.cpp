// Title: DX2 Sensor board
//
// Description: Main entry point for the application
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include "FreeRTOS.h"
#include "app_error.h"
#include "boards.h"
#include "bsp.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_drv_power.h"
#include "nrf_gpio.h"
#include "nrf_serial.h"
#include "nrf_delay.h"
#include "nrf52833_pwm_channels.h" // declares the instances of the pwm peripheral used
#include "nrf52833_uartes.h"   // declares the instances of uarts for the target, uses sdk serial library
#include "nrf52833_pwm.hpp"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_wdt.h"

#include "nrf_drv_pwm.h"

#include "sdk_errors.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <memory>
#include <sstream>
#include <iomanip>
#include "rs232_serial_interface.hpp"
#include "nrf52833_gpio_if.hpp"

#include "vl53lox_ranging_sensor.hpp"
#include "Adafruit_VL53L0X.h"
#include "Arduino.h"

#include "onboard_temperature_sensor.hpp"
#include "nvmc_flash_storage.hpp"
#include "scheduler_msg.hpp"
#include "scheduler.hpp"
#include <memory>
#include "dx2_post.hpp"
#include "dx2_serial_if.hpp"
#include "power_monitor.hpp"
#include "ina233_powermonitor.hpp"
#include "nrf52833_watchdog.hpp"



static TaskHandle_t  host_reader_task_handle;
static TaskHandle_t  scheduler_task_handle;
static QueueHandle_t schedulerQueue;

// objects for peripherals
static std::shared_ptr<wn::serial_interface> pc_com = std::make_shared<wn::rs232_serial_interface>(&wn::serial1_uarte);
static std::shared_ptr<wn::serial_interface> trace_com = std::make_shared<wn::rs232_serial_interface>(&wn::serial0_uarte);
static std::shared_ptr<wn::nrf52833_gpio_if> gpio = std::make_shared<wn::nrf52833_gpio_if>() ;
static std::shared_ptr<wn::persistent_storage_if> flash_if = std::make_shared<wn::nvmc_flash_storage>(64);
static std::shared_ptr<wn::factory_data> fact_data = std::make_shared<wn::factory_data>(flash_if);
static  std::shared_ptr<wn::pwm_if>pump_pwm = std::make_shared<wn::nrf52833_pwm>(m_pwm_pump,pump_pwm_config,PWM_CF);
static std::shared_ptr<arduino::TwoWire> twi = std::make_shared<arduino::TwoWire>();
static std::shared_ptr<wn::range_sensor> range_sensor = std::make_shared<wn::Vl53lox_ranging_sensor>(twi);
static std::shared_ptr<temperature_sensor> temperature_sensor = std::make_shared<wn::onboard_temperature_sensor>();
static std::shared_ptr<wn::Ina233_PowerMonitor> power_monitor = std::make_shared<wn::Ina233_PowerMonitor>(twi);
static std::shared_ptr<wn::NRF52_watchdog> watchdog = std::make_shared<wn::NRF52_watchdog>();
static void scheduler_task_function (void * pvParameter)
{
    wn::scheduler_msg::msg tickMsg(wn::scheduler_msg::msg_types::tick  );

    wn::dx2_post post(trace_com,gpio,flash_if,range_sensor,pump_pwm,temperature_sensor,twi,power_monitor);
    post.run();

    wn::Scheduler scheduler(trace_com,gpio, fact_data, pc_com,pump_pwm,power_monitor,watchdog);
    uint8_t buff[sizeof (wn::scheduler_msg::msg)];
    scheduler.init();
    for(;;) {
        // if runable serve any content and move onto the next tick
        if( xQueueReceive( schedulerQueue, buff, scheduler.runnable() ? pdMS_TO_TICKS(10) :  portMAX_DELAY ) )
        {
            scheduler.msg( (wn::scheduler_msg::msg *)(buff ));
        } else {
          scheduler.msg(&tickMsg);
        }
        if (scheduler.runnable()) {
            vTaskResume(host_reader_task_handle);
        } else {
            vTaskSuspend(host_reader_task_handle);
        }

    }
}

// Reads from host serial port forming messages to the scheduler
static void host_reader_fn (void * pvParameter)
{
    UNUSED_PARAMETER(pvParameter);
    uint8_t  achar;
    wn::dx2_serial_if protocol;
    protocol.reset_state();

    while (true)
    {
       auto res = pc_com->rx(&achar);
       if (res == 0) {
           protocol.rx(achar);
           //trace_com->tx("Got a character\r\n");

           if (protocol.ready()) {
               trace_com->tx("Got a full message.\r\n");
               auto msg = protocol.get_rx_msg();
               xQueueSendToBack(schedulerQueue, ( void * ) &msg, 0);
           }
       }
    }
}



void isr_gpio_power_on(nrf_drv_gpiote_pin_t , nrf_gpiote_polarity_t ) {
    wn::scheduler_msg::msg txCmd(wn::scheduler_msg::msg_types::power_on);
    xQueueSendToBackFromISR(schedulerQueue, ( void * ) &txCmd, 0);
}
void isr_gpio_mains_status_change(nrf_drv_gpiote_pin_t , nrf_gpiote_polarity_t ) {
    wn::scheduler_msg::msg txCmd(wn::scheduler_msg::msg_types::mains_status);
    xQueueSendToBackFromISR(schedulerQueue, ( void * ) &txCmd, 0);
}


int main(void)
{


    //Configure WDT.
//    nrf_drv_wdt_channel_id m_channel_id;
//    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
//    auto err_code = nrf_drv_wdt_init(&config, nullptr);
//    APP_ERROR_CHECK(err_code);
//    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
//    APP_ERROR_CHECK(err_code);
//    nrf_drv_wdt_enable();

    wn::configure_serial_ports();
    ret_code_t ret;
    ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);
    ret = nrf_drv_power_init(nullptr);
    APP_ERROR_CHECK(ret);
    nrf_drv_clock_lfclk_request(nullptr);

    gpio->set_falling_edge_cb(wn::dib_pin::power_on,isr_gpio_power_on);
    gpio->set_toggle_edge_cb(wn::dib_pin::mains_connected,isr_gpio_mains_status_change);

    schedulerQueue = xQueueCreate(10, sizeof (wn::scheduler_msg::msg));
    UNUSED_VARIABLE(xTaskCreate(host_reader_fn, "HOST", configMINIMAL_STACK_SIZE + 1024, NULL, 2, &host_reader_task_handle));
    UNUSED_VARIABLE(xTaskCreate(scheduler_task_function , "SCHEDULER", configMINIMAL_STACK_SIZE + 5000, NULL, 2, &scheduler_task_handle));

    /* Activate deep sleep mode */
    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

    vTaskStartScheduler();


    while (true) {
    }
}

/** @} */



//    std::shared_ptr<wn::gpio_if> gpio = std::make_shared<wn::nrf52833_gpio_if>() ;
//    std::shared_ptr<wn::persistent_storage_if> storage = std::make_shared<wn::nvmc_flash_storage>(64);
//    std::shared_ptr<wn::range_sensor> range_sensor = std::make_shared<wn::Vl53lox_ranging_sensor>();

//    std::shared_ptr<wn::pwm_if> pump_pwm = std::make_shared<wn::nrf52833_pwm>(m_pwm_pump,pump_pwm_config,PWM_CF);
//    std::shared_ptr<wn::pwm_if> battery_pwm = std::make_shared<wn::nrf52833_pwm>(m_pwm_battery,battery_pwm_config,PWM_CF);
//    std::shared_ptr<temperature_sensor> temperature_sensor = std::make_shared<wn::onboard_temperature_sensor>();

//    wn::dx2_post post(com1,gpio,storage,range_sensor,pump_pwm,battery_pwm,temperature_sensor);
//    post.run();

