// Title: DX2 Sensor board
//
// Description: nrf52833 storage implementation of persistent
// storage interface
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef NVMC_FLASH_STORAGE_HPP
#define NVMC_FLASH_STORAGE_HPP

#include "persistent_storage_if.hpp"
#include <vector>

namespace wn {

/**
 * @brief The nrf52833_flash_storage class
 * Implements persistent storage for nvmc library for the nRf52xxx boards
 */
class nvmc_flash_storage : public persistent_storage_if
{
public:
    nvmc_flash_storage(size_t extent);
    virtual ~nvmc_flash_storage() override;

    std::vector<uint32_t> read() const override;
    uint32_t save(const std::vector<uint32_t> &data) override;

private:
    uint32_t m_addr;
    uint32_t m_pg_size;
    uint32_t m_pg_num;

};

} // namespace wn
#endif // NVMC_FLASH_STORAGE_HPP
