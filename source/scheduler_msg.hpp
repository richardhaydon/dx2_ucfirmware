// Title: DX2 DIB scheduler message definitions
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef SCHEDULER_MSG_HPP
#define SCHEDULER_MSG_HPP

#include "wn_msg.hpp"
#include <iostream>
#include <string>

namespace wn {

namespace scheduler_msg {

// The commands that may be received by the scheduler
enum msg_types { nop, tick, serial_rx, power_on, mains_status };

struct msg : public WnMessage {
  msg_types m_msg_type{msg_types::nop};

  uint8_t m_address{0xFF};
  uint8_t m_control{0xFF};
  uint8_t m_command{0xFF};
  uint16_t m_value{0xFFFF};

  msg() = default;
  virtual ~msg() override;

  /**
   * @brief msg construct message without value associated with it
   * @param c
   */
  msg(msg_types c);

  /**
   * @brief msg construte message type with address, control, command and value
   * contents
   * @param c
   * @param address
   * @param control
   * @param command
   * @param value
   */
  msg(msg_types c, uint8_t m_address, uint8_t m_control, uint8_t m_command,
      uint16_t m_value);

  /**
   * @brief getCommandStr
   * @return string representation of the command
   */
  const std::string getCommandStr() const override;

  // allow for pipe out of object for logging
  friend std::ostream &operator<<(std::ostream &os, const msg &d) {
    os << " type: " << d.getCommandStr() << " addr: " << d.m_address
       << " ctrl: " << d.m_control << " cmd: " << d.m_command
       << " val: " << d.m_value;
    return os;
  }
};
} // namespace scheduler_msg
} // namespace wn
#endif // SCHEDULER_MSG_HPP
