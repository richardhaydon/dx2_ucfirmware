#include "vl53lox_ranging_sensor.hpp"

namespace wn {


Vl53lox_ranging_sensor::Vl53lox_ranging_sensor(std::shared_ptr<arduino::TwoWire> tw) : m_tw{tw}
{

}

int wn::Vl53lox_ranging_sensor::read_range_mm(int &range)
{

    if (!m_booted) {
      m_booted = m_ranger.begin(VL53L0X_I2C_ADDR,false,m_tw.get());
    }

    if (!m_booted) {
        return -1;
    }

    VL53L0X_RangingMeasurementData_t reading;
    auto res =m_ranger.rangingTest(&reading,false);
    range = reading.RangeMilliMeter;
    return res;
}

}
