// Title: DX2  - pin definitions for supported board types
//
// Description: mapping from pin names in the DX2 to nRF52 pins
// The
//
// Standard: C++ 17
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef DX2_PINS_HPP
#define DX2_PINS_HPP


namespace wn {


/** @brief Macro for mapping port and pin numbers to values understandable for nrf_gpio functions. */
#define NRF_GPIO_PIN_MAP(port, pin) (((port) << 5) | ((pin) & 0x1F))


enum dib_pin {
    door_lock = NRF_GPIO_PIN_MAP(0,2),
    door_sense = NRF_GPIO_PIN_MAP(0,3),
    uart1_tx_pin = NRF_GPIO_PIN_MAP(0,4),
    uart1_rx_pin = NRF_GPIO_PIN_MAP(0,5),

    uart0_tx_pin = NRF_GPIO_PIN_MAP(0,6),    
    uart0_rx_pin = NRF_GPIO_PIN_MAP(0,8),

    battery_low_warning = NRF_GPIO_PIN_MAP(0,12),
    pcb_led = NRF_GPIO_PIN_MAP(0,13),
    force_run_on_battery_n = NRF_GPIO_PIN_MAP(0,14),
    relay_on_230vac = NRF_GPIO_PIN_MAP(0,15),
    battery_charge = NRF_GPIO_PIN_MAP(0,16),
    valve_1 = NRF_GPIO_PIN_MAP(0,17),
    valve_2 = NRF_GPIO_PIN_MAP(0,18),
    valve_3 = NRF_GPIO_PIN_MAP(0,19),
    valve_4 = NRF_GPIO_PIN_MAP(0,20),
    battery_pwm =  NRF_GPIO_PIN_MAP(0,21),
    power_to_pc     = NRF_GPIO_PIN_MAP(0,22),
    fan_on          = NRF_GPIO_PIN_MAP(0,23),
    power_on        =   NRF_GPIO_PIN_MAP(0,24),
    mains_connected = NRF_GPIO_PIN_MAP(0,25),

    i2c_sda = NRF_GPIO_PIN_MAP(0,26),
    i2c_scl = NRF_GPIO_PIN_MAP(0,27),
    peripheral_low_power = NRF_GPIO_PIN_MAP(0,28),
    compressor = NRF_GPIO_PIN_MAP(0,29),


    pump_current_bit0 = NRF_GPIO_PIN_MAP(1,0),
    pump_current_bit1 = NRF_GPIO_PIN_MAP(1,1),
    pump_current_bit2 = NRF_GPIO_PIN_MAP(1,2),
    pump_current_bit3 = NRF_GPIO_PIN_MAP(1,3),
    pump_current_bit4 = NRF_GPIO_PIN_MAP(1,4),
    pump_reset = NRF_GPIO_PIN_MAP(1,5),
    pump_brake = NRF_GPIO_PIN_MAP(1,6),
    pump_direction = NRF_GPIO_PIN_MAP(1,7),
    pump_pwm = NRF_GPIO_PIN_MAP(1,8),
    pump_temperature_warning = NRF_GPIO_PIN_MAP(1,9),


    };


} // namespace wn


#endif // DX2_PINS_HPP
