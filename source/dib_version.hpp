// Title: DX2 Dib versiion
//
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef DIB_VERSION_HPP
#define DIB_VERSION_HPP

namespace wn {


constexpr auto  sw_ver_major   = 1 ;
constexpr auto  sw_ver_minor   = 35;
constexpr auto  sw_ver_micro   = 0 ;
constexpr auto  hw_pn_byte2    = 50;
constexpr auto  hw_pn_byte1    = 03;
constexpr auto  hw_pn_byte0    = 62;

} // namespace wn



#endif // DIB_VERSION_HPP
