// Title: Deconx Sensor board
//
// Description: Base type for tasks in the WN interprocess comms framework
//
// Standard: C++ 17
//
// Tools: Developed using <tools used>
//        cmake, mingw
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef WN_TASK_HPP
#define WN_TASK_HPP

#include <memory>
#include "wn_msg.hpp"


class wn_task
{
public:
    wn_task() {}
    virtual ~wn_task() {}
    virtual void init() =0;
    virtual void msg(WnMessage const *) = 0;
    virtual bool runnable() {return false;}

};


#endif // WN_TASK_HPP
