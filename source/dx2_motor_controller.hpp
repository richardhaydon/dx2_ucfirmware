// Title: DX2 DIB Motor Controller
//
// Description: DX2 TI DRV8840 implementation of the motor driver interface
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2020 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.



#ifndef DX2_MOTORCONTROLLER_HPP
#define DX2_MOTORCONTROLLER_HPP

#include <memory>
#include "motor_controller.hpp"
#include "pwm_if.hpp"
#include "gpio_if.hpp"
#include "dx2_pins.hpp"

namespace wn {


class DX2_MotorController : public MotorController
{
public:

    /**
     * @brief DX2_MotorController
     * @param pwm the motor controller is bound to the pwm interface to set the speed of operation
     */
    DX2_MotorController(std::shared_ptr<wn::pwm_if> pwm, std::shared_ptr<gpio_if> gpio);

    /**
     * @brief set_direction for the motor controller
     * @param d direction to apply
     */
    void set_direction(direction d) override;

    /**
     * @brief set_speed_percent
     * @param s speed as a percentage
     */
    void set_speed_percent(int s) override;

    /**
     * @brief set_power_percent
     * @param p sets a discrete power setting like current limit for the controlled motor
     */
    void set_power_percent(int p) override;


private:
    std::shared_ptr<wn::pwm_if> m_pwm;
    std::shared_ptr<wn::gpio_if>m_gpio;

};

} // namespace wn

#endif // DX2_MOTORCONTROLLER_HPP
