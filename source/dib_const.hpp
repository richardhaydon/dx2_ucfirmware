// Title: DX2 uc firmware definitions
//
// Description: constant definitions for the dx2 dib
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef DIB_CONST_HPP
#define DIB_CONST_HPP

#include <stdint.h>

namespace wn {


// mapping of models supported to protocol address
enum dx_part {
    dx2_dib                     = 0x55,
    dx2_dsb                     = 0x60
};


// Address mapping
enum dib_register {
  ADR_HW_PN_BYTE1_2             = 0x01,
  ADR_HW_PN_BYTE0               = 0x02,
  ADR_HW_VER                    = 0x03,
  ADR_HW_SN_BYTE_4              = 0x04,
  ADR_HW_SN_BYTE_2_3            = 0x05,
  ADR_HW_SN_BYTE_0_1            = 0x06,
  ADR_SW_VER                    = 0x07,
  ADR_VARIANT             = 0x0A,
  ADR_UNUSED_1           = 0x0B,
  ADR_SW_HW_READ                = 0x0F,
  ADR_STATUS                    = 0x10,
  ADR_SENSOR_1                  = 0x11,
  ADR_SENSOR_2                  = 0x12,
  ADR_SENSOR_3                  = 0x13,
  ADR_SENSOR_4                  = 0x14,
  ADR_SENSOR_5                  = 0x15,
  ADR_SENSOR_6                  = 0x16,
  ADR_SENSOR_7                  = 0x17,
  ADR_SENSOR_BATT_VOLT          = 0x18,
  ADR_SENSOR_BATT_FLOAT_VOLT    = 0x19,
  ADR_SENSOR_HUM                = 0x1A,
  ADR_SENSOR_TEMP               = 0x1B,
  ADR_CTRL                      = 0x1C,
  ADR_PUMP_CTRL                 = 0x1D,
  ADR_PUMP_CTRL_CURR_SENSE      = 0x1E,
  ADR_GP_IO                     = 0x1F,
  ADR_BATT_CHARGE_PWM           = 0x20,
  ADR_BATT_CNT_SEC              = 0x21,
  ADR_SENSOR_CTRL_READ          = 0x2F,
  ADR_COMMAND                   = 0x30,
  ADR_LED                       = 0x31,
  ADR_PUMP_SPEED                = 0x32,
  ADR_PUMP_REVERSE_TIME         = 0x33,
  ADR_STATE_CNT_SLEEP           = 0x40,
  ADR_STATE_CNT_RUN_PROCESS     = 0x41,
  ADR_STATE_CNT_STOP_PROCESS    = 0x42,
  ADR_STATE_CNT_HOST_TIMEOUT    = 0x43,
  ADR_STATE_CNT_LOST_POWER      = 0x44,
  ADR_STATE_CNT_BATT_SHD        = 0x45,
  ADR_STATE_CNT_BATT_SHD_LOW_VOL= 0x46,
  ADR_STATE_CNT_EMERG_BUTTON    = 0x47,
  ADR_STATE_CNT_READ            = 0x4F,
  ADR_SERPORT_RX_MSG_CNT        = 0x50,
  ADR_SERPORT_RX_MSG_OK_CNT     = 0x51,
  ADR_SERPORT_RX_OFLOW_CNT      = 0x52,
  ADR_SERPORT_RX_WR_REG_ERR_CNT = 0x53,
  ADR_SERPORT_RX_RD_REG_ERR_CNT = 0x54,
  ADR_SERPORT_RX_CHSUM_ERR_CNT  = 0x55,
  ADR_SERPORT_RX_STOP_FL_ERR_CNT= 0x56,
  ADR_SERPORT_RX_READ           = 0x5F
};

// Bit positions status reg
enum dib_status_register {
  BIT_STAT_SENSOR_TEMP_HUM_TYPE =    9 ,// 2 bit value
  BIT_STAT_PUMP_CTRL_TEMP_WARN  =    8 ,
  BIT_STAT_DOOR_LOCK            =    7 ,
  BIT_STAT_BATT_ALARM           =    4 ,// Also uses bit BIT_STAT_BATT_ALARM+1
  BIT_STAT_BATT_PRESENT         =    3 ,
  BIT_STAT_MAINS_CONNECTED      =    2 ,
  BIT_STAT_EMERGENCY_BUTTON     =    1 ,
  BIT_STAT_POWER_ON             =    0
};

// Bit positions control reg
enum dib_ctrl_register {
  BIT_CTRL_230VAC_EXT           =   10,
  BIT_CTRL_FAN_COMPR            =    9,
  BIT_CTRL_COMPR                =    8,
  BIT_CTRL_VALVE4               =    4,
  BIT_CTRL_VALVE3               =    3,
  BIT_CTRL_VALVE2               =    2,
  BIT_CTRL_VALVE1               =    1
};

// Bit positions pump control reg
enum dib_pump_ctrl_regiser {
 BIT_PUMP_CTRL_BRAKE            =   15,
 BIT_PUMP_CTRL_DIR              =   14,
 BIT_MASK_PUMP_CTRL_PWM         = 0x3FF //Also used as bitmask for PUMP_SPEED register
};

constexpr uint16_t  to_percent_from_mask(uint16_t v) { return  (uint32_t(v+1) * 100 ) / BIT_MASK_PUMP_CTRL_PWM ;    }
static_assert (to_percent_from_mask(0) == 0,"percent for pwm failed" );
static_assert (to_percent_from_mask(BIT_MASK_PUMP_CTRL_PWM) == 100,"percent for pwm failed" );



// Bit position Command register
enum dim_cmd_register {
 BIT_COMMAND_TEST_MODE_START    =  15,
 BIT_COMMAND_TEST_MODE_END      =  14,
 BIT_COMMAND_FACT_WRITE_END     =  13,
 BIT_COMMAND_FACT_WRITE_START   =  12,
 BIT_COMMAND_START_BOOTLOADER   =  11,
 BIT_COMMAND_230VAC_EXT_ON      =   6,
 BIT_COMMAND_230VAC_EXT_OFF     =   5,
 BIT_COMMAND_DOOR_OPEN_V2       =   4,
 BIT_COMMAND_DOOR_OPEN_V1       =   3,
 BIT_COMMAND_PROCESS_STOP       =   2,
 BIT_COMMAND_PROCESS_START      =   1,
 BIT_COMMAND_SLEEP              =   0
};

// Bit positions LED control reg
enum dim_led_ctrl_register {
 BIT_CTRL_LED_RED_BLINK          =  3,
 BIT_CTRL_LED_RED_ON_OFF         =  2,
 BIT_CTRL_LED_GREEN_BLINK        =  1,
 BIT_CTRL_LED_GREEN_ON_OFF       =  0
};

// Bit positions pump reverse time  reg
constexpr auto BIT_MASK_PUMP_REVERSE_TIME     =0xFF;

constexpr auto  OFF              =                0;
constexpr auto  ON               =                1;
constexpr auto  FALSE            =                0;
constexpr auto  TRUE             =                1;
constexpr auto  LOW              =                0;
constexpr auto  HIGH             =                1;
constexpr auto  FAIL             =                0;
constexpr auto  OK               =                1;
constexpr auto  CLEAR            =                0;
constexpr auto  SET              =                1;
constexpr auto  EMPTY            =                0;

// INPUT & OUTPUT signals
//#define  POWER_ON_BUTTON                RB0  // Active low
//#define  EMERGENCY_BUTTON               RB1  // Emergency button (High when pushed)
//#define  MAINS_CONNECTED                RB2  // High when mains power connected
//#define  PC_POWER                       RB3  // Set to high to power on PC
//#define  DOOR_LOCK                      RB4  // Door lock
//#define  PUMP_DIRECTION                 RB5  // Set high for forward
//#define  PUMP_CTRL_TEMP_WARN            RC5  // High when temperature warning from pump controller
//#define  I2C2_POWER_ON                  RD1  // Active low
//#define  I2C1_POWER_ON                  RD4  // Active low
//#define  RELAY_ON                       RD7  // Relay enable (on) when high
//#define  BATTERY_FORCE_N                RE0  // Set to low to force running on battery
//#define  BATTERY_LOW                    RE1  // '1' when battery voltage lower than approx 18V
//#define  PUMP_BRAKE                     RE2  // Brake when high
//#define  BATTERY_CHARGE                 RF5  // Set to high to turn on battery charger
//#define  AIN1_24V_ON                    RG4  // Power on to internal H2O2 sensor

// Scehduler
constexpr auto NUMBEROFTICS      =              10; // No of tics in paced loop, inclusive.

enum scheduler_state {
  STATE_BOOT                 =      0 ,// uC is booting after power on
  STATE_SLEEP                =      1 ,// Sleep mode
  STATE_OFF_MAINS            =      2 ,// Off - mains connected
  STATE_IDLE_BATTERY         =      3 ,// Idle mode - running on battery
  STATE_IDLE_MAINS           =      4 ,// Idle mode - running on mains
  STATE_START                =      5 ,// Go to starting state
  STATE_STARTING             =      6 ,// Preparing to start process (build compressor pressure)
  STATE_RUN                  =      7 ,// Go to run state
  STATE_RUNNING              =      8 ,// Running process
  STATE_TEST                 =      9 ,// Test mode - all registers contrallable by PC
  STATE_EMERG_BUTTON_PUSHED  =     10 ,// Emergency button is pushed
  STATE_STOP                 =     11 ,// Go to STOPPING state
  STATE_STOPPING             =     12 ,// Stop process (reverse pump to return unused chemistry back into tank)
  STATE_GO_TO_SLEEP          =     13 ,// Wait till PC has shut down before entering sleep mode
  STATE_TURNING_OFF_MAINS    =     14 ,// Enter off mains state (Wait till PC has shut down)
  STATE_TURN_OFF_MAINS       =     15 ,// Ensure PC is turned off min 1 second when turning it off (in case user has already turned on again power on button)
  STATE_GOING_TO_SLEEP       =     16 ,// Ensure PC is turned off min 1 second when going to sleep mode( in case user has already turned on again power on button)
  STATE_230VAC_EXT_ON        =     17 // External 230VAC outlet on (and compressor off)
};

constexpr auto SERPORT_BUFFER_LEN       =        54;
constexpr auto STATE_STARTING_SEC       =        5; // Number of seconds to stay in STARTING state before going to running state
constexpr auto STATE_STOPPING_SEC       =       60; // Default number of seconds to stay in STOPPING state before going to IDLE state

    // Value decides how long pump is run in reverse to empty hose after filling H2O2
constexpr auto  STATE_POWER_OFF_SEC     =        18; // Number of seconds to wait after POWER ON button is turned off before cutting power to PC
    // (allow PC to shut down Windows during this time)
constexpr auto STATE_PC_POWER_OFF_SEC   =        1; // Minmum number of seconds Power must be removed from PC in order for PC to start reboot


// Led
//#define	 PCBLED	             LATCbits.LATC0 //PCB led
constexpr auto LED_FREQ                  =      10; // Controls blinking frequency of PCB led
    // Higher number gives slower blinking



// ADC
constexpr auto ADC_OFFSET                =      4  ;// Compensate for leakaeg of approx 10 mV
    // at ADC inputs.
    // OFFSET = (10mV /(10V/2^12))
// Battery charger
constexpr auto BATT_PWM_MIN              =       64;  // Min PWM setting ( 6.25 duty cycle for PWM => 25.5V
constexpr auto BATT_PWM_MAX              =      576;  // Max PWM setting (56.25 duty cycle for PWM => 28.5V
constexpr auto BATT_PWM_DEFAULT          =      376;  // Default PWM setting (36.55 duty cycle for PWM => 27.4V
constexpr auto BATT_FLOAT_27_4V          =     1452;  // Measure (DIB prototype SN0015020001) ADC reading for Battery float voltage 27.4V
constexpr auto BATT_VOLT_22_5V           =     1785;  // Measure (DIB prototype SN0015020001) ADC reading for measured Battery voltage 22.5V
constexpr auto BATT_VOLT_24_4V           =     1936;  // Measure (DIB prototype SN0015020001) ADC reading for measured Battery voltage 24.4V
constexpr auto BATT_VOLT_25_0V           =     1984;  // Measure (DIB prototype SN0015020001) ADC reading for measured Battery voltage 25.0V
constexpr auto BATT_TIME_MAX             =     3600;  // Run max 1 hour on battery before automatic shutdown
constexpr auto BATT_TIME_MAX_LOW_VOLTAGE =      600;  // Run max 10 min on battery when low battery voltage

// Pump
constexpr auto PUMP_SPEED                =      300;  // Default pump speed used in disinfection process
constexpr auto PUMP_FORWARD              =        1;  // Value of PUMP_DIRECTION signal when running pump forward
constexpr auto PUMP_REVERSE              =        0;  // Value of PUMP_DIRECTION signal when running pump reverse

// Run process
constexpr auto MAX_TIME_HOST_POLL        =        6;  // Require at least one status poll (command 0x2F) from host every MAX_TIME_HOST_POLL seconds

// Door
constexpr auto DOOR_OPEN_TIME_SEC_V1     =       15;  // Time door lock stays open after receiving door open command (to avoid burning door lock)
    // Version 1 doorlock used on DX1a, DX1b, and DX1c

constexpr auto DOOR_OPEN_TIME_SEC_V2      =       1;  // Time door lock signal stays active after receiving door open command
    // Version 2 doorlock used on DX1d and newer models

} // namespace


#endif // DIB_CONST_HPP
