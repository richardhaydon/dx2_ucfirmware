// Title: DX2 - implementation of the serial interface using the nRF uart (easydma)
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#include "rs232_serial_interface.hpp"

namespace wn {


rs232_serial_interface::rs232_serial_interface(const nrf_serial_t *interface) : m_interface{interface}
{

}

rs232_serial_interface::~rs232_serial_interface() {}

uint32_t rs232_serial_interface::tx(uint8_t c)
{
    uint8_t buff[1] = {c};
    auto res = nrf_serial_write(m_interface,
                           &buff,
                           1,
                           nullptr,
                           NRF_SERIAL_MAX_TIMEOUT);
    (void)nrf_serial_flush(m_interface, 0);
    return res;

}

uint32_t rs232_serial_interface::tx(std::string s)
{
    auto res = nrf_serial_write(m_interface,
                           s.c_str(),
                           s.length(),
                           nullptr,
                           NRF_SERIAL_MAX_TIMEOUT);
    (void)nrf_serial_flush(m_interface, 0);
    return res;

}

uint32_t rs232_serial_interface::tx(uint8_t *p, size_t len)
{
    auto res = nrf_serial_write(m_interface,
                                p,
                                len,
                                nullptr,
                                NRF_SERIAL_MAX_TIMEOUT);
    (void)nrf_serial_flush(m_interface, 0);
    return res;
}

uint32_t rs232_serial_interface::rx(uint8_t *p_data, size_t size, size_t *p_read, uint32_t timeout_ms)
{
    return nrf_serial_read(m_interface,p_data,size,p_read,timeout_ms);
}

uint32_t rs232_serial_interface::rx(uint8_t *c)
{
    size_t sz;
    return nrf_serial_read(m_interface,c,1,&sz,NRF_SERIAL_MAX_TIMEOUT);
}


}
