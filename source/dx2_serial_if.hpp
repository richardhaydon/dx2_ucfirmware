// Title: DX2
//
// Description: nrf52833 serial protocol implementation
// Defines a parser for the DX2 protocol, maintaining state in the reception of messages and
// statistics
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef DX2_SERIAL_IF_HPP
#define DX2_SERIAL_IF_HPP

#include "serial_interface.hpp"
#include <memory>
#include <stdint.h>
#include <scheduler_msg.hpp>

namespace wn {

/**
 * @brief The dx2_serial_if class instance constructed with the commuinication
 * interface to use
 */
class dx2_serial_if {
public:
  constexpr static uint8_t serport_msg_len = 8;
  constexpr static uint8_t serport_adr_pos = 1;
  constexpr static uint8_t serport_ctrl_pos = 2;
  constexpr static uint8_t serport_cmd_pos = 3;
  constexpr static uint8_t serport_value_hi_pos = 4;
  constexpr static uint8_t serport_value_lo_pos = 5;
  constexpr static uint8_t serport_checksum_pos = 6;
  constexpr static uint8_t serport_start_stop_flag = 0x7E;

  // states for the serial port protocol receiver
  enum class rx_protocol_state { flag_in, addr, ctrl, cmd, value_hi, value_lo, checksum , flag_out, complete };

  struct stats {
      uint16_t  rx_ok_cnt     = 0;    // Serial port statistics counter
      uint16_t  rx_msg_cnt    = 0;    // Serial port statistics counter
      uint16_t  rx_checksum_err = 0;  //Serial port statistics counter
      uint16_t  rx_oflowcnt   = 0;    // Serial port overflow counter
      uint16_t  rx_wrong_stopflag= 0; // Serial port error counter
      uint16_t  rx_write_reg_err = 0; // Serial port error counter
      uint16_t  rx_read_reg_err = 0;  // Serial port error counter
  };


  dx2_serial_if();

  /**
   * @brief rx feed a received character to the
   * @param c
   * @return
   */
  bool rx(uint8_t c);

  /**
   * @brief ready
   * @return true if message is ready
   */
  bool ready();

  /**
   * @brief get_rx_msg
   * resets state of the receiver
   * @return complete scheduler message, throws if no message exists
   */
  scheduler_msg::msg get_rx_msg();


  /**
   * @brief reset_stats
   */
  void reset_stats();

  /**
   * @brief reset_state reset the state of the message receiver
   */
  void reset_state();



  uint16_t  get_rx_ok_cnt         ();    // Serial port statistics counter
  uint16_t  get_rx_msg_cnt        ();    // Serial port statistics counter
  uint16_t  get_rx_checksum_err   ();  //Serial port statistics counter
  uint16_t  get_rx_oflowcnt       ();    // Serial port overflow counter
  uint16_t  get_rx_wrong_stopflag (); // Serial port error counter
  uint16_t  get_rx_write_reg_err  (); // Serial port error counter
  uint16_t  get_rx_read_reg_err   ();  // Serial port error counter


private:
    stats m_stats;
    rx_protocol_state m_state{rx_protocol_state::flag_in};
    scheduler_msg::msg m_rx_msg;
    uint8_t m_rx_checksum{0x00};
    uint8_t m_calc_checksum{0x00};

};

} // namespace wn

#endif // DX2_SERIAL_IF_HPP
