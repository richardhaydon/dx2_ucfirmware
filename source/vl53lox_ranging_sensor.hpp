#ifndef VL53LOX_RANGING_SENSOR_HPP
#define VL53LOX_RANGING_SENSOR_HPP

#include "range_sensor.hpp"
#include "Adafruit_VL53L0X.h"
#include "Arduino.h"
#include <memory>

namespace wn {

class Vl53lox_ranging_sensor : public range_sensor
{
public:
    Vl53lox_ranging_sensor(std::shared_ptr<arduino::TwoWire> tw);
    virtual ~Vl53lox_ranging_sensor() = default;

    int read_range_mm(int &range) override;
private:
    std::shared_ptr<arduino::TwoWire> m_tw;
    Adafruit_VL53L0X m_ranger  = Adafruit_VL53L0X();
    bool m_booted{false};

};

}

#endif // VL53LOX_RANGING_SENSOR_HPP
