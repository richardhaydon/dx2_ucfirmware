// Title: DX2 - implementaion of the temperature interface using the nRF internal sensor
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef  ONBOARD_TEMPERATURE_SENSOR_HPP
#define ONBOARD_TEMPERATURE_SENSOR_HPP

#include "temperature_sensor.hpp"
#include <stdint.h>

namespace wn {


class onboard_temperature_sensor : public temperature_sensor
{
public:
    onboard_temperature_sensor();
    virtual ~onboard_temperature_sensor() override = default;

    /**
     * @brief read_temperature
     * @return temperature in C from the on board temperture module
     */
    int32_t read_temperature() override;
};

} // namespace wn
#endif // ONBOARD_TEMPERATURE_SENSOR
