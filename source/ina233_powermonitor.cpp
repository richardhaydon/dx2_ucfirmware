#include "ina233_powermonitor.hpp"

namespace wn {

namespace  {
const uint8_t  device_address = 64;

const uint8_t  CLEAR_FAULTS = 0x03; //  Clears the status registers and rearms the blackbox registers for updating Send byte 0 N/A
const uint8_t RESTORE_DEFAULT_ALL = 0x12; // RESTORE_DEFAULT_ALL Restores internal registers to the default values Send byte 0 N/A
const uint8_t IOUT_OC_WARN_LIMIT = 0x4A; //  Retrieves or stores the output overcurrent warn
const uint8_t VIN_OV_WARN_LIMIT = 0x57; //  Retrieves or stores the input overvoltage warn

const uint8_t VIN_UV_WARN_LIMIT = 0x58; //  Retrieves or stores the input undervoltage warn
const uint8_t PIN_OP_WARN_LIMIT = 0x6B; //  Retrieves or stores the output overpower warn
const uint8_t STATUS_BYTE = 0x78; //  Retrieves information about the device operating
const uint8_t STATUS_WORD = 0x79; //  Retrieves information about the device operating
const uint8_t STATUS_IOUT = 0x7B; //  Retrieves information about the output current
const uint8_t STATUS_INPUT = 0x7C; //  Retrieves information about the input status R/W, CLR 1 00h
const uint8_t STATUS_CML = 0x7E; //  Retrieves information about the communications
const uint8_t READ_VIN = 0x88; //  Retrieves the measurement for the VBUS voltage R 2 0000h
const uint8_t READ_IN = 0x89; //  Retrieves the input current measurement,
const uint8_t READ_PIN = 0x97; //  Retrieves the input power measurement R 2 0000h
const uint8_t MFR_ID = 0x99; //  Retrieves the manufacturer ID in ASCII
const uint8_t MFR_MODEL = 0x9A; //  Retrieves the device number in ASCII characters
const uint8_t MFR_REVISION = 0x9B; //  Retrieves the device revision letter and number in
const uint8_t MFR_ADC_CONFIG = 0xD0; //  Configures the ADC averaging modes,
const uint8_t MFR_READ_VSHUNT = 0xD1; //  Retrieves the shunt voltage measurement R 2 0000h
const uint8_t MFR_ALERT_MASK = 0xD2; //  Allows masking of device warnings R/W 1 F0h
const uint8_t MFR_CALIBRATION = 0xD4; //  ws the value of the current-sense resistor calibration value to be input. Must be programed at power-up. Default value is set to 1.


}


Ina233_PowerMonitor::Ina233_PowerMonitor(std::shared_ptr<arduino::TwoWire> twi, float shunt_resistor, float max_current) : m_twi{twi} ,  m_shunt_resistor{shunt_resistor}, m_max_current{max_current}
{
}

uint32_t Ina233_PowerMonitor::set_power_mode(bool normal)
{
    uint32_t result {0};
    uint16_t settings, check;
    result = read_reg16(MFR_ADC_CONFIG,settings);

    if (result == 0) {
        if (normal) {
            settings |= 0x07;
        } else {
            settings &= ~(0x07);
        }
        result = write_reg16(MFR_ADC_CONFIG,settings);

    }
    return result;
}

std::string Ina233_PowerMonitor::get_model()
{
    std::string result;
    uint8_t rx[8] = {0};
    m_twi->beginTransmission(device_address);
    m_twi->write(MFR_MODEL);
    m_twi->endTransmission();
    auto count = m_twi->requestFrom(device_address, 7);
    for(auto i=0;i<count;i++) {
        rx[i] = m_twi->read();
    }

    result = std::string((char *)&rx[0]);
    return result;

}

uint32_t Ina233_PowerMonitor::config()
{

    // if the i2c bus is not yet joined attempt
    if (!m_twi->begin()) {
        return 1;
    }
    m_current_lsb = m_max_current / 0x8000;
    float cal = 0.00512f / (m_current_lsb * m_shunt_resistor);
    uint16_t calib;
    auto result = read_reg16(MFR_CALIBRATION,calib);
    if (result == 0) {
        calib = static_cast<uint16_t>(cal) & 0x7fff;
        result = write_reg16(MFR_CALIBRATION,calib);
    }
    return result;
}

uint32_t Ina233_PowerMonitor::get_current_A(float &amps)
{

    uint16_t Y;
    auto res = read_reg16(READ_IN,Y);
    if (res == 0) {
        amps = static_cast<float>(Y) * m_current_lsb;
    }
    return res;
}

uint32_t Ina233_PowerMonitor::get_sense_V(float &volts)
{
    // for vShunt units volts m=4 & r=5 b=0
    // x volts = 1/m * ( Y * 10^-r -b)
    uint16_t Y;
    auto res = read_reg16(MFR_READ_VSHUNT,Y);
    if (res == 0) {
        volts = static_cast<float>(Y) / 1e5f / 4;
    }
    return res;
}

uint32_t Ina233_PowerMonitor::get_bus_V(float &volts)
{
    // for vin units volts
    // x volts = 1/m * ( Y * 10^-r -b) m=8 & r=2
    uint16_t Y;
    auto res = read_reg16(READ_VIN,Y);
    if (res == 0) {
        volts = static_cast<float>(Y) / 1e2f / 8;
    }
    return res;
}

uint32_t Ina233_PowerMonitor::read_reg8(const uint8_t reg, uint8_t &val)
{
    const uint8_t cnt {1};
    m_twi->beginTransmission(device_address);
    m_twi->write(reg);
    if (m_twi->endTransmission(false)) {
        return 1;
    }
    auto count = m_twi->requestFrom(device_address, cnt);
    if (count == cnt) {
        val= m_twi->read();
    } else {
        return 2;
    }
    return 0;

}

uint32_t Ina233_PowerMonitor::read_reg16(const uint8_t reg, uint16_t &val)
{
    const uint8_t cnt {2};
    m_twi->beginTransmission(device_address);
    m_twi->write(reg);
    if (m_twi->endTransmission(false)) {
        return 1;
    }
    auto count = m_twi->requestFrom(device_address, cnt);
    if (count == cnt ) {
        val = m_twi->read();
        val += (m_twi->read() << 8 );
    } else {
        return 2;
    }
    return 0;

}

uint32_t Ina233_PowerMonitor::write_reg16(const uint8_t reg, uint16_t val)
{
    m_twi->beginTransmission(device_address);
    m_twi->write(reg);
    m_twi->write(val & 0xff);
    m_twi->write((reg >> 8) & 0xff);
    if (m_twi->endTransmission()) {
        return 1;
    }
    return 0;
    //

}

} // namespace wn
