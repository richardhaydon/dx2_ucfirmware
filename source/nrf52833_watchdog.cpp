#include "nrf52833_watchdog.hpp"

namespace wn {

NRF52_watchdog::NRF52_watchdog()
{
    auto err_code = nrf_drv_wdt_init(&m_config, nullptr);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
}

NRF52_watchdog::~NRF52_watchdog()
{

}

void NRF52_watchdog::reset()
{
    nrf_drv_wdt_channel_feed(m_channel_id);
}

void NRF52_watchdog::enable()
{
    nrf_drv_wdt_enable();
}



}
