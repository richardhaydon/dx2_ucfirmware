#ifndef NRF52833_PWMS_H
#define NRF52833_PWMS_H

#include "nrf_drv_pwm.h"
#include "boards.h"
#include "bsp.h"
#include "dx2_pins.hpp"


#define PWM_COUNT_TOP 1000
#define PWM_CF 250000

// Declare the PWM objects use in the DX sensor application
nrf_drv_pwm_config_t const pump_pwm_config =
{
    .output_pins =
    {
        static_cast<int>(wn::dib_pin::pump_pwm) , // channel 0
        NRF_DRV_PWM_PIN_NOT_USED,     // channel 1
        NRF_DRV_PWM_PIN_NOT_USED,             // channel 2
        NRF_DRV_PWM_PIN_NOT_USED,             // channel 3
    },
    .irq_priority = APP_IRQ_PRIORITY_LOWEST,
    .base_clock   = NRF_PWM_CLK_250kHz,
    .count_mode   = NRF_PWM_MODE_UP,
    .top_value    = PWM_COUNT_TOP,
    .load_mode    = NRF_PWM_LOAD_COMMON,
    .step_mode    = NRF_PWM_STEP_AUTO
};

// battery pwm channel not used
//nrf_drv_pwm_config_t const battery_pwm_config =
//    {
//        .output_pins =
//            {
//                NRF_DRV_PWM_PIN_NOT_USED, // channel 0
//                static_cast<int>(wn::dib_pin::pump_pwm),     // channel 1
//                NRF_DRV_PWM_PIN_NOT_USED,             // channel 2
//                NRF_DRV_PWM_PIN_NOT_USED,             // channel 3
//            },
//        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
//        .base_clock   = NRF_PWM_CLK_250kHz,
//        .count_mode   = NRF_PWM_MODE_UP,
//        .top_value    = PWM_COUNT_TOP,
//        .load_mode    = NRF_PWM_LOAD_COMMON,
//        .step_mode    = NRF_PWM_STEP_AUTO
//};



static nrf_drv_pwm_t m_pwm_pump = NRF_DRV_PWM_INSTANCE(0);
//static nrf_drv_pwm_t m_pwm_battery = NRF_DRV_PWM_INSTANCE(1);
#define USED_PWM(idx) (1UL << idx)



#endif // NRF52833_PWMS_H
