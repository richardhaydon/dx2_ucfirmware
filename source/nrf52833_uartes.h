// Title: DX2 - definitions of the uart channels used from the nrf52833
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef SERIAL0_H
#define SERIAL0_H

#include "app_error.h"
#include "boards.h"
#include "bsp.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_drv_clock.h"
#include "nrf_drv_power.h"
#include "nrf_serial.h"
#include "sdk_errors.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "dx2_pins.hpp"
#include <FreeRTOS.h>
#include <task.h>

namespace wn {

static void sleep_handler(void)
{
    vTaskDelay(1);
//    __WFE();
//    __SEV();
//    __WFE();
}


#define SERIAL_FIFO_TX_SIZE 32
#define SERIAL_FIFO_RX_SIZE 32

NRF_SERIAL_QUEUES_DEF(serial0_queues, SERIAL_FIFO_TX_SIZE, SERIAL_FIFO_RX_SIZE);
NRF_SERIAL_QUEUES_DEF(serial1_queues, SERIAL_FIFO_TX_SIZE, SERIAL_FIFO_RX_SIZE);


#define SERIAL_BUFF_TX_SIZE 1
#define SERIAL_BUFF_RX_SIZE 1

NRF_SERIAL_BUFFERS_DEF(serial0_buffs, SERIAL_BUFF_TX_SIZE, SERIAL_BUFF_RX_SIZE);
NRF_SERIAL_BUFFERS_DEF(serial1_buffs, SERIAL_BUFF_TX_SIZE, SERIAL_BUFF_RX_SIZE);


NRF_SERIAL_CONFIG_DEF(serial0_config, NRF_SERIAL_MODE_DMA,
                      &serial0_queues, &serial0_buffs, NULL, sleep_handler);
NRF_SERIAL_CONFIG_DEF(serial1_config, NRF_SERIAL_MODE_DMA,
                      &serial1_queues, &serial1_buffs, NULL, sleep_handler);


NRF_SERIAL_UART_DEF(serial0_uarte, 0);
NRF_SERIAL_UART_DEF(serial1_uarte, 1);

nrf_drv_uart_config_t uarte0_drv_config ;
nrf_drv_uart_config_t uarte1_drv_config ;



void configure_serial_ports() {
    uarte0_drv_config.pselrts = NRF_UART_PSEL_DISCONNECTED;
    uarte0_drv_config.pseltxd = static_cast<int>(wn::dib_pin::uart0_tx_pin);
    uarte0_drv_config.pselcts = NRF_UART_PSEL_DISCONNECTED;
    uarte0_drv_config.pselrxd = static_cast<int>(wn::dib_pin::uart0_rx_pin);

    uarte0_drv_config.hwfc = NRF_UART_HWFC_DISABLED;
    uarte0_drv_config.parity = NRF_UART_PARITY_EXCLUDED;
    uarte0_drv_config.baudrate = NRF_UART_BAUDRATE_115200;
    uarte0_drv_config.interrupt_priority = UART_DEFAULT_CONFIG_IRQ_PRIORITY;
    uarte0_drv_config.use_easy_dma = true;


    uarte1_drv_config.pselrts = NRF_UART_PSEL_DISCONNECTED;
    uarte1_drv_config.pseltxd = static_cast<int>(wn::dib_pin::uart1_tx_pin);
    uarte1_drv_config.pselcts = NRF_UART_PSEL_DISCONNECTED;
    uarte1_drv_config.pselrxd = static_cast<int>(wn::dib_pin::uart1_rx_pin);

    uarte1_drv_config.hwfc = NRF_UART_HWFC_DISABLED;
    uarte1_drv_config.parity = NRF_UART_PARITY_EXCLUDED;
    uarte1_drv_config.baudrate = NRF_UART_BAUDRATE_115200;
    uarte1_drv_config.interrupt_priority = UART_DEFAULT_CONFIG_IRQ_PRIORITY;
    uarte1_drv_config.use_easy_dma = true;

    nrf_serial_init(&wn::serial0_uarte, &wn::uarte0_drv_config, &wn::serial0_config);

    nrf_serial_init(&wn::serial1_uarte, &wn::uarte1_drv_config, &wn::serial1_config);

}


}


#endif // SERIAL0_H
