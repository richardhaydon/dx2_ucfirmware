// Title: DX2  - persistent storage interface
//
// Description: Factory data class
// The
//
// Standard: C++ 17
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include "dx2_factory_data.hpp"

#include <assert.h>
namespace wn {

factory_data::factory_data(std::shared_ptr<persistent_storage_if> storage_if)
    : m_storage_if{storage_if} {
  if (m_storage_if->get_extent() < dx_data_offset::check_word + 1) {
    throw("storage_too_small_for_data");
  }
  init();
}

uint32_t factory_data::read() {
  uint32_t result{0};
  m_dx_data = m_storage_if->read();
  if (m_dx_data.size() < +dx_data_offset::check_word + 1) {
    return bad_size;
  }
  if (m_dx_data.at(dx_data_offset::layout_version) != m_layout_version) {
    return bad_version;
  }
  if (m_dx_data.at(dx_data_offset::check_word) != checkword()) {
    return bad_checkword;
  }

  return result;
}

uint32_t factory_data::write()
{
    m_dx_data.at(dx_data_offset::check_word) = checkword();
    return m_storage_if->save(m_dx_data);
}

void factory_data::init() {
  m_dx_data.clear();
  m_dx_data.push_back(m_layout_version);
  for (auto i = static_cast<int>(factory_data::dx_data_offset::hw_manu);
       i <= static_cast<int>(factory_data::dx_data_offset::check_word); i++) {
    m_dx_data.push_back(0);
  }
}

uint32_t factory_data::get_hw_manu() const {
  return m_dx_data.at(factory_data::dx_data_offset::hw_manu);
}

uint32_t factory_data::get_hw_ver_major() const {
  return m_dx_data.at(factory_data::dx_data_offset::hw_ver_major);
}

uint32_t factory_data::get_hw_ver_minor() const {
  return m_dx_data.at(factory_data::dx_data_offset::hw_ver_minor);
}

uint32_t factory_data::get_hw_manu_year() const {
  return m_dx_data.at(factory_data::dx_data_offset::hw_manu_year);
}

uint32_t factory_data::get_hw_manu_month() const {
  return m_dx_data.at(factory_data::dx_data_offset::hw_manu_month);
}

uint32_t factory_data::
    get_hw_sn() const {
  return m_dx_data.at(factory_data::dx_data_offset::hw_sn);
}

uint32_t factory_data::get_board_variant() const {
  return m_dx_data.at(factory_data::dx_data_offset::variant);
}

void factory_data::set_hw_manu(uint32_t value) {
  m_dx_data.at(factory_data::hw_manu) = value;
}

void factory_data::set_hw_ver_major(uint32_t value) {
  m_dx_data.at(factory_data::hw_ver_major) = value;
}

void factory_data::set_hw_ver_minor(uint32_t value) {
  m_dx_data.at(factory_data::hw_ver_minor) = value;
}

void factory_data::set_hw_manu_year(uint32_t value) {
  m_dx_data.at(factory_data::hw_manu_year) = value;
}

void factory_data::set_hw_manu_month(uint32_t value) {
  m_dx_data.at(factory_data::hw_manu_month) = value;
}

void factory_data::set_hw_sn(uint32_t value) {
  m_dx_data.at(factory_data::hw_sn) = value;
}

void factory_data::set_board_variant(uint32_t value) {
  m_dx_data.at(factory_data::variant) = value;
}


uint32_t factory_data::checkword() const {
  uint32_t result = 0xbeadbabe;
  for(auto i=static_cast<size_t>(dx_data_offset::hw_manu);i<static_cast<size_t>(dx_data_offset::check_word);i++) {
      result ^= m_dx_data.at(i);
  }
  return result;
}

} // namespace wn
