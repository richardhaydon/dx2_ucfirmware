// Title: DX2 - implementaion of the persistent storage interface using the nRF nvmc driver
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#include "nvmc_flash_storage.hpp"

#include "nrf_nvmc.h"
#include "nordic_common.h"


namespace wn {

namespace  {
const size_t max_extent {128};
}

nvmc_flash_storage::nvmc_flash_storage(size_t extent) : persistent_storage_if(extent)
{
    if (extent > max_extent) {
        throw "Exceeds max extent";
    }
    m_pg_num = NRF_FICR->CODESIZE - 1;
    m_pg_size = NRF_FICR->CODEPAGESIZE;
    m_addr = (m_pg_num * m_pg_size);
}

nvmc_flash_storage::~nvmc_flash_storage() {}

std::vector<uint32_t> nvmc_flash_storage::read() const
{
    std::vector<uint32_t> result;
    uint32_t *rp = reinterpret_cast<uint32_t *>(m_addr);
    for(auto i = 0u;i<m_extent;i++) {
        result.push_back( *rp );
        rp++;
    }
    return result;
}

uint32_t nvmc_flash_storage::save(const std::vector<uint32_t> &data)
{
    nrf_nvmc_page_erase(m_addr);

    // Enable write.
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy);

    for (auto i = 0u; i < data.size(); i++)
    {
        ((uint32_t*)m_addr)[i] =  data.at(i);
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy);
    }

    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy);

    return 0;
}

}
