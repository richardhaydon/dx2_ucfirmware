#include "dx2_motor_controller.hpp"


namespace wn {

DX2_MotorController::DX2_MotorController(std::shared_ptr<pwm_if> pwm, std::shared_ptr<gpio_if> gpio) : m_pwm{pwm}, m_gpio{gpio}
{

}

void DX2_MotorController::set_direction(MotorController::direction d)
{
    switch (d) {
        case direction::forward:
            m_gpio->set(wn::dib_pin::pump_direction);
            break;
        case direction::backwards:
            m_gpio->clr(wn::dib_pin::pump_direction);
            break;
    }
}

void DX2_MotorController::set_speed_percent(int s)
{
    if (s==0) {
        m_pwm->stop();
    } else {
        m_pwm->start(s);
    }
}

void DX2_MotorController::set_power_percent(int p)
{
    uint8_t setmap = p / 33;
    p & 0x01 ? m_gpio->set(wn::dib_pin::pump_current_bit0) :  m_gpio->clr(wn::dib_pin::pump_current_bit0);
    p & 0x02 ? m_gpio->set(wn::dib_pin::pump_current_bit1) :  m_gpio->clr(wn::dib_pin::pump_current_bit1);
}



} // namespace wn
