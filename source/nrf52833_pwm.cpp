// Title: DX2 - implementation of the pwm interface for the nrf52833 board
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#include "nrf52833_pwm.hpp"

#include "nrf_drv_pwm.h"
#include "FreeRTOS.h"
#include "task.h"

namespace wn {


nrf52833_pwm::nrf52833_pwm(const nrf_drv_pwm_t &pwm_ins, const nrf_drv_pwm_config_t &config, uint32_t cf) : m_pwm_ins{pwm_ins}, m_config{config}, m_cf{cf}
{
    m_sequence.length = 1;
    m_sequence.repeats = 0;
    m_sequence.end_delay = 0;
    m_sequence.values.p_common = &m_seq_value;
    APP_ERROR_CHECK(nrf_drv_pwm_init(&pwm_ins, &config, nullptr));
}

nrf52833_pwm::~nrf52833_pwm()
{

}

uint32_t nrf52833_pwm::run(uint16_t pct, uint32_t ms)
{
    auto cycles_per_s = m_cf / m_config.top_value;
    uint32_t repeats = ms * cycles_per_s / 1000u;
    m_seq_value = duty_cycle_reg(m_config.top_value,25000u,pct);

    (void)nrf_drv_pwm_simple_playback(&m_pwm_ins, &m_sequence, repeats, NRF_DRV_PWM_FLAG_STOP);
    while(!nrfx_pwm_is_stopped(&m_pwm_ins)) {
        vTaskDelay(1);
    }
    return 0;
}

uint32_t nrf52833_pwm::start(uint16_t pct)
{
    if(!nrfx_pwm_is_stopped(&m_pwm_ins)) {
        stop();
    }
    m_seq_value = duty_cycle_reg(m_config.top_value,m_cf,pct);
    return nrf_drv_pwm_simple_playback(&m_pwm_ins, &m_sequence, 1, NRFX_PWM_FLAG_LOOP);
}

uint32_t nrf52833_pwm::stop()
{
    return nrfx_pwm_stop(&m_pwm_ins,true);
}


} // namespace wn
