// Title: DX2
//
// Description: nrf52833 serial protocol implementation
// Defines a parser for the DX2 protocol, maintaining state in the reception of messages and
// statistics
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include "dx2_serial_if.hpp"
#include <iostream>

namespace wn {


dx2_serial_if::dx2_serial_if()
{

}


bool dx2_serial_if::rx(uint8_t c)
{
    switch (m_state) {
    case rx_protocol_state::complete:
    case rx_protocol_state::flag_in:
        if (c == serport_start_stop_flag) {
            m_state = rx_protocol_state::addr;
            m_calc_checksum = 0x00;
        }
        break;
    case rx_protocol_state::addr:
        m_rx_msg.m_address = c;
        m_state = rx_protocol_state::ctrl;
        m_calc_checksum+=c;
        break;
    case rx_protocol_state::ctrl:
        m_rx_msg.m_control = c;
        m_state = rx_protocol_state::cmd;
        m_calc_checksum+=c;
        break;
    case rx_protocol_state::cmd:
        m_rx_msg.m_command = c;
        m_state = rx_protocol_state::value_hi;
        m_calc_checksum+=c;
        break;
    case rx_protocol_state::value_hi:
        m_rx_msg.m_value = static_cast<uint16_t>(c) << 8u;
        m_state = rx_protocol_state::value_lo;
        m_calc_checksum+=c;
        break;
    case rx_protocol_state::value_lo:
        m_rx_msg.m_value |= static_cast<uint16_t>(c);
        m_state = rx_protocol_state::checksum;
        m_calc_checksum+=c;
        break;
    case rx_protocol_state::checksum:
        m_rx_checksum = c;
        m_state = rx_protocol_state::flag_out;
        break;
    case rx_protocol_state::flag_out:
        m_stats.rx_msg_cnt++;
        if (c == serport_start_stop_flag && m_rx_checksum == m_calc_checksum) {
            m_state = rx_protocol_state::complete;
            m_stats.rx_ok_cnt++;
        } else if (c != serport_start_stop_flag) {
            m_stats.rx_wrong_stopflag++;
            m_state = rx_protocol_state::flag_in;
        } else if (m_rx_checksum != m_calc_checksum) {
            m_stats.rx_checksum_err++;
            m_state = rx_protocol_state::flag_in;
        }
        break;
    }
    return (rx_protocol_state::complete == m_state);
}

bool dx2_serial_if::ready()
{
    return rx_protocol_state::complete == m_state;
}

scheduler_msg::msg dx2_serial_if::get_rx_msg()
{
    m_state = rx_protocol_state::flag_in;
    return m_rx_msg;
}

void dx2_serial_if::reset_stats()
{
    m_stats = stats{};
}

void dx2_serial_if::reset_state()
{
    m_rx_msg=scheduler_msg::msg{};
    m_state = rx_protocol_state::flag_in;
    m_rx_msg.m_msg_type = wn::scheduler_msg::msg_types::serial_rx;
}

uint16_t  dx2_serial_if::get_rx_ok_cnt        () {return m_stats.rx_ok_cnt;}    // Serial port statistics counter
uint16_t  dx2_serial_if::get_rx_msg_cnt       () {return m_stats.rx_msg_cnt;}    // Serial port statistics counter
uint16_t  dx2_serial_if::get_rx_checksum_err  () {return m_stats.rx_checksum_err;}  //Serial port statistics counter
uint16_t  dx2_serial_if::get_rx_oflowcnt      () {return m_stats.rx_oflowcnt;}    // Serial port overflow counter
uint16_t  dx2_serial_if::get_rx_wrong_stopflag() {return m_stats.rx_wrong_stopflag;} // Serial port error counter
uint16_t  dx2_serial_if::get_rx_write_reg_err () {return m_stats.rx_write_reg_err;} // Serial port error counter
uint16_t  dx2_serial_if::get_rx_read_reg_err  () {return m_stats.rx_read_reg_err;}

// Serial port error counter



} // namespace wn
