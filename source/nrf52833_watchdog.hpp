#ifndef NRF52_WATCHDOG_H
#define NRF52_WATCHDOG_H

#include "interface/watchdog.hpp"
#include "nrf_drv_wdt.h"

namespace wn {


class NRF52_watchdog : public wn::Watchdog
{
public:
    NRF52_watchdog();
    virtual ~NRF52_watchdog() override;

    void reset() override;

    void enable() override;

private:
    nrf_drv_wdt_channel_id m_channel_id;
    nrf_drv_wdt_config_t m_config = NRF_DRV_WDT_DEAFULT_CONFIG;

};

}
#endif // NRF52_WATCHDOG_H
