// Title: DX2 DIB scheduler message definitions
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.
#include "scheduler_msg.hpp"

wn::scheduler_msg::msg::~msg()
{

}

wn::scheduler_msg::msg::msg(wn::scheduler_msg::msg_types c) : m_msg_type{c}
{

}

wn::scheduler_msg::msg::msg(wn::scheduler_msg::msg_types c, uint8_t address, uint8_t control, uint8_t command, uint16_t value) : m_msg_type{c}, m_address{address}, m_control{control}, m_command{command},m_value{value}
{
}

const std::string wn::scheduler_msg::msg::getCommandStr() const
{
        switch (this->m_msg_type) {
        case msg_types::nop:
            return "NOP";
        case msg_types::tick:
            return "TICK";
        case msg_types::serial_rx:
            return "SERIAL_RX";
        case msg_types::power_on:
            return "POWER_ON";
        case msg_types::mains_status:
            return "MAINS_STATUS";
        }
        return "";
}
