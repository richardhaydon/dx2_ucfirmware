// Title: DX2 Sensor board
//
// Description: Scheduler task
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef SCHEDULER_HPP
#define SCHEDULER_HPP

#include "dib_const.hpp"
#include "wn_task.hpp"
#include "wn_msg.hpp"
#include "serial_interface.hpp"
#include "dx2_factory_data.hpp"
#include "scheduler_msg.hpp"
#include "gpio_if.hpp"
#include "pwm_if.hpp"
#include "power_monitor.hpp"
#include "interface/watchdog.hpp"

#include <memory>
namespace wn {


class Scheduler : public wn_task
{
public:
    Scheduler(std::shared_ptr<serial_interface> trace_com, std::shared_ptr<gpio_if> gpio, std::shared_ptr<factory_data> fact_data, std::shared_ptr<serial_interface> pc_com, std::shared_ptr<wn::pwm_if> pump_pwm, std::shared_ptr<wn::PowerMonitor> power_monitor, std::shared_ptr<wn::Watchdog> wdog);
    virtual ~Scheduler() override {}

    virtual void init() override;

    virtual void msg(WnMessage const *scheduler_msg) override;

    virtual bool runnable() override;

protected:
    // construction parameters
    std::shared_ptr<serial_interface> m_com;
    std::shared_ptr<gpio_if> m_gpio;
    std::shared_ptr<factory_data> m_factory_data;
    std::shared_ptr<serial_interface> m_pc_com;
    std::shared_ptr<wn::pwm_if> m_pump_pwm;
    std::shared_ptr<wn::PowerMonitor> m_power_monitor;
    std::shared_ptr<wn::Watchdog> m_watchdog;


    uint8_t   m_stateWriteFactoryData = FALSE; // TRUE when writing factory data
    uint8_t   m_ucLoopTics    = 0; // paced loop counter. Tic no.0 = startup.
    uint16_t  m_ucLoopCnt    = 0;  // Loop counter. Used for statistics
    uint8_t   m_ucState       = 0; // Contains current state of scheduler
    uint8_t   m_ucStateNxt    = 0; // Contains next state of scheduler
    uint8_t   m_ucStateNxtIrq = 0; // Contains next state of scheduler set from IRQ
    uint8_t   m_ucStateSetIrq = 0; // Set to high when IRQ changes uC state
    uint8_t   m_cnt100msec    = 0; // Counter used to generate 1 PPS signal
    uint8_t   m_pps_pulse     = LOW; // 1 PPS signal
    uint16_t  m_ucStateSecCnt =0;  // Helper counter used to count seconds in state machine

    // Serial port
    uint8_t   m_rx_cnt        = 0;    // Serial port RX byte counter
    //uint8_t   m_rx_buffer[SERPORT_MSG_LEN]; // Serial port RX buffer

    uint16_t  m_rx_ok_cnt     = 0;    // Serial port statistics counter
    uint16_t  m_rx_msg_cnt    = 0;    // Serial port statistics counter
    uint16_t  m_rx_checksum_err = 0;  //Serial port statistics counter
    uint16_t  m_rx_oflowcnt   = 0;    // Serial port overflow counter
    uint16_t  m_rx_wrong_stopflag= 0; // Serial port error counter
    uint16_t  m_rx_write_reg_err = 0; // Serial port error counter
    uint16_t  m_rx_read_reg_err = 0;  // Serial port error counter
    uint8_t   m_tx_cnt        = 0;    // Serial port TX byte counter
    uint8_t   m_tx_buffer[SERPORT_BUFFER_LEN]; // Serial portr TX buffer
    uint8_t   m_tx_wr_reg = 0;        // register to write to serial port
    uint8_t   m_tx_write = FALSE;     // Flag to signal write cmd to serial port from IRQ to process
    size_t    m_tx_msg_len;           // Length of msg to transmit on serial port


    //static uint16_t  rc2oflowcnt = 0;   // overflow counter for serial port #2
    //static uint16_t  rc2rxcnt = 0;      // receive message cnt for serial port #2
    // Sensors
    uint16_t  m_SensorCurrentLoop1    = 0; //Current Loop #1, analog result register.
    uint16_t  m_SensorCurrentLoop2    = 0; //Current Loop #2, analog result register.
    uint16_t  m_Sensor0To10V1         = 0; //0-10V #1, analog result register.
    uint16_t  m_Sensor0To10V2         = 0; //0-10V #2, analog result register.
    uint16_t  m_Sensor0To10V3         = 0; //0-10V #3, analog result register.
    uint16_t  m_Sensor0To10V4         = 0; //0-10V #4, analog result register.
    uint16_t  m_Sensor0To10V5         = 0; //0-10V #5, analog result register.
    uint16_t  m_Sensor0To10V6         = 0; //0-10V #6, analog result register.
    uint16_t  m_Sensor0To10V7         = 0; //0-10V #7, analog result register.
    uint16_t  m_Sensor1_024VbandGap   = 0; //Measured ADC value for 1.024V Band Gap value - used to calibrate ADCs
    uint16_t  m_SensorBattVoltADC     = 0; //Measured battery voltage (ADC value)
    float     m_SensorBattVolt        = 0.0; //Measured battery voltage
    uint16_t  m_SensorBattFloatADC    = 0; //Measured battery float voltage (ADC value)
    float     m_SensorBattFloat       = 0.0; //Measured battery float voltage
    float     m_BattChargeVolt1       = 0.0; // Estimated battery charger voltage (reading #1)
    float     m_BattChargeVolt2       = 0.0; // Estimated battery charger voltage (reading #2)
    uint16_t  m_SensorHumidity        = 0; //SHT25 Moisture data, 12bit.
    uint16_t  m_SensorTemperature     = 0; //SHT25 Temperature data, 14bit.
    uint16_t  m_SensorPumpCtrlCurr    = 0; //Monitored current sense
    uint16_t  m_SensorTempHumDeviceID = 0; //Sensor deviceId for temp/hum sensor

   //struct HDC1080ResultStr TempHumMeasurment;
   //uint16_t  SensorTempHumType     = TEMP_HUM_NO_SENSOR;  //Indicates whether SHT25 or HDC1080 is used as temp/hum sensor
    // Registers
   uint16_t m_regStatus             = 0; // Status register
   uint16_t m_regCtrl               = 0; // Control register
   uint16_t m_regPumpCtrl           = 0; // Pump control register
   uint16_t m_regBattChargePwm      = BATT_PWM_DEFAULT; // Battery Charge PWM control register
   uint16_t m_regLedCtrl            = 0; // LED control register
   uint16_t m_regPumpSpeed          = PUMP_SPEED; // Pump speed register
   uint16_t m_regPumpReverseTime    = STATE_STOPPING_SEC; // Pump reverse time register

   uint16_t m_pcbledcnt = 0;       // Counter for controlling PCB LED blinking frequency
   uint16_t m_topledcnt = 0;       // Counter for controlling Top LED blinking frequency
   uint8_t  m_BattSwitchCnt = 0;   // Counter used to control switching from battery to mains
   uint8_t  m_BattPresent = 0;     // State variable 1 when battery is detected, else 0
   uint8_t  m_battLowWarning = 0;  // State variable: 0 = No warning, battery healthy
        //                  1 = >3V difference to float voltage detected (Vfloat >= 27.4V)
        //                  2 = Batt voltage < 22.5V
        //                  3 = Batt voltage < 18V
   uint16_t  m_BattCntSec = 0;      // Counter how many seconds machine have been running on battery
   uint16_t  m_BattShutdownSec = 0; // BattCntValue when DX1 will automatically shutdown to preserve battery power.

   uint8_t m_HostPollSecCnt = 0;  // Counter to keep track of number of seconds since last status poll (0x2F command) from host when running process

    // Statistics counters
    uint16_t  m_stateCntSleep = 0;                 // Statistics counter
    uint16_t  m_stateCntRunProcess = 0;            // Statistics counter
    uint16_t  m_stateCntStopProcess = 0;           // Statistics counter
    uint16_t  m_stateCntStopProcessHostTimeout = 0;// Statistics counter
    uint16_t  m_stateCntStopProcessLostPower = 0;  // Statistics counter
    uint16_t  m_stateCntBattTimeout = 0;           // Statistics counter
    uint16_t  m_stateCntBattTimeoutLowVolt = 0;    // Statistics counter
    uint16_t  m_stateCntEmergBtn = 0;              // Statistics counter

    // Door
    int  m_DoorOpenSecCnt = 0;                // Door lock counter


    // handle periodic tick event
    void tick();

    /**
     * @brief BlinkPcbLed
     */
    void BlinkPcbLed();

    /**
     * @brief ReadStatus
     */
    void ReadStatus();

    /**
     * @brief BlinkTopLed
     */
    void BlinkTopLed();

    /**
     * @brief ReadAnalog
     */
    void ReadAnalog();

    /**
     * @brief ReadTempHumSensor
     */
    void ReadTempHumSensor();

    /**
     * @brief Init_io_low_power
     * Sets pins in low power state (ready for sleep mode)
     */
    void Init_io_low_power();

    /**
     * @brief CtrlWrite Update ports to match new control word written
     * Update relay, fan, door lock and valve status
     * after received new control word
     * @param value
    */
    void CtrlWrite(uint16_t value);

    /**
     * @brief ProcessStop - Compressor off, close all valves and stop pump
     */
    void ProcessStop();

    /**
     * @brief ProcessPrepare
     * Prepare process  - turn on compressor
     */
    void ProcessPrepare();

    /**
     * @brief ProcessStart - Keep compressor on, open air valve and start pump
     */
    void ProcessStart();

    /**
     * @brief PumpStart
     */
    void PumpStart(uint8_t direction);

    /**
     * @brief Ext230VACOn
     *  Turn on power to external 220VAC outlet
     *               Only supported when compressor (process) is not running
     *              - Compressor off,
     */
    void Ext230VACOn();

    /**
     * @brief irqPowerOn
     */
    void irqPowerOn();

    /**
     * @brief irqMainsStateChanbe
     */
    void irqMainsStateChange();


    /**
     * @brief ProcessPrepareToStop
     *   - turn off compressor
     *   - reverse pump
     *   - Open air bleed valve
     */
    void ProcessPrepareToStop();

    /**
     * @brief PumpStop Update Pump Control after write from PC
     */
    void PumpStop();

    /**
     * @brief PumpCtrlWrite
     * @param value: New pump control reg content
     */
    void PumpCtrlWrite(uint16_t value);


    /**
     * @brief BattChargePwmUpdate Update Battery Charger PWM when new duty cycle
     * @param duty_cycle  (0-1023)
     */
    void BattChargePwmUpdate( uint16_t duty_cycle );


    /**
     * @brief FactoryDataWrtieStart Initialize temporary buffer for factory data update
     */
    void FactoryDataWriteStart();

    /**
     * @brief FactoryDataWriteEnd
     * Erase flash sector and write factory data
     */
    void FactoryDataWriteEnd();

    /**
     * @brief StartBootloader
     * Reset the board in bootloader mode.
     */
    void StartBootloader();

    /**
     * @brief Init_io_power_on Sets pins in init state when power on
     * @param pc_on
     */
    void Init_io_power_on(uint8_t pc_on);

    /**
     * @brief Config_irq_sleep
     * Configure IRQs for sleep mode
     * Only enable IRQs for power on button and mains connected
     */
    void Config_irq_sleep();


    /**
     * @brief Config_irq_poweron
     * Configure IRQs for powerr on mode
     */
    void Config_irq_poweron();

    /**
     * @brief serport_rx_cnt_reset
     * Reset RX serial port statistics counters
     * (used when rx_msg_cnt wraps around)
     */
    void serport_rx_cnt_reset();

    /**
     * @brief serport_receive Process RX messages received on serial port
     */
    void serport_receive( wn::scheduler_msg::msg const *m );

    /**
     * @brief serport_transmit Transmit message on serial port
     */
    void serport_transmit( void );



};

} // namespace wn
#endif // SCHEDULER_HPP
