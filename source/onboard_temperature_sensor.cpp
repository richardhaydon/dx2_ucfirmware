// Title: DX2 - implementaion of the temperature interface using the nRF internal sensor
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include "onboard_temperature_sensor.hpp"

#include "nrf_temp.h"

namespace wn {

onboard_temperature_sensor::onboard_temperature_sensor() { nrf_temp_init(); }

int32_t onboard_temperature_sensor::read_temperature() {
  int32_t result;

  NRF_TEMP->TASKS_START = 1; /** Start the temperature measurement. */

  /* Busy wait while temperature measurement is not finished, you can skip
   * waiting if you enable interrupt for DATARDY event and read the result in
   * the interrupt. */
  /*lint -e{845} // A zero has been given as right argument to operator '|'" */
  while (NRF_TEMP->EVENTS_DATARDY == 0) {
    // Do nothing.
  }
  NRF_TEMP->EVENTS_DATARDY = 0;

  /**@note Workaround for PAN_028 rev2.0A anomaly 29 - TEMP: Stop task clears
   * the TEMP register. */
  result = (nrf_temp_read() / 4);

  /**@note Workaround for PAN_028 rev2.0A anomaly 30 - TEMP: Temp module analog
   * front end does not power down when DATARDY event occurs. */
  NRF_TEMP->TASKS_STOP = 1; /** Stop the temperature measurement. */

  return result;
}

} // namespace wn
