// Title: DX2 Sensor board - rs232_serial_interface
//
// Description: RS232 UART implementation of the serial interface class
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

//
#ifndef RS232_SERIAL_INTERFACE_HPP
#define RS232_SERIAL_INTERFACE_HPP

#include "serial_interface.hpp"
#include <nrf_serial.h>

namespace wn {


class rs232_serial_interface : public serial_interface
{
public:

    /**
     * @brief rs232_serial_interface construct with associated instance of the nrf_serial_t structure
     * @param interface
     */
    rs232_serial_interface(nrf_serial_t const *interface);

    ~rs232_serial_interface() override;

    /**
     * @brief tx send a byte
     * @return error code from nrf sdk
     */
    uint32_t tx(uint8_t c) override;

    /**
     * @brief tx string overload for tx
     * @param s string to send
     * @return error code from nrf sdk
     */
    uint32_t tx(std::string s) override;


    /**
     * @brief tx
     * @param p pointer to data to send
     * @param len of data to send
     * @return error code from nrf sdk
     */
    uint32_t tx(uint8_t *p, size_t len) override;


    /**
     * @brief rx read single byte
     * @return error code from nrf sdk
     */
    uint32_t rx(uint8_t * p_data,
                size_t size,
                size_t * p_read,
                uint32_t timeout_ms ) override;

    /**
     * @brief rx simplified read interface single byte blocking
     * @param c destination
     * @return error code
     */
    virtual uint32_t rx(uint8_t *c) override;

private:
    nrf_serial_t const *m_interface;
};

}
#endif // RS232_SERIAL_INTERFACE_HPP
