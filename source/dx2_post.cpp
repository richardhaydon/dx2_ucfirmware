// Title: DX2 - Power on self test
//
// Description: Sequence of actions using peripheral instances run prior to
// the main application
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#include "dx2_post.hpp"
#include "FreeRTOS.h"
#include "dx2_pins.hpp"
#include "task.h"
#include <iomanip>
#include <sstream>
#include "dx2_factory_data.hpp"

namespace wn {

const auto lm75_address {0x48};
const auto LM75A_REGISTER_PRODID {0x07};
const auto LM75A_REGISTER_TEMP {0x00};

dx2_post::dx2_post(std::shared_ptr<serial_interface> com,
                   std::shared_ptr<gpio_if> gpio,
                   std::shared_ptr<persistent_storage_if> storage,
                   std::shared_ptr<range_sensor> sensor,
                   std::shared_ptr<pwm_if> pwm,
                   std::shared_ptr<temperature_sensor> temp_sensor,
                   std::shared_ptr<arduino::TwoWire> twi, std::shared_ptr<Ina233_PowerMonitor> power_monitor)
    : m_com{com}, m_gpio{gpio}, m_storage{storage}, m_sensor{sensor},
    m_pump_pwm{pwm},m_temp_sensor{temp_sensor}, m_twi{twi}, m_power_monitor{power_monitor} {}

void dx2_post::run() {

  std::stringstream ss;
  m_com->tx("DX2 SelfTest\r\n");
  //m_gpio->init_pins_run_mode();
  vTaskDelay(pdMS_TO_TICKS(400));
  m_com->tx("DX2 start running test\r\n");


  auto temperature = m_temp_sensor->read_temperature();
  ss << "..Temperature " << std::setw(3) << std::setfill('0') << temperature
     << "\r\n";
  m_com->tx(ss.str());

  ss.clear();
  ss.str("");
//  int distance_mm;
//  auto read_range = m_sensor->read_range_mm(distance_mm);
//  if (read_range == 0) {
//    ss << "..Range       " << std::setw(3) << std::setfill('0') << distance_mm
//       << "\r\n";
//  } else {
//    ss << "..Range Fail "
//       << "\r\n";
//  }
  m_com->tx(ss.str());


  m_power_monitor->config();
  for(auto k=0;k<5;k++)  {
      vTaskDelay(pdMS_TO_TICKS(100));
      std::stringstream ss2;
      float vbus, vshunt;

      auto res = m_power_monitor->get_sense_V(vshunt);
      ss << "on sense " << vshunt << "\r\n";

      m_com->tx(ss.str());

  }
  {
      std::stringstream ss2;
      float current;

      auto res = m_power_monitor->get_current_A(current);
      ss << "current " << current << "\r\n";

      m_com->tx(ss.str());

  }

  std::string model = m_power_monitor->get_model();
  ss << "model is " <<model << "\r\n";


  // read the revision of the ina233
  ss.str("");







//  m_com->tx("Read the LM75 pid\r\n");
//  uint8_t read_8reg = LM75A_REGISTER_PRODID;
//  m_twi->beginTransmission(lm75_address);
//  m_twi->write(read_8reg);
//  m_twi->endTransmission();
//  auto result = m_twi->read();

//  m_com->tx("Read the LM75 temp\r\n");
//  uint8_t read_16reg = LM75A_REGISTER_TEMP;

//  m_twi->beginTransmission(lm75_address);
//  m_twi->write(read_16reg);
//  result = m_twi->endTransmission();
//  // result is 0-4
//    if (result != 0)
//  {
//    m_com->tx("failed\r\n");
//  }

//  result = m_twi->requestFrom(lm75_address, (uint8_t)2);
//  if (result != 2)
//  {
//      m_com->tx("failed\r\n");
//  }
//  uint8_t part1 = m_twi->read();
//  uint8_t part2 = m_twi->read();

////  //response = (Wire.read() << 8) | Wire.read();
//  uint16_t temp = part1 << 8 | part2;
//  auto response = part1 << 8 | part2;
//  char buf[64];
//  float lmtemperature = response/256.0f;

//  sprintf(buf,"read temp %0.3f\r\n", lmtemperature);

//  m_com->tx(buf);



//  m_com->tx("Setting indicators\r\n");

//  m_gpio->set(wn::dib_pin::pcb_led);
//  m_gpio->set(wn::dib_pin::force_run_on_battery_n);
//  m_gpio->set(wn::dib_pin::relay_on_230vac);
//  m_gpio->set(wn::dib_pin::battery_charge);

//  for (auto i = 0; i < 4; i++) {

//    if (i & 0x01) {
//        m_gpio->clr(static_cast<int>(wn::dib_pin::pcb_led));
//    } else {
//        m_gpio->set(static_cast<int>(wn::dib_pin::pcb_led));
//    }
//    if (i & 0x02) {
//        m_gpio->clr(static_cast<int>(wn::dib_pin::force_run_on_battery_n));
//    } else {
//        m_gpio->set(static_cast<int>(wn::dib_pin::force_run_on_battery_n));
//    }
//    vTaskDelay(pdMS_TO_TICKS(400));

//  }

//  for (auto i = 0; i < 16; i++) {

//      if (i & 0x01) {
//          m_gpio->clr(static_cast<int>(wn::dib_pin::valve_1));
//      } else {
//          m_gpio->set(static_cast<int>(wn::dib_pin::valve_1));
//      }
//      if (i & 0x02) {
//          m_gpio->clr(static_cast<int>(wn::dib_pin::valve_2));
//      } else {
//          m_gpio->set(static_cast<int>(wn::dib_pin::valve_2));
//      }
//      if (i & 0x04) {
//          m_gpio->clr(static_cast<int>(wn::dib_pin::valve_3));
//      } else {
//          m_gpio->set(static_cast<int>(wn::dib_pin::valve_3));
//      }
//      if (i & 0x08) {
//          m_gpio->clr(static_cast<int>(wn::dib_pin::valve_4));
//      } else {
//          m_gpio->set(static_cast<int>(wn::dib_pin::valve_4));
//      }

      //vTaskDelay(pdMS_TO_TICKS(400));

//  }



//  m_pump_pwm->run(5,5000);
////  m_battery_pwm->run(100,5000);
//  m_pump_pwm->run(100,5000);
////  m_battery_pwm->run(5,5000);


//  m_com->tx("Run forever tests\r\n");
////  m_battery_pwm->stop();
//  m_pump_pwm->stop();

////  m_battery_pwm->start(10);

//  for(auto j=1;j<5;j++) {
//  for(auto i=10;i<100;i++) {
//      m_pump_pwm->start(i);
//      vTaskDelay(pdMS_TO_TICKS(50));

//  }
//  for(auto i=100;i>0;i--) {
//      m_pump_pwm->start(i);
//      vTaskDelay(pdMS_TO_TICKS(50));

//  }
//  }

//  m_battery_pwm->stop();
//  m_pump_pwm->stop();
//  m_battery_pwm->stop();
//  m_pump_pwm->stop();


//  for(auto j=1;j<5;j++) {
//      for(auto i=10;i<100;i++) {
//          m_pump_pwm->start(i);
//          vTaskDelay(pdMS_TO_TICKS(50));

//      }
//      for(auto i=100;i>0;i--) {
//          m_pump_pwm->start(i);
//          vTaskDelay(pdMS_TO_TICKS(50));

//      }
//  }

//  m_battery_pwm->stop();
//  m_pump_pwm->stop();

  m_com->tx("Factory data test\r\n");

  wn::factory_data fs(m_storage);

  auto read_res = fs.read();
  auto write_rs = 0;
  ss.str({});
  if (read_res == factory_data::success) {
      ss << "Initial read result ok\r\n";
      ss << "get_hw_manu : " << fs.get_hw_manu() << "\r\n";
      ss << "get_hw_ver_major : " << fs.get_hw_ver_major() << "\r\n";
      ss << "get_hw_ver_minor : " << fs.get_hw_ver_minor() << "\r\n";
      ss << "get_hw_manu_year : " << fs.get_hw_manu_year() << "\r\n";
      ss << "get_hw_manu_month : " << fs.get_hw_manu_month() << "\r\n";
      ss << "get_hw_sn : " << fs.get_hw_sn() << "\r\n";
      ss << "get_variant : " << fs.get_board_variant() << "\r\n";
  } else  {
      ss << "Initial read result failed check repopulate\r\n";
      fs.init();
      fs.set_hw_manu(1);
      fs.set_hw_ver_major(0);
      fs.set_hw_ver_minor(1);
      fs.set_hw_manu_year(2020);
      fs.set_hw_manu_month(01);
      fs.set_hw_sn(0x000001);
      fs.set_board_variant(factory_data::board_variant::dib);
      write_rs = fs.write();
      ss << "Write result was " << std::to_string(write_rs) << "\r\n";

  }

  m_com->tx(ss.str());

  m_com->tx("exit running test\r\n");



}

} // namespace wn
