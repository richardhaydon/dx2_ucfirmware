// Title: DX2 - Power on self test
//
// Description: Sequence of actions using peripheral instances run prior to
// the main application
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include "gpio_if.hpp"
#include "persistent_storage_if.hpp"
#include "pwm_if.hpp"
#include "range_sensor.hpp"
#include "serial_interface.hpp"
#include "temperature_sensor.hpp"
#include "Arduino.h"
#include "power_monitor.hpp"
#include "ina233_powermonitor.hpp"

#include <memory>

#ifndef DX2_POST_HPP
#define DX2_POST_HPP

namespace wn {

class dx2_post {
public:
  dx2_post(std::shared_ptr<serial_interface> com, std::shared_ptr<gpio_if> gpio,
           std::shared_ptr<persistent_storage_if> storage,
           std::shared_ptr<range_sensor> sensor, std::shared_ptr<pwm_if> pump_pwm,
           std::shared_ptr<temperature_sensor> temp_sensor, std::shared_ptr<arduino::TwoWire> twi,
             std::shared_ptr<Ina233_PowerMonitor> power_monitor  );
  ~dx2_post() = default;

  void run();

private:
  std::shared_ptr<serial_interface> m_com;
  std::shared_ptr<gpio_if> m_gpio;
  std::shared_ptr<persistent_storage_if> m_storage;
  std::shared_ptr<range_sensor> m_sensor;
  std::shared_ptr<pwm_if> m_pump_pwm;
  std::shared_ptr<temperature_sensor> m_temp_sensor;
  std::shared_ptr<arduino::TwoWire> m_twi;
  std::shared_ptr<Ina233_PowerMonitor> m_power_monitor;
};

} // namespace wn
#endif // DX2_POST_HPP
