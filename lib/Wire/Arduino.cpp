#include "Arduino.h"

#include "FreeRTOS.h"
#include "boards.h"
#include "dx2_pins.hpp"
#include "nrf_drv_twi.h"
#include "task.h"

namespace arduino {

const int TWI_SUCCESS = 0;
const int TWI_ADDRESS_NAK = 2;
const int TWI_DATA_NAK = 3;
const int TWI_ERROR = 4;

void delay(unsigned d) { vTaskDelay(d * 10); }

std::function<void(std::string)> TwoWire::m_pf = nullptr;

TwoWire::TwoWire() {}

void TwoWire::set_printfn(std::function<void(std::string)> pf) { m_pf = pf; }

bool TwoWire::begin() {
  if (!m_i2c_joined) {
    m_twi_config = {.scl = static_cast<int>(wn::dib_pin::i2c_scl),
                    .sda = static_cast<int>(wn::dib_pin::i2c_sda),
                    .frequency = NRF_DRV_TWI_FREQ_100K,
                    .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
                    .clear_bus_init = false};
    auto res = nrf_drv_twi_init(&m_twi, &m_twi_config, nullptr, nullptr);
    APP_ERROR_CHECK(res);
    nrf_drv_twi_enable(&m_twi);
    m_i2c_joined = (res == 0);

  }
  return m_i2c_joined;
}

uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity) {
  m_rp = 0;
  auto res = nrf_drv_twi_rx(&m_twi, address, &m_rx_bytes[m_rp], quantity);
  if (res != TWI_SUCCESS) {
    m_last_error = "requestFrom error " + std::to_string(res);
    return 0;
  } else {
    return quantity;
  }
}

void TwoWire::beginTransmission(uint8_t addr) {
  m_txAddress = addr;
  m_wp = 0;
}

void TwoWire::write(uint8_t n) { m_tx_bytes[m_wp++] = n; }

uint8_t TwoWire::endTransmission(bool stop) {
  auto res = nrf_drv_twi_tx(&m_twi, m_txAddress, &m_tx_bytes[0], m_wp, !stop);
  m_last_error = "endTransmission error " + std::to_string(res);
  return static_cast<uint8_t>(res & 0xFF);
}

uint8_t arduino::TwoWire::read() {
  uint8_t result = m_rx_bytes[m_rp++];
  return result;
}

void TwoWire::debug(std::string s) {
  if (m_pf) {
    m_pf(s);
  }
}

void println(std::string s) { TwoWire::debug(s); }

void print(std::string s) {
  TwoWire::debug(s);
  TwoWire::debug("\r\n");
}

void print(char *s) { TwoWire::debug(s); }

void print(int n) { TwoWire::debug(std::to_string(n)); }

void println(int n) {
  TwoWire::debug(std::to_string(n));
  TwoWire::debug("\r\n");
}

} // namespace arduino
