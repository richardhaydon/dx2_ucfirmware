// Title: DX2 Sensor board - serial_interface
//
// Description: Emulation layer for the Arduino libraries:
// wire
// serial print
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#ifndef ARDUINO_H
#define ARDUINO_H

#include <cstdlib>
#include <functional>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

#include "nrf_drv_twi.h"

typedef bool boolean;
typedef uint8_t byte;
#include <functional>

namespace arduino {

/**
 * @brief The TwoWire class
 * Provides Arduino Two Wire class operations onto the nRF52 platform
 */
class TwoWire {
public:
  TwoWire();

  void set_printfn(std::function<void(std::string)> pf);

  /**
   * @brief begin Joint the i2c bus as master
   */
  bool begin();

  /**
   * @brief beginTransmission
   * Begin a transmission to the I2C slave device with the given address.
   * Subsequently, queue bytes for transmission with the write() function and
   * transmit them by calling endTransmission().
   * @param n address: the 7-bit address of the device to transmit to
   *
   */
  void beginTransmission(uint8_t n);

  /**
   * @brief write Writes data from a slave device in response to a request from
   * a master, or queues bytes for transmission from a master to slave device
   * (in-between calls to beginTransmission() and endTransmission()).
   * @param addr address we start transmission to
   */
  void write(uint8_t addr);

  /**
   * @brief endTransmission Ends a transmission to a slave device that was begun
   * by beginTransmission() and transmits the bytes that were queued by write().
   * @param set stop condition
   * @return byte, which indicates the status of the transmission:
   * 0:success
   * 1:data too long to fit in transmit buffer
   * 2:received NACK on transmit of address
   * 3:received NACK on transmit of data
   * 4:other error
   */
  uint8_t endTransmission(bool stop = true);

  /**
   * @brief requestFrom Used by the master to request bytes from a slave device.
   * The bytes may then be retrieved with the available() and read() functions.
   * @param address: the 7-bit address of the device to request bytes from
   * @param quantity: the number of bytes to request
   * @return the number of bytes returned from the slave device
   */
  uint8_t requestFrom(uint8_t address, uint8_t quantity);

  /**
   * @brief read Reads a byte that was transmitted from a slave device to a
   * master after a call to requestFrom() or was transmitted from a master to a
   * slave. read() inherits from the Stream utility class.
   * @return The next byte received
   */
  uint8_t read();

  static void debug(std::string s);

private:
  nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(0); // hard codes as instance 0
  nrf_drv_twi_config_t m_twi_config;
  uint8_t m_txAddress{0};
  uint8_t m_request_length{0};
  uint8_t m_tx_bytes[128];
  uint8_t m_rx_bytes[128];
  uint8_t m_rp;
  uint8_t m_wp;
  std::string m_last_error;
  bool m_i2c_joined{false};


  // method to log debug
  static std::function<void(std::string)> m_pf;
};

void println(std::string s);
void print(std::string s);

void print(char *s);

void print(int n);
void println(int n);

void delay(unsigned d);

} // namespace arduino

#endif // ARDUINO_H
