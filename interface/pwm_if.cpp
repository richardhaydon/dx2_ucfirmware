// Title: DX2 - base class for pwm interface implementations
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include "pwm_if.hpp"

namespace wn {

pwm_if::pwm_if()
{

}

uint16_t wn::pwm_if::duty_cycle_reg(uint16_t top_count, uint32_t cf, uint16_t percent)
{
    if (percent>100)
        percent=100;

  return top_count - static_cast<uint16_t>(0xFFFF & (static_cast<uint32_t>(top_count) * percent) / 100);
}
}

