#ifndef TEMPERATURE_SENSOR_HPP
#define TEMPERATURE_SENSOR_HPP

#include <stdint.h>

/**
 * @brief The temperature_sensor class interface class for temperature sensor
 */
class temperature_sensor
{
public:
    temperature_sensor() {}
    virtual ~temperature_sensor() {}
    /**
     * @brief read_temperature
     * @return temperature in C
     */
    virtual int32_t read_temperature() =0;
};

#endif // TEMPERATURE_SENSOR_HPP
