// Title: DX2 - gpio interface definition
//
// Description: Base class for gpio interface operations
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef GPIO_IF_HPP
#define GPIO_IF_HPP

namespace wn {


class gpio_if
{
public:
    gpio_if() {}

    virtual ~gpio_if() {}
    virtual void set(unsigned int pin) =0;
    virtual void clr(unsigned int pin) =0;
    virtual int get(int pin) = 0;

    virtual void init_pins_run_mode() = 0;
    virtual void init_pins_sleep_mode() = 0;

};

} // namespace wn

#endif // GPIO_IF_HPP
