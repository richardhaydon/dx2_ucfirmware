// Title: DX2 DIB
//
// Description: Base interface class for a power monitor implementation
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <stdint.h>
#include <string>

#ifndef POWERMONITOR_HPP
#define POWERMONITOR_HPP


namespace wn {


class PowerMonitor
{
public:
    PowerMonitor() {}
    virtual ~PowerMonitor() {}


    /**
     * @brief power_mode
     * @param normal true for normal operation false for low power off mode
     */
    virtual uint32_t set_power_mode(bool normal) = 0;

    /**
     * @brief get_model
     * @return model string
     */
    virtual std::string get_model() = 0;


    /**
     * @brief config setup the ina233 for current and power measurements with information on the sense resistor used
     * @return error code
     */
    virtual uint32_t config() = 0;

    /**
     * @brief get_current_A
     * @param reference to value to set
     * @return error code
     */
    virtual uint32_t get_current_A(float &amps) = 0;


    /**
     * @brief get_sense_V
     * @param reference to value to set
     * @return error code
     */
    virtual uint32_t get_sense_V(float &volts) = 0;

    /**
     * @brief get_bus_V
     * @param reference to value to set
     * @return error code
     */
    virtual uint32_t get_bus_V(float &volts) = 0;

};

} // namespace wn

#endif // POWERMONITOR_HPP
