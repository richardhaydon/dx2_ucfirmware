// Title: DX2 DIB Motor Controller
//
// Description: Base class for the motor controller implementation
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.



#ifndef MOTORCONTROLLER_HPP
#define MOTORCONTROLLER_HPP


class MotorController
{
public:
    MotorController() {}
    virtual ~MotorController() {}

    enum direction {forward, backwards};

    virtual void set_direction(direction) = 0;

    virtual void set_speed_percent(int) = 0;

    virtual void set_power_percent(int) = 0;


protected:
    direction m_direction {forward};
    int m_speed_percent{0};


};

#endif // MOTORCONTROLLER_HPP
