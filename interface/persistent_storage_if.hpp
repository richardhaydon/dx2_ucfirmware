// Title: DX2 Sensor board - persistent storage interface
//
// Description: Base class for persistent storage implementation
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

#include <vector>
#include <stdint.h>

#ifndef PERSISTENT_STORAGE_IF_HPP
#define PERSISTENT_STORAGE_IF_HPP

namespace wn {

class persistent_storage_if
{
public:
    persistent_storage_if(size_t extent) : m_extent{extent} {}
    virtual ~persistent_storage_if() {}

    /**
     * @brief save repopulate the entire storage area with given content
     * @return error code
     */
    virtual uint32_t save(const std::vector<uint32_t> &) = 0;

    /**
     * @brief read
     * @return flash content up to extent
     */
    virtual std::vector<uint32_t> read() const = 0;

    /**
     * @brief get_extent
     * @return max extent of the addressed structure
     */
    virtual inline size_t get_extent() { return m_extent; }

protected:
    size_t m_extent;

};

} // namespace wn
#endif // PERSISTENT_STORAGE_IF_HPP
