// Title: DX2 DIB - serial_interface
//
// Description: Base class for serial interface implementations
//
// Standard: C++ 17
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.

//
#ifndef SERIAL_INTERFACE_HPP
#define SERIAL_INTERFACE_HPP

#include <string>

namespace wn {

class serial_interface
{
public:
    virtual ~serial_interface() {};

    /**
     * @brief tx send a byte
     * @return error code from nrf sdk
     */
    virtual uint32_t tx(uint8_t) = 0;

    /**
     * @brief tx string overload for tx
     * @return error code from nrf sdk
     */
    virtual uint32_t tx(std::string) = 0;


    /**
     * @brief tx string overload for tx
     * @param p pointer to contiguous data to sent
     * @param len of data to send
     * @return error code from nrf sdk
     */
    virtual uint32_t tx(uint8_t *p, size_t len) = 0;


    /**
     * @brief rx
     * @param p_data destination buffer to read to
     * @param size max size of buffer
     * @param p_read set by call how many bytes read
     * @param timeout_ms
     * @return error code
     */
    virtual uint32_t rx(uint8_t * p_data,
                        size_t size,
                        size_t * p_read,
                        uint32_t timeout_ms ) = 0;

    /**
     * @brief rx simplified read interface single byte blocking
     * @param c destination
     * @return error code
     */
    virtual uint32_t rx(uint8_t *c) = 0;

};

}
#endif // SERIAL_INTERFACE_HPP
