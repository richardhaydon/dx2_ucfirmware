// Title: DX2 - base class for pwm interface implementations
//
//
// Standard: C++ 17, nRF52833 SDK 1.6
//
//
// Copyright notice
// =========================================================================
// Copyright (c) 2014-2019 by WideNorth AS (www.widenorth.com).
// All rights reserved.
//
// This file is part of the WideNorth library. Files and source code from
// this library can be made available to WideNorth's customers in
// development (consulting) projects.
// Customers of WideNorth are allowed to copy and modify this file for use
// in their own projects and products - provided that any copy or
// derivation from this or parts of this file includes this Copyright
// notice in its heading. Customers of WideNorth are not allowed to
// further sublicense this file, or distribute source code for this file,
// without prior written approval from WideNorth.


#ifndef PWM_IF_HPP
#define PWM_IF_HPP

#include <stdint.h>

namespace wn {


/**
 * @brief The pwm_if class base class for the pwm implementations
 */
class pwm_if
{
public:
    pwm_if();
    virtual ~pwm_if() {}

    /**
     * @brief run drive the pin associated with the instance at the given duty cycle for the given period
     * @param pct
     * @param ms
     * @return error code
     */
    virtual uint32_t run(uint16_t pct, uint32_t ms) = 0;

    /**
     * @brief start drive output with % duty cycle until stopped
     * @param pct
     * @return error code
     */
    virtual uint32_t start(uint16_t pct) =0;

    /**
     * @brief stop drive output
     * @param pct
     * @return error code
     */
    virtual uint32_t stop() = 0;

    /**
     * @brief duty_cycle_reg
     * @param top_count trigger point for counter up to top count
     * @param top_count wrap around point for counter
     * @param percent required duty cycle percent
     * @return setting for 16bit duty cycle value for c.f. and top count for a non inverted pin
     */
    uint16_t duty_cycle_reg(uint16_t top_count, uint32_t cf, uint16_t percent);


};

}
#endif // PWM_IF_HPP
